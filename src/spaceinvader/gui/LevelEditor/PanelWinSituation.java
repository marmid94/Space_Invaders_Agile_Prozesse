
package spaceinvader.gui.LevelEditor;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

/**
 * class which generate the WinSituation of the LevelEditor
 * @author Stefan Fruhwirth
 * @version 1.1
 */
class PanelWinSituation extends JPanel{
    
    private final JLabel labelAll, labelPoints; 
    private final JCheckBox checkboxAll, checkboxPoints; 
    
    private final JSpinner spinner;
    
    private final JPanel panel; 
    
    /**
     * Konstructor
     * @param controller 
     */
    public PanelWinSituation(LevelEditorController controller){
               
        this.panel = new JPanel();
        this.panel.setLayout(new GridLayout(3,3,10,10));        
        
        this.labelAll = new JLabel("alle Gegner besiegt"); 
        this.labelPoints = new JLabel("Punkte");
        
        this.checkboxAll = new JCheckBox(); 
        this.checkboxAll.setSelected(true);
        this.checkboxAll.setActionCommand("WinSituationAllEnemysDestroyed");
        this.checkboxAll.addActionListener(controller);
        
        this.checkboxPoints = new JCheckBox();
        this.checkboxPoints.setActionCommand("WinSituationPoints");
        this.checkboxPoints.addActionListener(controller);
        
        this.spinner = new JSpinner(new SpinnerNumberModel(0, 0, 100000, 10));
        this.spinner.setName("WinSituationPoints");
        this.spinner.addChangeListener(controller);
        
        this.panel.add(new JLabel(""));
        this.panel.add(new JLabel("Aktivieren"));
        this.panel.add(new JLabel("Anzahl"));
        this.panel.add(this.labelAll); 
        this.panel.add(this.checkboxAll);
        this.panel.add(new JLabel("")); 
        this.panel.add(this.labelPoints); 
        this.panel.add(this.checkboxPoints); 
        this.panel.add(this.spinner); 
        
        this.setLayout(new FlowLayout(30,30,30));
        this.add(this.panel);  
        
    }
    
    /**
     * get the points to win information
     * @return points
     */
    public int getPointsForWin() {
        if(this.isPointsForWinEnabled()) {
            return (int)this.spinner.getValue();
        }
        else {
            return -1;
        }
    }
    
    /**
     * get the information if points to win is active
     * @return bool
     */
    public boolean isPointsForWinEnabled() {
        return this.checkboxPoints.isSelected();
    }
    
    /**
     * get the information if the win situation, 
     * kill all enemies to win is active
     * @return 
     */
    public boolean isWinOnAllEnemiesDeadEnabled() {
        return this.checkboxAll.isSelected();
    }
}
