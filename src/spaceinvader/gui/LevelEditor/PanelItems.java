
package spaceinvader.gui.LevelEditor;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JSpinner;

/**
 * class which generate the Item of the LevelEditor
 * @author Stefan Fruhwirth
 * @version 1.1
 */
class PanelItems extends JPanel{

    private final JLabel labelPointItem, labelMirrorItem, 
            labelLifeItem, labelReflectingItem, labelDeflectorItem; 
    private final JCheckBox checkBoxPointItemActive, checkBoxMirrorItemActive,
            checkBoxLifeItemActive, checkBoxReflectingItemActive, checkBoxDeflectorItemActive; 
    private final JSpinner spinnerPointItemPoints, spinnerMirrorItemTime,
            spinnerReflectingItemTime, spinnerDeflectorItemTime; 
    private final JSlider sliderPointItemPropability, sliderMirrorItemPropability,
            sliderLifeItemPropability, sliderReflectingItemPropability, sliderDeflectorItemPropability; 
    
    private final JPanel panel; 
    
    /**
     * Konstructor
     * @param controller 
     */
    public PanelItems(LevelEditorController controller) {
        
        this.panel = new JPanel(); 
        this.panel.setLayout(new GridLayout(6, 4));
        
        this.labelMirrorItem = new JLabel("Punktitem");
        this.labelPointItem = new JLabel("Spiegelitem"); 
        this.labelLifeItem = new JLabel("Lebenitem");
        this.labelReflectingItem = new JLabel("Geschlossene-Welt-Item");
        this.labelDeflectorItem = new JLabel("Deflectoritem");
        
        this.checkBoxPointItemActive = new JCheckBox(); 
        this.checkBoxPointItemActive.setActionCommand("checkBoxPointsItem");
        this.checkBoxPointItemActive.addActionListener(controller);
        
        this.checkBoxMirrorItemActive = new JCheckBox();
        this.checkBoxMirrorItemActive.setActionCommand("checkBoxMirrorItem");
        this.checkBoxMirrorItemActive.addActionListener(controller);
        
        this.checkBoxLifeItemActive = new JCheckBox();
        this.checkBoxLifeItemActive.setActionCommand("checkBoxLifeItem");
        this.checkBoxLifeItemActive.addActionListener(controller);
        
        this.checkBoxReflectingItemActive = new JCheckBox();
        this.checkBoxReflectingItemActive.setActionCommand("checkBoxReflectingItem");
        this.checkBoxReflectingItemActive.addActionListener(controller);
        
        this.checkBoxDeflectorItemActive = new JCheckBox();
        this.checkBoxDeflectorItemActive.setActionCommand("checkBoxDeflectorItem");
        this.checkBoxDeflectorItemActive.addActionListener(controller);
        
        this.spinnerPointItemPoints = new JSpinner(); 
        this.spinnerPointItemPoints.setName("spinnerPointItem");
        this.spinnerPointItemPoints.addChangeListener(controller);
        
        this.spinnerMirrorItemTime = new JSpinner(); 
        this.spinnerMirrorItemTime.setName("spinnerMirrorItem");
        this.spinnerMirrorItemTime.addChangeListener(controller);
        
        this.spinnerReflectingItemTime = new JSpinner(); 
        this.spinnerReflectingItemTime.setName("spinnerReflectingItem");
        this.spinnerReflectingItemTime.addChangeListener(controller);
        
        this.spinnerDeflectorItemTime = new JSpinner(); 
        this.spinnerDeflectorItemTime.setName("spinnerDeflectorItem");
        this.spinnerDeflectorItemTime.addChangeListener(controller);
        
        this.sliderPointItemPropability = new JSlider(); 
        this.sliderPointItemPropability.setName("sliderPointItemPropability");
        this.sliderPointItemPropability.setMinimum(0);
        this.sliderPointItemPropability.setMaximum(100);
        
        this.sliderPointItemPropability.setMajorTickSpacing(25);
        this.sliderPointItemPropability.setMinorTickSpacing(1);
        this.sliderPointItemPropability.setPaintLabels(true);
        this.sliderPointItemPropability.setPaintTicks(true);
        
        this.sliderPointItemPropability.addChangeListener(controller);
        
        this.sliderMirrorItemPropability = new JSlider(); 
        this.sliderMirrorItemPropability.setName("sliderMirrorItemPropability");
        this.sliderMirrorItemPropability.setMinimum(0);
        this.sliderMirrorItemPropability.setMaximum(100);
        
        this.sliderMirrorItemPropability.setMajorTickSpacing(25);
        this.sliderMirrorItemPropability.setMinorTickSpacing(1);
        this.sliderMirrorItemPropability.setPaintLabels(true);
        this.sliderMirrorItemPropability.setPaintTicks(true);
        
        this.sliderMirrorItemPropability.addChangeListener(controller);
           
        this.sliderLifeItemPropability = new JSlider(); 
        this.sliderLifeItemPropability.setName("sliderLifeItemPropability");
        this.sliderLifeItemPropability.setMinimum(0);
        this.sliderLifeItemPropability.setMaximum(100);
        
        this.sliderLifeItemPropability.setMajorTickSpacing(25);
        this.sliderLifeItemPropability.setMinorTickSpacing(1);
        this.sliderLifeItemPropability.setPaintLabels(true);
        this.sliderLifeItemPropability.setPaintTicks(true);
        
        this.sliderLifeItemPropability.addChangeListener(controller);
        
        this.sliderReflectingItemPropability = new JSlider(); 
        this.sliderReflectingItemPropability.setName("sliderMirrorItemPropability");
        this.sliderReflectingItemPropability.setMinimum(0);
        this.sliderReflectingItemPropability.setMaximum(100);
        
        this.sliderReflectingItemPropability.setMajorTickSpacing(25);
        this.sliderReflectingItemPropability.setMinorTickSpacing(1);
        this.sliderReflectingItemPropability.setPaintLabels(true);
        this.sliderReflectingItemPropability.setPaintTicks(true);
        
        this.sliderReflectingItemPropability.addChangeListener(controller);
        
        this.sliderDeflectorItemPropability = new JSlider(); 
        this.sliderDeflectorItemPropability.setName("sliderMirrorItemPropability");
        this.sliderDeflectorItemPropability.setMinimum(0);
        this.sliderDeflectorItemPropability.setMaximum(100);
        
        this.sliderDeflectorItemPropability.setMajorTickSpacing(25);
        this.sliderDeflectorItemPropability.setMinorTickSpacing(1);
        this.sliderDeflectorItemPropability.setPaintLabels(true);
        this.sliderDeflectorItemPropability.setPaintTicks(true);
        
        this.sliderDeflectorItemPropability.addChangeListener(controller);
        
        this.panel.add(new JLabel(""));
        this.panel.add(new JLabel("Akivieren"));
        this.panel.add(new JLabel("Wahrscheinlichkeit"));
        this.panel.add(new JLabel("Dauer in Sekunden / Punkte")); 
        
        this.panel.add(this.labelPointItem); 
        this.panel.add(this.checkBoxPointItemActive);
        this.panel.add(this.sliderPointItemPropability); 
        this.panel.add(this.spinnerPointItemPoints); 
        
        this.panel.add(this.labelMirrorItem);  
        this.panel.add(this.checkBoxMirrorItemActive);  
        this.panel.add(this.sliderMirrorItemPropability);
        this.panel.add(this.spinnerMirrorItemTime); 
        
        this.panel.add(this.labelLifeItem);  
        this.panel.add(this.checkBoxLifeItemActive);  
        this.panel.add(this.sliderLifeItemPropability);
        this.panel.add(new JLabel()); 
        
        this.panel.add(this.labelReflectingItem);  
        this.panel.add(this.checkBoxReflectingItemActive);  
        this.panel.add(this.sliderReflectingItemPropability);
        this.panel.add(this.spinnerReflectingItemTime); 
        
        this.panel.add(this.labelDeflectorItem);  
        this.panel.add(this.checkBoxDeflectorItemActive);  
        this.panel.add(this.sliderDeflectorItemPropability);
        this.panel.add(this.spinnerDeflectorItemTime); 
        
        this.setLayout(new FlowLayout(20,20,20));
        this.add(this.panel); 
        
        this.getPointsItemPoints();
    }
    
    /**
     * get the information if the pointItem is active
     * @return bool
     */
    public boolean getPointItemActive() {
        return this.checkBoxPointItemActive.isSelected();
    }
    
    /**
     * get from the PointItem the porpability
     * @return porpability
     */
    public int getPointItemPorpability() {
        return this.sliderPointItemPropability.getValue();
    }
    
    /**
     * get from the PointsItem the points
     * @return points
     */
    public int getPointsItemPoints() {
        return (int)this.spinnerPointItemPoints.getValue();
    }
    
    /**
     * get the information if the LifeItem is active
     * @return bool
     */
    public boolean getLifeItemActive() {
        return this.checkBoxLifeItemActive.isSelected();
    }
    
    /**
     * get from the LifeItem the porpability
     * @return porpability
     */
    public int getLifeItemPorpability() {
        return this.sliderLifeItemPropability.getValue();
    }
    
    /**
     * get the infromation if the MirrorItem is active
     * @return bool
     */
    public boolean getMirrorItemActive() {
        return this.checkBoxMirrorItemActive.isSelected();
    }
    
    /**
     * get from the MirrorItem the porpability
     * @return porpability
     */
    public int getMirrorItemPorpability() {
        return this.sliderMirrorItemPropability.getValue();
    }
    
    /**
     * get from the MirrorItem the time
     * @return time
     */
    public int getMirrorItemTime() {
        return (int)this.spinnerMirrorItemTime.getValue();
    }
    
    /**
     * get the information if the ReflectingItem is active
     * @return bool
     */
    public boolean getReflectingItemActive() {
        return this.checkBoxReflectingItemActive.isSelected();
    }
    
    /**
     * get from the ReflectingItem the porpability
     * @return porpability
     */
    public int getReflectingItemPorpability() {
        return this.sliderReflectingItemPropability.getValue();
    }
    
    /**
     * get from the ReflectingItem the time
     * @return time
     */
    public int getReflectingItemTime() {
        return (int)this.spinnerReflectingItemTime.getValue();
    }
    
    /**
     * get the information if the DeflectorItem is active
     * @return bool
     */
    public boolean getDeflectorItemActive() {
        return this.checkBoxDeflectorItemActive.isSelected();
    }
    
    /**
     * get from the DeflectingItem the porpability
     * @return porpability
     */
    public int getDeflectorItemPorpability() {
        return this.sliderDeflectorItemPropability.getValue();
    }
    
    /**
     * get from the DeflectorItem the time
     * @return time
     */
    public int getDeflectorItemTime() {
        return (int)this.spinnerDeflectorItemTime.getValue();
    }
    
}
