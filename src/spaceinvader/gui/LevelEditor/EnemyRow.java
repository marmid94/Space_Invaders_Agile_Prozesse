
package spaceinvader.gui.LevelEditor;

import java.util.List;

/**
 * class which generate a row of enemies, each enemy has different typs and 
 * different munition 
 * @author Stefan Fruhwirth
 * @version 1.1
 */
public class EnemyRow {
    
    protected static int counter = 1;
    
    private String name; 
    private String typ; 
    private int number; 
    private int livePoints; 
    private int rateOfFire; 
    private String weapon; 
    private int wave; 
    private int points; 
    
    /**
     * Konstructor
     * @param listMonsterTyps
     * @param listMunition 
     */
    public EnemyRow(List<String> listMonsterTyps, List<String> listMunition){
        
        this.name = "Reihe " + counter;
        this.typ = ""; 
        this.number = 10; 
        this.livePoints = 3; 
        this.rateOfFire = 50; 
        this.weapon = listMunition.get(0); 
        this.wave = 1; 
        this.points = 10;  
        this.typ = listMonsterTyps.get(0); 
        
    }
    
    /**
     * set the Name of the enemy
     * @param name 
     */
    public void setName(String name){
        this.name = name; 
    }
     
    /**
     * get the name of the enemy 
     * @return name
     */
    public String getName(){
        return this.name; 
    }
    
    /**
     * sets the typ of the enemy
     * @param typ
     */
    public void setTyp(String typ){
        this.typ = typ; 
    }
    
    /**
     * get the type of the enemy
     * @return typ
     */
    public String getType(){
        return this.typ; 
    }
    
    /**
     * set the number of the enemy
     * @param number 
     */
    public void setNumber(int number){
        this.number = number; 
    }
    
    /**
     * get the number of the enemy
     * @return number
     */
    public int getNumber(){
        return this.number; 
    }
    
    /**
     * set the livePoints of the enemy
     * @param livePoints 
     */
    public void setLivePoints(int livePoints){
        this.livePoints = livePoints; 
    }
    
    /**
     * get the livePoints of the enemy
     * @return livePoints
     */
    public int getLivePoints(){
        return this.livePoints; 
    }
    
    /**
     * set the rateOfFire of the enemy
     * @param rateOfFire 
     */
    public void setRateOfFire(int rateOfFire){
        this.rateOfFire = rateOfFire; 
    }
    
    /**
     * gets the rateOfFire of the enemy
     * @return rateOfFire
     */
    public int getRateOfFire(){
        return this.rateOfFire;
    }
    
    /**
     * set the Weapon of the enemy
     * @param weapon 
     */
    public void setWeapon(String weapon){
        this.weapon = weapon; 
    }
    
    /**
     * gets the weapon the enemy
     * @return weapon
     */
    public String getWeapon(){
        return this.weapon; 
    }
    
    /**
     * set the wave of the enemy
     * @param wave 
     */
    public void setWave(int wave){
        this.wave = wave; 
    }
    
    /**
     * get the wave of the enemy
     * @return wave
     */
    public int getWave(){
        return this.wave; 
    }
    
    /**
     * set the points of the enemy
     * @param points 
     */
    public void setPoints(int points){
        this.points = points; 
    }
    
    /**
     * gets the points of the enemy
     * @return points
     */
    public int getPoints(){
        return this.points; 
    }
    
}
