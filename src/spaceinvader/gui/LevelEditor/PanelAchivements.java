
package spaceinvader.gui.LevelEditor;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

/**
 * class which generate the achivement frame for the levelEditor
 * @author Stefan Fruhwirth
 * @version 1.1
 */
class PanelAchivements extends JPanel{

    private final JPanel panel; 
    
    private final JLabel labelDoubleShoot, labelTripleShoot;
    private final JCheckBox checkBoxDoubleShoot, checkBoxTripleShoot;    
    private final JSpinner spinnerDoubleShootTime, spinnerDoubleShootPoints, 
                     spinnerTripleShootTime, spinnerTripleShootPoints; 
    
    
    /**
     * Konstructor
     * @param controller 
     */
    public PanelAchivements(LevelEditorController controller) {
        this.panel = new JPanel(); 
        
        this.panel.setLayout(new GridLayout(3,4,30,30));
        
        this.labelDoubleShoot = new JLabel("Doppelschuss");
        this.labelTripleShoot = new JLabel("Dreifachschuss");
        
        this.checkBoxDoubleShoot = new JCheckBox();
        this.checkBoxDoubleShoot.setActionCommand("DoubleShoot");
        this.checkBoxDoubleShoot.addActionListener(controller);
        
        this.checkBoxTripleShoot = new JCheckBox(); 
        this.checkBoxTripleShoot.setActionCommand("TripleShoot");
        this.checkBoxTripleShoot.addActionListener(controller);
        
        this.spinnerDoubleShootTime = new JSpinner(new SpinnerNumberModel(0, 0, 5000, 10));
        this.spinnerDoubleShootTime.setName("spinnerDoubleShootTime");
        this.spinnerDoubleShootTime.addChangeListener(controller);
        
        this.spinnerDoubleShootPoints = new JSpinner(new SpinnerNumberModel(0,0,5000,10));
        this.spinnerDoubleShootPoints.setName("spinnterDoubleShootPoints");
        this.spinnerDoubleShootPoints.addChangeListener(controller);
        
        this.spinnerTripleShootTime = new JSpinner(new SpinnerNumberModel(0, 0, 5000, 10));
        this.spinnerTripleShootTime.setName("spinnerTripleShootTime");
        this.spinnerTripleShootTime.addChangeListener(controller);
        
        this.spinnerTripleShootPoints = new JSpinner(new SpinnerNumberModel(0,0,5000,10));
        this.spinnerTripleShootPoints.setName("spinnerTripleShootPoints");
        this.spinnerTripleShootPoints.addChangeListener(controller);
        
        this.panel.add(new JLabel(""));
        this.panel.add(new JLabel("Aktivieren"));
        this.panel.add(new JLabel("Zeit in Sekunden"));
        this.panel.add(new JLabel("Punkte")); 
        
        this.panel.add(this.labelDoubleShoot); 
        this.panel.add(this.checkBoxDoubleShoot);
        this.panel.add(this.spinnerDoubleShootTime); 
        this.panel.add(this.spinnerDoubleShootPoints); 
        
        this.panel.add(this.labelTripleShoot); 
        this.panel.add(this.checkBoxTripleShoot); 
        this.panel.add(this.spinnerTripleShootTime); 
        this.panel.add(this.spinnerTripleShootPoints); 
        
        this.setLayout(new FlowLayout(20,20,20));
        this.add(this.panel); 
    }
    
    /**
     * get the information if double kill is active
     * @return bool
     */
    public boolean isDoubleKillActive() {
        return this.checkBoxDoubleShoot.isSelected();
    }
    
    /**
     * get the double kill Time
     * @return time
     */
    public int getDoubleKillTime() {
        return (int)this.spinnerDoubleShootTime.getValue();
    }
    
    /**
     * get the double kill points
     * @return points
     */
    public int getDoubleKillPoints() {
        return (int)this.spinnerDoubleShootPoints.getValue();
    }
    
    /**
     * get the information if triple kill is active
     * @return bool
     */
    public boolean isTripleKillActive() {
        return this.checkBoxTripleShoot.isSelected();
    }
    
    /**
     * get the triple kill time
     * @return time
     */
    public int getTripleKillTime() {
        return (int)this.spinnerTripleShootTime.getValue();
    }
    
    /**
     * get the triple kill points
     * @return points
     */
    public int getTripleKillPoints() {
        return (int)this.spinnerTripleShootPoints.getValue();
    }
    
}