
package spaceinvader.gui.LevelEditor;

import java.awt.BorderLayout;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import spaceinvader.model.GameController; 

/**
 * class which generate the Frame of the LevelEditor
 * @author Stefan Fruhwirth
 * @version 1.1
 */
public class LevelEditor extends JFrame{
    
    private final JTabbedPane tabbedPane; 
    private final JPanel panel, panelButton; 
    
    private final JButton buttonSafe; 
    
    private final PanelDescription panelDescription;
    protected final PanelMonster panelMonster; 
    private final PanelPlayer panelPlayer; 
    private final PanelItems panelItems; 
    private final PanelWinSituation panelWinSituation; 
    private final PanelAchivements panelAchivements; 
    
    /**
     * Konstructor
     * @param controller
     * @param gameController
     * @param monster 
     */
    public LevelEditor(LevelEditorController controller, GameController gameController, EnemyRow monster){
    
        this.setTitle("LevelEditor");
        
        this.tabbedPane = new JTabbedPane(); 
                       
        this.panelDescription = new PanelDescription(); 
        this.panelMonster = new PanelMonster(controller, monster); 
        this.panelPlayer = new PanelPlayer(controller, gameController); 
        this.panelItems = new PanelItems(controller); 
        this.panelWinSituation = new PanelWinSituation(controller); 
        this.panelAchivements = new PanelAchivements(controller); 
        
        this.tabbedPane.add("Beschreibung",this.panelDescription); 
        this.tabbedPane.add("Monster",this.panelMonster); 
        this.tabbedPane.add("Spieler",this.panelPlayer);
        this.tabbedPane.add("Items", this.panelItems); 
        this.tabbedPane.add("Gewinnbedingungen", this.panelWinSituation); 
        this.tabbedPane.add("Achivements", this.panelAchivements); 
        
        
        this.panelButton = new JPanel();
        
        this.buttonSafe = new JButton("Speichern"); 
        this.buttonSafe.setName("buttonSafe"); 
        this.buttonSafe.setActionCommand("buttonSafe");
        this.buttonSafe.addActionListener(controller);
        
        this.panelButton.add(this.buttonSafe); 
        
        this.panel = new JPanel(); 
        this.panel.setLayout(new BorderLayout());
        
        this.panel.add(this.tabbedPane, BorderLayout.CENTER); 
        this.panel.add(this.panelButton, BorderLayout.SOUTH); 
        
        this.add(this.panel); 
        
        this.pack();
        this.setVisible(true);
    }
    
    /**
     * get the levelName
     * @return name
     */
    String getLevelName() {
        return this.panelDescription.getNameInformation();
    }
    
    /**
     * gets from the PointItem activeMode
     * @return bool
     */
    public boolean getPointItemActive() {
        return this.panelItems.getPointItemActive();
    }
    
    /**
     * gets of the PointItem the porbalitiy 
     * @return porbalitity
     */
    public int getPointItemPorpability() {
        return this.panelItems.getPointItemPorpability();
    }
    
    /**
     * get from the PointItem the points
     * @return points
     */
    public int getPointItemPoints() {
        return this.panelItems.getPointsItemPoints();
    }
    
    /**
     * get from the LifeTime the activeMode
     * @return bool
     */
    public boolean getLifeItemActive() {
        return this.panelItems.getLifeItemActive();
    }
    
    /**
     * get from the LiveItem the porpability
     * @return porbability
     */
    public int getLifeItemPorpability() {
        return this.panelItems.getLifeItemPorpability();
    }
    
    /**
     * get from the MirrorItem the active mode
     * @return bool
     */
    public boolean getMirrorItemActive() {
        return this.panelItems.getMirrorItemActive();
    }
    
    /**
     * get from the MirrorItem the time
     * @return time
     */
    public int getMirrorItemTime() {
        return this.panelItems.getMirrorItemTime();
    }
    
    /**
     * get from the MirrorItem the porpability
     * @return porpability 
     */
    public int getMirrorItemPorpability() {
        return this.panelItems.getMirrorItemPorpability();
    }
    
    /**
     * get from the ReflectingItem the active mode
     * @return bool
     */
    public boolean getReflectingItemActive() {
        return this.panelItems.getReflectingItemActive();
    }
     
    /**
     * get from the ReflectingItem the porpability
     * @return porpability
     */
    public int getReflectingItemPorpability() {
        return this.panelItems.getReflectingItemPorpability();
    }
    
    /**
     * get from the ReflectingItem the time
     * @return time 
     */
    public int getReflectingItemTime() {
        return this.panelItems.getReflectingItemTime();
    }
    
    /**
     * get from the DeflectorItem the active mode
     * @return bool
     */
    public boolean getDeflectorItemActive() {
        return this.panelItems.getDeflectorItemActive();
    }
    
    /**
     * get from the DeflectorItem the propability
     * @return propability
     */
    public int getDeflectorItemPorpability() {
        return this.panelItems.getDeflectorItemPorpability();
    }
    
    /**
     * get from the DeflectorItem the time
     * @return time
     */
    public int getDeflectorItemTime() {
        return this.panelItems.getDeflectorItemTime();
    }
    
    /**
     * get the list of active player weapons
     * @return weapons
     */
    public List<String> getActivePlayerWeapons() {
        return this.panelPlayer.getActiveWeapons();
    }
    
    /**
     * get the information of a doubleKill
     * @return bool
     */
    public boolean isDoubleKillActive() {
        return this.panelAchivements.isDoubleKillActive();
    }
    
    /**
     * get the DoubleKill time
     * @return time
     */
    public int getDoubleKillTime() {
        return this.panelAchivements.getDoubleKillTime();
    }
    
    /**
     * get the DoubleKill points
     * @return points
     */
    public int getDoubleKillPoints() {
        return this.panelAchivements.getDoubleKillPoints();
    }
    
    /**
     * get the triple kill event
     * @return bool
     */
    public boolean isTripleKillActive() {
        return this.panelAchivements.isTripleKillActive();
    }
    
    /**
     * get the triple kill time
     * @return time
     */
    public int getTripleKillTime() {
        return this.panelAchivements.getTripleKillTime();
    }
    
    /**
     * get the triple kill points
     * @return points
     */
    public int getTripleKillPoints() {
        return this.panelAchivements.getTripleKillPoints();
    }
    
    /**
     * get the win points
     * @return points
     */
    public int getPointsForWin() {
        return this.panelWinSituation.getPointsForWin();
    }
    
    /**
     * get the points to win
     * @return points
     */
    public boolean isPointsForWinEnabled() {
        return this.panelWinSituation.isPointsForWinEnabled();
    }
    
    /**
     * get the information of all enemies dead to win
     * @return bool
     */
    public boolean isWinOnAllEnemiesDeadEnabled() {
        return this.panelWinSituation.isWinOnAllEnemiesDeadEnabled();
    }
    
}
