
package spaceinvader.gui.LevelEditor;

import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * class which generate the description (name) of the LevelEditor
 * @author Stefan Fruhwirth
 * @version 1.1
 */
class PanelDescription extends JPanel{

    private final JLabel labelName; 
    private final JTextField textField; 
    
    /**
     * Konstructor
     */
    public PanelDescription() {
        
        this.setLayout(new FlowLayout(30,30,30));
        
        this.labelName = new JLabel("Levelname: "); 
        this.textField = new JTextField(""); 
        this.textField.setColumns(40);
        
        this.add(this.labelName); 
        this.add(this.textField); 
    }
    
    /**
     * get the name of the Level
     * @return levelName
     */
    public String getNameInformation(){
        return this.textField.getText(); 
    }
    
}
