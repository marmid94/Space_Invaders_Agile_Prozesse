
package spaceinvader.gui.LevelEditor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerNumberModel;

/**
 * class which generate the Monster of the LevelEditor
 * @author Stefan Fruhwirth
 * @version 1.1
 */
class PanelMonster extends JPanel{
    
    private final JPanel panel; 
    private final JPanel panelList, panelButton; 
    private final JPanel panelTextField; 
    
    private final JList listMonster;  
    private final JButton buttonAdd, buttonDelete; 
    
    private final JLabel labelName, labelMonsterTyp, labelNumber, labelLive,
                         labelFireRate, labelMunition, labelWave, labelPoints; 
    
    private final JTextField textFieldName, textFieldNumber;    
    private final JComboBox comboBoxMonsterTyp, comboBoxMunition;  
    private final JSlider sliderLive, sliderFireRate; 
    
    private final JSpinner spinnerWave, spinnerPoints; 

    /**
     * Konstructor
     * @param controller
     * @param monster 
     */
    public PanelMonster(LevelEditorController controller, EnemyRow monster){
        
        this.panelList = new JPanel(); 
        this.panelList.setLayout(new BorderLayout(5,5));
        
        this.panelButton = new JPanel(); 
        this.panelButton.setLayout(new GridLayout(1,2,5,5));
        
        this.panelList.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Reihen"),BorderFactory.createEmptyBorder(5, 5, 5, 5)));
          
        this.listMonster = new JList(); 
        this.listMonster.setSelectionBackground(Color.GRAY);
        this.listMonster.setSelectionMode(ListSelectionModel.SINGLE_SELECTION); 
        this.listMonster.setName("ListeMonster");
        this.listMonster.addListSelectionListener(controller);
        
        this.buttonAdd = new JButton("+");
        this.buttonAdd.setActionCommand("buttonAddMonster");
        this.buttonAdd.addActionListener(controller);
        
        this.buttonDelete = new JButton("-"); 
        this.buttonDelete.setActionCommand("buttonDeleteMonster");
        this.buttonDelete.addActionListener(controller);
        
        this.panelButton.add(this.buttonAdd); 
        this.panelButton.add(this.buttonDelete); 
        
        this.panelList.add(this.listMonster, BorderLayout.CENTER);
        this.panelList.add(this.panelButton, BorderLayout.SOUTH); 
        
        /**
         * Panel with Monster Attributes
         */
        this.panelTextField = new JPanel(); 
        this.panelTextField.setLayout(new GridLayout(6,4,20,20));
        
        this.labelName = new JLabel("Reihenname"); 
        this.textFieldName = new JTextField(monster.getName()); 
        this.textFieldName.setActionCommand("TextFieldMonsterName");
        this.textFieldName.setName("TextFieldMonsterName");
        this.textFieldName.addKeyListener(controller);
        
        this.labelMonsterTyp = new JLabel("Monstertyp");
        this.comboBoxMonsterTyp = new JComboBox(); 
        this.comboBoxMonsterTyp.setActionCommand("comboBoxMonsterTyp");
        this.comboBoxMonsterTyp.addItemListener(controller);
        
        this.labelNumber = new JLabel("Anzahl der Monster"); 
        this.textFieldNumber = new JTextField(monster.getNumber()); 
        this.textFieldNumber.setActionCommand("TextFieldMonsterNumber");
        this.textFieldNumber.setName("TextFieldMonsterNumber");
        this.textFieldNumber.addKeyListener(controller);
        
        this.labelLive = new JLabel("Leben der Monster"); 
        this.sliderLive = new JSlider(); 
        this.sliderLive.setName("sliderMonsterLive");
        this.sliderLive.setMinimum(1);
        this.sliderLive.setMaximum(10);
        
        this.sliderLive.setMajorTickSpacing(2);
        this.sliderLive.setMinorTickSpacing(1);
        this.sliderLive.setPaintLabels(true);
        this.sliderLive.setPaintTicks(true);
        
        this.sliderLive.setValue(monster.getLivePoints());
        this.sliderLive.addChangeListener(controller);
        
        this.labelFireRate = new JLabel("Schussrate");
        this.sliderFireRate = new JSlider(); 
        this.sliderFireRate.setName("sliderMonsterFireRate");
        this.sliderFireRate.setMinimum(0);
        this.sliderFireRate.setMaximum(100);
        
        this.sliderFireRate.setMajorTickSpacing(25);
        this.sliderFireRate.setMinorTickSpacing(1);
        this.sliderFireRate.setPaintLabels(true);
        this.sliderFireRate.setPaintTicks(true);
        
        this.sliderFireRate.setValue(monster.getRateOfFire());
        this.sliderFireRate.addChangeListener(controller);
        
        this.labelMunition = new JLabel("Munition"); 
        this.comboBoxMunition = new JComboBox(); 
        this.comboBoxMunition.setActionCommand("comboBoxMunition");
        this.comboBoxMunition.setName("comboBoxMunition");
        this.comboBoxMunition.addItemListener(controller);
        
        this.labelWave = new JLabel("Welle"); 
        this.spinnerWave = new JSpinner(new SpinnerNumberModel(1, 0, 5, 1)); 
        this.spinnerWave.setName("spinnerMonsterWave");
        this.spinnerWave.addChangeListener(controller);
        
        this.labelPoints = new JLabel("Punkte"); 
        this.spinnerPoints = new JSpinner(new SpinnerNumberModel(10, 0, 100000, 10)); 
        this.spinnerPoints.setName("spinnerMonsterPoints");
        this.spinnerPoints.addChangeListener(controller);
        
        this.panelTextField.add(this.labelName);         
        this.panelTextField.add(this.textFieldName);   
        this.panelTextField.add(new JLabel(""));      
        this.panelTextField.add(new JLabel(""));
        
        this.panelTextField.add(this.labelMonsterTyp);
        this.panelTextField.add(this.comboBoxMonsterTyp); 
        this.panelTextField.add(this.labelNumber); 
        this.panelTextField.add(this.textFieldNumber);
        
        this.panelTextField.add(this.labelLive); 
        this.panelTextField.add(this.sliderLive);
        this.panelTextField.add(new JLabel(""));
        this.panelTextField.add(new JLabel(""));
        
        this.panelTextField.add(this.labelFireRate);
        this.panelTextField.add(this.sliderFireRate);
        this.panelTextField.add(new JLabel(""));
        this.panelTextField.add(new JLabel(""));
        
        this.panelTextField.add(this.labelMunition); 
        this.panelTextField.add(this.comboBoxMunition);
        this.panelTextField.add(new JLabel(""));
        this.panelTextField.add(new JLabel(""));
        
        this.panelTextField.add(this.labelWave); 
        this.panelTextField.add(this.spinnerWave);
        this.panelTextField.add(this.labelPoints);
        this.panelTextField.add(this.spinnerPoints);
        
        this.panel = new JPanel(); 
        this.panel.setLayout(new BorderLayout(10,10));
        this.panel.add(this.panelList, BorderLayout.WEST); 
        this.panel.add(this.panelTextField, BorderLayout.CENTER); 
        
        
        this.setLayout(new FlowLayout(30,30,30));
        this.add(this.panel); 
    }
       
    /**
     * set the List(rows) with the monster
     * @param monster 
     */
    public void setListMonster(List<EnemyRow> monster){ 
        
        String a[] = new String[monster.size()]; 
        
        for(int i=0; i<monster.size(); i++){  
            a[i] = monster.get(i).getName();
        }    
        this.listMonster.setListData(a);
    }
    
    public void setSelectedListMonster(int i){
        this.listMonster.setSelectedIndex(i);
    }
    
    /**
     * sets the Monstertyps in the comboBox
     * @param typs 
     */
    public void setMonsterTyps(List<String> typs){   
        
        for (String typ : typs) {
            this.comboBoxMonsterTyp.addItem(typ);
        }
    }
    
    /**
     * set the munition of the Monster
     * @param munition 
     */
    public void setMonsterMunition(List<String> munition){
        for (String munition1 : munition) {
            this.comboBoxMunition.addItem(munition1);  
        }
    }
    
    /**
     * set all attributes to the monster
     * @param monster 
     */
    public void setMonsterAttributs(EnemyRow monster){
    
        this.textFieldName.setText(monster.getName());  
        this.textFieldNumber.setText(String.valueOf(monster.getNumber()));
        this.sliderFireRate.setValue(monster.getRateOfFire());
        this.sliderLive.setValue(monster.getLivePoints());
        this.spinnerPoints.setValue(monster.getPoints());
        this.spinnerWave.setValue(monster.getWave());
        this.comboBoxMonsterTyp.setSelectedItem(monster.getType());
        this.comboBoxMunition.setSelectedItem(monster.getWeapon());
    }  
    
    /**
     * get the text from the textFieldName
     * @return name
     */
    public String getNameFromTextField(){
        return this.textFieldName.getText(); 
    }
    
    /**
     * get the number 
     * @return value
     */
    public int getNumberFromTextField(){
        
        int value = 0; 
        
        try{
            value = Integer.parseInt(this.textFieldNumber.getText());
            this.textFieldNumber.setBackground(Color.WHITE);
        }
        catch(java.lang.NumberFormatException e){
            this.textFieldNumber.setBackground(Color.RED);
        }
           
        return value; 
    }

    
    
}
