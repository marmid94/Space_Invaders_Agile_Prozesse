
package spaceinvader.gui.LevelEditor;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import spaceinvader.model.GameController;
import spaceinvader.model.level.GameLibrary;

/**
 * class which generate the Player of the LevelEditor
 * @author Stefan Fruhwirth
 * @version 1.1
 */
class PanelPlayer extends JPanel{
    
    private final JLabel labelWeapon;
    private final JPanel panel; 
    private final List<JCheckBox> checkBoxesWeapons; 
    private final List<String> weaponNames;
    
    /**
     * Konstructor
     * @param controller
     * @param gameController 
     */
    public PanelPlayer(LevelEditorController controller, GameController gameController){
        
        this.panel = new JPanel(); 
        this.panel.setLayout(new GridLayout(5,4,10,10));
        
        this.weaponNames = new ArrayList<>(GameLibrary.getInstance().getPlayerWeapons().keySet());
        
        this.labelWeapon = new JLabel("Waffengattung"); 
        
        this.panel.add(this.labelWeapon); 
        this.panel.add(new JLabel("")); 
        this.panel.add(new JLabel("")); 
        this.panel.add(new JLabel("")); 
        
        this.checkBoxesWeapons = new ArrayList<>();
        
        for(String weaponName : this.weaponNames) {
            JCheckBox tmpCheckBox = new JCheckBox(); 
            tmpCheckBox.setActionCommand("checkBoxWeapon");
            tmpCheckBox.addActionListener(controller);
            tmpCheckBox.setSelected(true);
            this.checkBoxesWeapons.add(tmpCheckBox);
            this.panel.add(new JLabel("")); 
            this.panel.add(tmpCheckBox); 
            this.panel.add(new JLabel(weaponName));
            this.panel.add(new JLabel(""));
        }
  
        this.setLayout(new FlowLayout(30,30,30)); 
        this.add(this.panel);        
    }
    
    /**
     * get the list of all active weapons
     * @return weapons
     */
    public List<String> getActiveWeapons() {
        List<String> resultList = new LinkedList<>();
        for(int i = 0; i < this.weaponNames.size(); i++) {
            if(this.checkBoxesWeapons.get(i).isSelected()) {
                resultList.add(this.weaponNames.get(i));
            }
        }
        
        return resultList;
    }
    
}
