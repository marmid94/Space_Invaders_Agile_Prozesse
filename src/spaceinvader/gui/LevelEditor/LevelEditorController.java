package spaceinvader.gui.LevelEditor;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import spaceinvader.model.GameController;
import spaceinvader.model.level.GameLibrary; 
import spaceinvader.model.level.LevelSaver;

/**
 * class which generate LevelEditorController for all events
 * @author Stefan Fruhwirth
 * @version 1.1
 */
public class LevelEditorController implements ActionListener, ChangeListener, ListSelectionListener, ItemListener, KeyListener {

    private final List <EnemyRow> listMonster;
    private final List <String> listMonsterTyps; 
    private final List <String> listMunition; 
    
    private JList selectedListItem; 
    private final EnemyRow monster; 
    
    private LevelEditor levelEditor;
    
    private final GameController gameController;
    
    /**
     * Konstructor
     * @param gameController 
     */
    public LevelEditorController(GameController gameController){        
        
        this.gameController = gameController;         
        
        this.listMonsterTyps = new ArrayList<>();        
        this.listMunition = new ArrayList<>();       
        this.listMonster = new ArrayList<>(); 
        
        
        this.listMonsterTyps.addAll(GameLibrary.getInstance().getEnemyImages().keySet()); 
        this.listMunition.addAll(GameLibrary.getInstance().getEnemyWeapons().keySet()); 

        
        this.monster = new EnemyRow(this.listMonsterTyps, this.listMunition); 
        this.listMonster.add(this.monster); 
       
    }

    /**
     * get the LevelEditor
     * @return levelEditor
     */
    public LevelEditor getLevelEditor() {
        return levelEditor;
    }
    
    /**
     * Action Event
     * @param e 
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        
        switch(e.getActionCommand()){
            case "LevelEditor":
                this.levelEditor = new LevelEditor(this, gameController, monster);           
                this.levelEditor.panelMonster.setListMonster(this.listMonster);
                this.levelEditor.panelMonster.setMonsterTyps(this.listMonsterTyps);
                this.levelEditor.panelMonster.setMonsterMunition(this.listMunition);
                
                this.levelEditor.panelMonster.setSelectedListMonster(0);
                break; 
               
            //Monster
            case "buttonAddMonster":
                EnemyRow.counter++;  
                EnemyRow monster = new EnemyRow(this.listMonsterTyps, this.listMunition); 
                this.listMonster.add(monster); 
                this.levelEditor.panelMonster.setListMonster(this.listMonster);
                this.levelEditor.panelMonster.setMonsterAttributs(monster);
                this.levelEditor.panelMonster.setSelectedListMonster(EnemyRow.counter);
                break;
                
            case "buttonDeleteMonster": 
                if(this.selectedListItem != null){
                    int index = this.selectedListItem.getSelectedIndex(); 
                    if(!this.listMonster.isEmpty()){
                        for(int i=0; i< this.listMonster.size(); i++){
                            if(i == index){
                                this.listMonster.remove(index); 
                                this.levelEditor.panelMonster.setListMonster(this.listMonster);
                            }
                        }
                    }
                }
                break;                  
                
            // Safe the settings
            case "buttonSafe":
                LevelSaver saver = new LevelSaver();
                
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setFileFilter(new FileNameExtensionFilter(null, "game"));
                fileChooser.setSelectedFile(new File("Spiel1.game"));
             
                if (fileChooser.showSaveDialog(this.levelEditor) == JFileChooser.APPROVE_OPTION) {
                 
                    try {
                        saver.saveLevelConfig(fileChooser.getSelectedFile(), this);
                        JOptionPane.showMessageDialog(this.levelEditor, "Speichern erfolgreich.");
                        this.levelEditor.setVisible(false);
                        this.levelEditor.dispose();
                        this.levelEditor = null;
                    } 
                    catch (Exception ex) {
                        JOptionPane.showMessageDialog(this.levelEditor, "Speichern fehlgeschlagen.");
                    }
                }
                
                break; 
                
            }
    }
    
   /**
    * Action if a spinner or slider value changed
    * @param e 
    */
    @Override
    public void stateChanged(ChangeEvent e) {
       
        if(JSpinner.class.isInstance(e.getSource())){
            JSpinner spinner = (JSpinner) e.getSource();       
            SpinnerModel spinnerModel = spinner.getModel(); 
            int value = (int) spinnerModel.getValue(); 
       
            switch(spinner.getName()){
                case "spinnerMonsterWave":
                    if(this.selectedListItem != null){
                        int index = this.selectedListItem.getSelectedIndex(); 
                        
                        for(int i=0; i< this.listMonster.size(); i++){
                            if(i == index){ 
                                EnemyRow monster = this.listMonster.get(index); 
                                monster.setWave(value);
                            }
                        }               
                    }
                    break; 
                    
                case "spinnerMonsterPoints":
                    if(this.selectedListItem != null){
                        int index = this.selectedListItem.getSelectedIndex(); 
                        
                        for(int i=0; i< this.listMonster.size(); i++){
                            if(i == index){ 
                                EnemyRow monster = this.listMonster.get(index); 
                                monster.setPoints(value);
                            }
                        }               
                    }
                    break;      
                    
                    
            }
        } 
        
        if(JSlider.class.isInstance(e.getSource())){
            
            JSlider slider = (JSlider) e.getSource(); 
            int value = slider.getValue(); 
            
            switch(slider.getName()){
                
                case "sliderMonsterLive":
                    if(this.selectedListItem != null){
                        int index = selectedListItem.getSelectedIndex();
                        for(int i=0; i<this.listMonster.size(); i++){
                            if(i == index){
                                EnemyRow monster = this.listMonster.get(index); 
                                monster.setLivePoints(value);
                            }
                        }
                     }
                    break;
                    
                case "sliderMonsterFireRate":
                    if(this.selectedListItem != null){
                        int index = selectedListItem.getSelectedIndex();
                        for(int i=0; i<this.listMonster.size(); i++){
                            if(i == index){
                                EnemyRow monster = this.listMonster.get(index); 
                                monster.setRateOfFire(value);
                            }
                        }
                    }
                    break; 
            }
            
        }
               
    }

    /**
     * event if a ListItem is selected 
     * @param e 
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
             
        this.selectedListItem = (JList)e.getSource();
        int index = selectedListItem.getSelectedIndex();
        
        for(int i=0; i<this.listMonster.size(); i++){
            if(i == index){
                //set the Monster to the GUI-Elements
                this.levelEditor.panelMonster.setMonsterAttributs(this.listMonster.get(index));
            }
        }
        
    }

    /**
     * event if a ListItem is selected, changed new list
     * @param e 
     */
    @Override
    public void itemStateChanged(ItemEvent e) {
        
        if(e.getStateChange() == ItemEvent.SELECTED)
        {
            String item = (String) e.getItem();
            
            for (int i=0; i< listMonsterTyps.size(); i++) {
                
                if (item == this.listMonsterTyps.get(i)) {
                    
                    if(this.selectedListItem != null){
                        
                        if(!this.selectedListItem.isSelectionEmpty()){

                            int index = selectedListItem.getSelectedIndex();

                            for(int j=0; j<this.listMonster.size(); j++){
                                if(j == index){
                                    EnemyRow monster = this.listMonster.get(index); 
                                    monster.setTyp(item);
                                }
                            }
                        }
                    }
                }
            }    
            
            for (int i=0; i<this.listMunition.size(); i++) {
                
                if (item == this.listMunition.get(i)) {
                    
                    if(this.selectedListItem != null){
                        
                        if(!this.selectedListItem.isSelectionEmpty()){

                            int index = selectedListItem.getSelectedIndex();

                            for(int j=0; j<this.listMonster.size(); j++){
                                if(j == index){
                                    EnemyRow monster = this.listMonster.get(index); 
                                    monster.setWeapon(item);
                                }
                            }
                        }
                    }
                }
            }
        }        
        
    }

    @Override
    public void keyTyped(KeyEvent e) {}

    @Override
    public void keyPressed(KeyEvent e) {}

    /**
     * event for the Textfields
     * @param e 
     */
    @Override
    public void keyReleased(KeyEvent e) {
        
        JTextField textField = (JTextField) e.getSource();
        
        switch(textField.getName()){
                            
            case "TextFieldMonsterName":
                if(this.selectedListItem != null){
                    int index = selectedListItem.getSelectedIndex();
                    for(int i=0; i<this.listMonster.size(); i++){
                        if(i == index){
                            EnemyRow monster = this.listMonster.get(index); 
                            monster.setName(this.levelEditor.panelMonster.getNameFromTextField());
                            this.levelEditor.panelMonster.setListMonster(this.listMonster);  
                            this.levelEditor.panelMonster.setSelectedListMonster(index);
                        }
                    }
                }
                break; 
                
            case "TextFieldMonsterNumber":
                if(this.selectedListItem != null){
                    int index = selectedListItem.getSelectedIndex();
                    for(int i=0; i<this.listMonster.size(); i++){
                        if(i == index){
                            EnemyRow monster = this.listMonster.get(index); 
                            monster.setNumber(this.levelEditor.panelMonster.getNumberFromTextField());                  
                            this.levelEditor.panelMonster.setSelectedListMonster(index);
                        }
                   }
                }
                break; 
        } 
    }
    
    /**
     * get the levelName
     * @return levelName
     */
    public String getLevelName() {
        return this.getLevelEditor().getLevelName();
    }
    
    /**
     * get the List of EnemyRows
     * @return enemyRow
     */
    public List<EnemyRow> getEnemyRows() {
        return this.listMonster;
    }
    
    /**
     * get from the PointItem the active mode
     * @return bool
     */
    public boolean getPointItemActive() {
        return this.getLevelEditor().getPointItemActive();
    }
    
    /**
     * get from the PointItem the porpability
     * @return porpability
     */
    public int getPointItemPorpability() {
        return this.getLevelEditor().getPointItemPorpability();
    }
    
    /**
     * get from the PointItem the points
     * @return points
     */
    public int getPointItemPoints() {
        return this.getLevelEditor().getPointItemPoints();
    }
    
    /**
     * get from the LifeItem the active mode
     * @return bool
     */
    public boolean getLifeItemActive() {
        return this.getLevelEditor().getLifeItemActive();
    }
    
    /**
     * get from the LifeItem the porpability
     * @return porpability
     */
    public int getLifeItemPorpability() {
        return this.getLevelEditor().getLifeItemPorpability();
    }
    
    /**
     * get form the MirrorItem the acive mode
     * @return bool
     */
    public boolean getMirrorItemActive() {
        return this.getLevelEditor().getMirrorItemActive();
    }
    
    /**
     * get from the MirrorItem the propability
     * @return porpabiliy
     */
    public int getMirrorItemPorpability() {
        return this.getLevelEditor().getMirrorItemPorpability();
    }
    
    /**
     * get from the MirrorItem the Time
     * @return time
     */
    public int getMirrorItemTime() {
        return this.getLevelEditor().getMirrorItemTime();
    }
    
    /**
     * get from the ReflectingItem the active mode
     * @return bool
     */
    public boolean getReflectingItemActive() {
        return this.getLevelEditor().getReflectingItemActive();
    }
    
    /**
     * get from the ReflectingItem the propability
     * @return porpability
     */
    public int getReflectingItemPorpability() {
        return this.getLevelEditor().getReflectingItemPorpability();
    }
    
    /**
     * get from the ReflectingItem the time
     * @return time
     */
    public int getReflectingItemTime() {
        return this.getLevelEditor().getReflectingItemTime();
    }
    
    /**
     * get from the DeflectorItem the active mode
     * @return bool
     */
    public boolean getDeflectorItemActive() {
        return this.getLevelEditor().getDeflectorItemActive();
    }
    
    /**
     * get from the DeflectorItem the porpability
     * @return porpability
     */
    public int getDeflectorItemPorpability() {
        return this.getLevelEditor().getDeflectorItemPorpability();
    }
    
    /**
     * get from the DeflectorItem the time
     * @return time
     */
    public int getDeflectorItemTime() {
        return this.getLevelEditor().getDeflectorItemTime();
    }
   
    /**
     * get a list of active player weapons
     * @return weapons
     */
    public List<String> getActivePlayerWeapons() {
        return this.getLevelEditor().getActivePlayerWeapons();
    }
    
    /**
     * get the information if it is a double kill
     * @return bool
     */
    public boolean isDoubleKillActive() {
        return this.getLevelEditor().isDoubleKillActive();
    }
    
    /**
     * get the double kill time
     * @return time 
     */
    public int getDoubleKillTime() {
        return this.getLevelEditor().getDoubleKillTime();
    }
    
    /**
     * get the double kill points
     * @return points
     */
    public int getDoubleKillPoints() {
        return this.getLevelEditor().getDoubleKillPoints();
    }
    
    /**
     * get the information if the triple kill is active
     * @return bool
     */
    public boolean isTripleKillActive() {
        return this.getLevelEditor().isTripleKillActive();
    }
    
    /**
     * get the triple kill time
     * @return time
     */
    public int getTripleKillTime() {
        return this.getLevelEditor().getTripleKillTime();
    }
    
    /**
     * get the triple kill points
     * @return points
     */
    public int getTripleKillPoints() {
        return this.getLevelEditor().getTripleKillPoints();
    }
    
    /**
     * get the points to win
     * @return points
     */
    public int getPointsForWin() {
        return this.getLevelEditor().getPointsForWin();
    }
    
    /**
     * get the information if points for win is active
     * @return bool
     */
    public boolean isPointsForWinEnabled() {
        return this.getLevelEditor().isPointsForWinEnabled();
    }
    
    /**
     * get the information if the option to win is active
     * win option, kill all enemies
     * @return bool
     */
    public boolean isWinOnAllEnemiesDeadEnabled() {
        return this.getLevelEditor().isWinOnAllEnemiesDeadEnabled();
    }
    
}