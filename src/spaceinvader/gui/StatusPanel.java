package spaceinvader.gui;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 * class which generate the StatusPanel of the MainFrame
 * @author Stefan Fruhwirth
 * @version 1.1
 */

public class StatusPanel extends JPanel implements ActionListener {

    private final JLabel labelStatus; 

    private boolean isStatusFixed;
    private boolean holdStatus;
    private int statusTime;
    private final Timer timer;
    private Color statusColor, statusTextColor;
    
    private static final int MAX_STATUS_TIME = 200;
    
    public static final Color DEFAULT_STATUS_COLOR = Color.DARK_GRAY;
    public static final Color DEFAULT_STATUS_TEXT_COLOR = Color.WHITE;

    /**
     * Konstructor
     */
    public StatusPanel() {
        this.timer = new Timer(10, this);
        this.statusTime = 0;
        this.isStatusFixed = false;
        this.holdStatus = false;
    
        this.setLayout(new BorderLayout());

        this.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createTitledBorder(""),
                BorderFactory.createEmptyBorder(5,5,5,5))
                
        );
        
        this.labelStatus = new JLabel("");
        this.labelStatus.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        this.labelStatus.setFont(new Font("Arial", Font.BOLD, 16));
        
        this.add(this.labelStatus);
        
        this.setVisible(false);
        this.setOpaque(false);
        
        timer.start();
    }
    
    /**
     * get the event to repaint
     * @param g 
     */
     @Override
    public void paint(Graphics g) {
        g.setColor( getBackground() );
        g.fillRect(10, 10, getWidth() - 20, getHeight() - 20);
        //super.paintComponent(g);
        super.paintChildren(g);
    }
 
    /**
     * set the status on the label
     * @param text 
     * @param isFixed 
     * @param holdStatus 
     * @param statusColor 
     * @param statusTextColor 
     */
    public void setStatus(String text, boolean isFixed, boolean holdStatus, Color statusColor, Color statusTextColor){
        if(holdStatus || !this.holdStatus) {
            this.statusTime = MAX_STATUS_TIME;
            this.statusColor = statusColor;
            this.statusTextColor = statusTextColor;
            this.labelStatus.setText(text);
            
            this.labelStatus.setForeground(
                new Color(statusTextColor.getRed(), statusTextColor.getGreen(), statusTextColor.getBlue(), 255));

            this.setBackground(
                    new Color(statusColor.getRed(), statusColor.getGreen(), statusColor.getBlue(), 255));

            this.isStatusFixed = isFixed;
            this.holdStatus = holdStatus;

            this.setVisible(true);
        }
      
    }

    /**
     * get the timer event to change the background and foreground of the panel
     * @param e 
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == this.timer) {
            if(!this.isStatusFixed) {
                if(this.statusTime > 0) {
                    this.statusTime--;
                    
                    this.setBackground(
                        new Color(statusColor.getRed(), statusColor.getGreen(), statusColor.getBlue(),
                            (statusTime * 255) / MAX_STATUS_TIME));
                                        
                    this.labelStatus.setForeground(
                        new Color(statusTextColor.getRed(), statusTextColor.getGreen(), statusTextColor.getBlue(), 
                            (statusTime * 255) / MAX_STATUS_TIME));     
                }
                else {
                    this.statusTime = 0;
                    this.holdStatus = false;
                    this.labelStatus.setText("");
                    this.setVisible(false);
                }
            }
            else {
                this.holdStatus = false;
            }
        }
    }
    
}
