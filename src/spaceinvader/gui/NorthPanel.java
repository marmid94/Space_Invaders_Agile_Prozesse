package spaceinvader.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * class which generate the NorthPanel of the MainFrame
 * @author Stefan Fruhwirth
 * @version 1.1
 */
public class NorthPanel extends JPanel {
    
    private final JLabel labelLevelValue; 
    private final JPanel panelLevel;
    private final StatusPanel panelStatus; 

    /**
     * Konstructor
     */
    public NorthPanel(){
           
        this.setBackground(Color.BLACK); 
        this.setLayout(new BorderLayout());
    
        this.panelStatus = new StatusPanel(); 
        this.add(this.panelStatus, BorderLayout.CENTER);  
        
        this.panelLevel = new JPanel();
        this.panelLevel.setBackground(Color.BLACK);
        this.panelLevel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        
        this.panelLevel.setLayout(new GridLayout(1,1));
        this.panelLevel.setPreferredSize(new Dimension(80,50));
          
        this.labelLevelValue = new JLabel();       
        this.labelLevelValue.setForeground(Color.WHITE);
        this.labelLevelValue.setFont(new Font("Arial", Font.BOLD, 16));
        
        this.panelLevel.add(this.labelLevelValue);  
        
        this.add(this.panelLevel, BorderLayout.EAST);  
        
    }

    /**
     * set the actuell level
     * @param level 
     */
    public void setLevel(int level){
        this.labelLevelValue.setText("Welle " + String.valueOf(level));
    }
    
    /**
     * set reset the level
     */
    public void setLevel(){
        this.labelLevelValue.setText("");
    }
    
    /**
     * set the Status for information
     * @param text 
     */
    public void setStatus(String text){
        this.setStatus(text, false, false, StatusPanel.DEFAULT_STATUS_COLOR, StatusPanel.DEFAULT_STATUS_TEXT_COLOR);
    }
    
    /**
     * set the status for information
     * @param text
     * @param isFixed
     * @param holdStatus 
     */
    public void setStatus(String text, boolean isFixed, boolean holdStatus){
        this.setStatus(text, isFixed, holdStatus, StatusPanel.DEFAULT_STATUS_COLOR, StatusPanel.DEFAULT_STATUS_TEXT_COLOR);
    }
    
    /**
     * set the status on the label
     * @param text 
     */
    public void setStatus(String text, boolean isFixed, boolean holdStatus, Color statusColor, Color statusTextColor){
        this.panelStatus.setStatus(text, isFixed, holdStatus, statusColor, statusTextColor);    
    }
 
}
