
package spaceinvader.gui;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * class which generate the FeatureFrame of the Menu
 * to change the keys for control
 * @author Stefan Fruhwirth
 * @version 1.1
 */
public class FeatureFrame extends JFrame implements KeyListener {
    
    private final JTextField textFieldControlLeft1, textFieldControlRight1, textFieldControlShoot1, textFieldControlWeapon1;
    
    private final JTextField textFieldControlLeft2, textFieldControlRight2, textFieldControlShoot2, textFieldControlWeapon2;
    
    private final JTextField textFieldControlPlayPause, textFieldControlRespawn;
    
    private final JButton buttonExit, buttonOkey; 
    
    private int left1, right1, shoot1, weapon1; 
    private int left2, right2, shoot2, weapon2;
    private int playPause, respawn;
    
    /**
     * Konstructor
     * @param controller
     * @param left1
     * @param right1
     * @param shoot1
     * @param weapon1
     * @param left2
     * @param right2
     * @param shoot2
     * @param weapon2
     * @param playPause
     * @param respawn 
     */
    public FeatureFrame(GUIController controller, 
            int left1, int right1, int shoot1, int weapon1,
            int left2, int right2, int shoot2, int weapon2,
            int playPause, int respawn) {
        
        //init key code values
        this.left1 = left1; 
        this.right1 = right1; 
        this.shoot1 = shoot1; 
        this.weapon1 = weapon1;
        
        this.left2 = left2; 
        this.right2 = right2; 
        this.shoot2 = shoot2; 
        this.weapon2 = weapon2;
        
        this.playPause = playPause;
        this.respawn = respawn;       
        
        //init the panel
        JPanel panelControl1 = new JPanel(); 
        JPanel panelControl2 = new JPanel(); 
        JPanel panelControl3 = new JPanel(); 
        JPanel panelControl4 = new JPanel(); 
        panelControl1.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createTitledBorder("Spieler 1"),
                BorderFactory.createEmptyBorder(5, 5, 5, 5)));
        panelControl2.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createTitledBorder("Spieler 2"),
                BorderFactory.createEmptyBorder(5, 5, 5, 5)));
        panelControl3.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createTitledBorder("Allgemein"),
                BorderFactory.createEmptyBorder(5, 5, 5, 5)));
        
        panelControl1.setLayout(new GridLayout(4,2,5,5));
        panelControl2.setLayout(new GridLayout(4,2,5,5));
        panelControl3.setLayout(new GridLayout(2,2,5,5));
        panelControl4.setLayout(new FlowLayout(10, 10, 10));
            
        
        //init text fields
        this.textFieldControlLeft1 = new JTextField();
        this.textFieldControlLeft1.setText(KeyEvent.getKeyText(this.left1));
        this.textFieldControlLeft1.addKeyListener(this);
        
        this.textFieldControlRight1 = new JTextField();
        this.textFieldControlRight1.setText(KeyEvent.getKeyText(this.right1));
        this.textFieldControlRight1.addKeyListener(this);
        
        this.textFieldControlShoot1 = new JTextField(); 
        this.textFieldControlShoot1.setText(KeyEvent.getKeyText(this.shoot1));
        this.textFieldControlShoot1.addKeyListener(this);
        
        this.textFieldControlWeapon1 = new JTextField(); 
        this.textFieldControlWeapon1.setText(KeyEvent.getKeyText(this.weapon1));
        this.textFieldControlWeapon1.addKeyListener(this);
        
        panelControl1.add(new JLabel("Links")); 
        panelControl1.add(this.textFieldControlLeft1);
        
        panelControl1.add(new JLabel("Rechts")); 
        panelControl1.add(this.textFieldControlRight1); 
        
        panelControl1.add(new JLabel("Schießen")); 
        panelControl1.add(this.textFieldControlShoot1); 
        
        panelControl1.add(new JLabel("Waffe wchseln")); 
        panelControl1.add(this.textFieldControlWeapon1); 
        
        
        this.textFieldControlLeft2 = new JTextField();
        this.textFieldControlLeft2.setText(KeyEvent.getKeyText(this.left2));
        this.textFieldControlLeft2.addKeyListener(this);
        
        this.textFieldControlRight2 = new JTextField();
        this.textFieldControlRight2.setText(KeyEvent.getKeyText(this.right2));
        this.textFieldControlRight2.addKeyListener(this);
        
        this.textFieldControlShoot2 = new JTextField(); 
        this.textFieldControlShoot2.setText(KeyEvent.getKeyText(this.shoot2));
        this.textFieldControlShoot2.addKeyListener(this);
        
        this.textFieldControlWeapon2 = new JTextField(); 
        this.textFieldControlWeapon2.setText(KeyEvent.getKeyText(this.weapon2));
        this.textFieldControlWeapon2.addKeyListener(this);
        
        panelControl2.add(new JLabel("Links")); 
        panelControl2.add(this.textFieldControlLeft2);
        
        panelControl2.add(new JLabel("Rechts")); 
        panelControl2.add(this.textFieldControlRight2); 
        
        panelControl2.add(new JLabel("Schießen")); 
        panelControl2.add(this.textFieldControlShoot2); 
        
        panelControl2.add(new JLabel("Waffe wchseln")); 
        panelControl2.add(this.textFieldControlWeapon2); 
        
        
        
        this.textFieldControlPlayPause = new JTextField(); 
        this.textFieldControlPlayPause.setText(KeyEvent.getKeyText(this.playPause));
        this.textFieldControlPlayPause.addKeyListener(this);
        
        this.textFieldControlRespawn = new JTextField(); 
        this.textFieldControlRespawn.setText(KeyEvent.getKeyText(this.respawn));
        this.textFieldControlRespawn.addKeyListener(this);
        
        panelControl3.add(new JLabel("Starten / Pause")); 
        panelControl3.add(this.textFieldControlPlayPause); 
        
        panelControl3.add(new JLabel("Respawn")); 
        panelControl3.add(this.textFieldControlRespawn); 
        
        
        this.buttonOkey = new JButton("OK");
        this.buttonOkey.setBackground(Color.GREEN);
        this.buttonOkey.setActionCommand("OK_Feature");
        this.buttonOkey.addActionListener(controller);
        
        this.buttonExit = new JButton("Exit");
        this.buttonExit.setBackground(Color.red);
        this.buttonExit.setActionCommand("Exit_Feature");
        this.buttonExit.addActionListener(controller);
        
        panelControl4.add(this.buttonOkey);
        panelControl4.add(this.buttonExit);
  
        this.setLayout(new FlowLayout());
        this.add(panelControl1); 
        this.add(panelControl2); 
        this.add(panelControl3); 
        this.add(panelControl4); 
                
        this.pack(); 
        this.setVisible(true);      
    }

    /** 
     * get the keyLeft for player 1
     * @return key
     */
    public int getKeyLeft1(){
        return this.left1; 
    }
    
    /**
     * get the keyRight for player 1
     * @return key
     */
    public int getKeyRight1(){
        return this.right1; 
    }
    
    /**
     * get the key for shoot player 1
     * @return key
     */
    public int getKeyShoot1(){
        return this.shoot1; 
    }
    
    /**
     * get the key for change weapon of player 1
     * @return key
     */
    public int getKeyWeapon1(){
        return this.weapon1; 
    }
    
    /**
     * get the keyLeft for player 2
     * @return key
     */
    public int getKeyLeft2(){
        return this.left2; 
    }
    
    /**
     * get the keyRight for player 2
     * @return key
     */
    public int getKeyRight2(){
        return this.right2; 
    }
    
    /**
     * get the key for shooting player 2
     * @return key
     */
    public int getKeyShoot2(){
        return this.shoot2; 
    }
    
    /**
     * get the key for change weapon player 2
     * @return 
     */
    public int getKeyWeapon2(){
        return this.weapon2; 
    }
    
    /**
     * get the key to pause the game
     * @return 
     */
    public int getKeyPlayPause(){
        return this.playPause; 
    }
    
    /**
     * get the key to restart the game
     * @return key
     */
    public int getKeyRespawn(){
        return this.respawn; 
    }
    
    /**
     * event if the key is typed
     * @param e 
     */
    @Override
    public void keyTyped(KeyEvent e) {
        e.consume();
    }

    /**
     * event for the key is pressed
     * @param e 
     */
    @Override
    public void keyPressed(KeyEvent e) {
        
        
        if(e.getSource() == this.textFieldControlLeft1) {
            this.textFieldControlLeft1.setText(KeyEvent.getKeyText(e.getKeyCode()));
            this.left1 = e.getKeyCode();
        }
        else if(e.getSource() == this.textFieldControlRight1) {
            this.textFieldControlRight1.setText(KeyEvent.getKeyText(e.getKeyCode()));
            this.right1 = e.getKeyCode();
        }
        else if(e.getSource() == this.textFieldControlShoot1) {
            this.textFieldControlShoot1.setText(KeyEvent.getKeyText(e.getKeyCode()));
            this.shoot1 = e.getKeyCode();
        }
        else if(e.getSource() == this.textFieldControlWeapon1) {
            this.textFieldControlWeapon1.setText(KeyEvent.getKeyText(e.getKeyCode()));
            this.weapon1 = e.getKeyCode();
        }
        else if(e.getSource() == this.textFieldControlLeft2) {
            this.textFieldControlLeft2.setText(KeyEvent.getKeyText(e.getKeyCode()));
            this.left2 = e.getKeyCode();
        }
        else if(e.getSource() == this.textFieldControlRight2) {
            this.textFieldControlRight2.setText(KeyEvent.getKeyText(e.getKeyCode()));
            this.right2 = e.getKeyCode();
        }
        else if(e.getSource() == this.textFieldControlShoot2) {
            this.textFieldControlShoot2.setText(KeyEvent.getKeyText(e.getKeyCode()));
            this.shoot2 = e.getKeyCode();
        }
        else if(e.getSource() == this.textFieldControlWeapon2) {
            this.textFieldControlWeapon2.setText(KeyEvent.getKeyText(e.getKeyCode()));
            this.weapon2 = e.getKeyCode();
        }
        else if(e.getSource() == this.textFieldControlPlayPause) {
            this.textFieldControlPlayPause.setText(KeyEvent.getKeyText(e.getKeyCode()));
            this.playPause = e.getKeyCode();
        }
        else if(e.getSource() == this.textFieldControlRespawn) {
            this.textFieldControlRespawn.setText(KeyEvent.getKeyText(e.getKeyCode()));
            this.respawn = e.getKeyCode();
        }
        
        e.consume();
    }

    /**
     * event if the key is released
     * @param e 
     */
    @Override
    public void keyReleased(KeyEvent e) {
        e.consume();
    }
}
