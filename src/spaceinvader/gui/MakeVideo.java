
package spaceinvader.gui;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.swing.Timer;

import org.jcodec.api.SequenceEncoder;

/**
 * class which generate Video Mode
 * @author Stefan Fruhwirth
 * @version 1.1
 */
public class MakeVideo implements ActionListener{
    
    private final MainFrame frame;     
    private SequenceEncoder enc;
    
    private Timer timer;
    
    /**
     * Konstructor
     * @param frame 
     */
    public MakeVideo(MainFrame frame){
        
        this.frame = frame;   
        this.timer = new Timer(100, this); 
    }
    
    /**
     * starting recording video
     * @param path 
     */
    public void startMakePicture(String path){        
        try {
            //System.out.println(path);  
            enc = new SequenceEncoder(new File(path));        
            this.timer.start();
        } catch (IOException ex) { System.out.println("IO Path");}
    }
    
    /**
     * safe the one picture
     */
    private void safePicture(){
                
        BufferedImage bi = new BufferedImage(this.frame.getWidth(), this.frame.getHeight(), BufferedImage.TYPE_INT_RGB);       
        
        this.frame.paint(bi.getGraphics());
               
        try {           
            enc.encodeNativeFrame(AWTUtil.fromBufferedImage(bi));
        } 
        catch (NullPointerException | IOException ex) { }
    }
    
    /**
     * generate the video of the pictures
     */
    public void makeVideo(){  
              
        try {
            this.timer.stop();
            enc.finish();          
        } 
        catch (IOException | NullPointerException ex) {  }
    }

    /**
     * event of the timer for making a picture
     * @param e 
     */
    @Override
    public void actionPerformed(ActionEvent e) {
    
        if (e.getSource().equals(this.timer)) {
            this.safePicture();
        } 
    }
}