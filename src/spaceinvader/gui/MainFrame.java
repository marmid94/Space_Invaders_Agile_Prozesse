
package spaceinvader.gui;

import java.awt.Dimension;
import java.awt.Rectangle;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import spaceinvader.gui.LevelEditor.LevelEditorController;
import spaceinvader.model.*;

/**
 * class which generate the MainFrame of the Game
 * @author Stefan Fruhwirth
 * @version 1.1
 */
public class MainFrame extends JFrame{

    private final GUIController controller;
    private final LevelEditorController levelController; 
    
    private final NorthPanel headerPanel;
    private final MiddlePanel middlePanel;
    private final SouthPanel southPanel; 
    
    private JMenu fileMenu, changeMenu, helpMenu, videoMenu;
    
    private JMenuItem menuLevel1, menuLevel2, menuLevel3; 
    private JMenuItem menuItemExit, menuItemNew;
    private JMenuItem menuItemLevel, menuItemControl; 
    private JMenuItem menuItemInfo, menuItemHelp;     
    private JMenuItem menuModus, subMenuPlayer1,  subMenuPlayer2, subMenuKI; 
   
    private JMenuItem menuStartVideo, menuExitVideo; 
    
    private JCheckBox checkBoxPlayer1, checkBoxPlayer2, checkBoxKI; 
    
    /**
     * Konstructor
     * @param gameController 
     */
    public MainFrame(GameController gameController){
          
        this.controller = new GUIController(this, gameController);
        gameController.getNotificationManager().addGameListener(controller);
        gameController.getNotificationManager().addRepaintListener(controller);
        
        SoundController sound = new SoundController(gameController);
        gameController.getNotificationManager().addGameListener(sound);
        
        
        this.levelController = new LevelEditorController(gameController);
   
        this.setJMenuBar(this.createMenubar());
        this.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        this.setLayout(null);
        this.headerPanel = new NorthPanel();         
        this.middlePanel = new MiddlePanel(gameController.getWorld()); 
        this.southPanel = new SouthPanel(gameController);
   
        this.headerPanel.setBounds(new Rectangle(gameController.getWorld().getWidth(), 
                50));
        this.add(this.headerPanel); 
        
        this.middlePanel.setBounds(new Rectangle(
                0,
                50,
                gameController.getWorld().getWidth(), 
                gameController.getWorld().getHeight()));
        this.add(this.middlePanel); 
        
        this.southPanel.setBounds(new Rectangle(
                0,
                50 + gameController.getWorld().getHeight(),
                gameController.getWorld().getWidth(), 
                110));
        this.add(this.southPanel);
        
        this.setPreferredSize(new Dimension(gameController.getWorld().getWidth(), 
                gameController.getWorld().getHeight() + 160));
        this.setResizable(false);

        this.setVisible(true);
        this.pack();
        
    }

    /**
     * get the HeaderPanel
     * @return headerPanel
     */
    public NorthPanel getHeaderPanel() {
        return this.headerPanel;
    }

    /**
     * get the MiddlePanel
     * @return middlePanel
     */
    public MiddlePanel getMiddlePanel() {
        return this.middlePanel;
    }

    /**
     * get the SouthPanel
     * @return southPanel
     */
    public SouthPanel getSouthPanel() {
        return this.southPanel;
    }

    /**
     * generates the complete menu
     * @return menu
     */
    private JMenuBar createMenubar() {
        
        JMenuBar menuBar = new JMenuBar();
        menuBar.add(createFileMenu());
        menuBar.add(createChangeMenu());
        menuBar.add(createHelpMenu());
        menuBar.add(createVideoMenu());
 
        return menuBar;   
    }
    
    /**
     * generates the Menu for fileMenu
     * @return fileMenu
     */
    private JMenu createFileMenu(){
        //submenu file
        this.fileMenu = new JMenu("Datei");
        
        //menuItem 
        this.menuItemNew = new JMenu("Neu");
        
        this.menuLevel1 = new JMenuItem("Spiel 1"); 
        this.menuLevel1.setName("Game 1"); 
        this.menuLevel1.setActionCommand("Game1");
        this.menuLevel1.addActionListener(this.controller);
        this.menuItemNew.add(this.menuLevel1);  
        
        this.menuLevel2 = new JMenuItem("Spiel 2"); 
        this.menuLevel2.setName("Game 2"); 
        this.menuLevel2.setActionCommand("Game2");
        this.menuLevel2.addActionListener(this.controller);
        this.menuItemNew.add(this.menuLevel2);  

        this.menuLevel3 = new JMenuItem("Spiel öffnen"); 
        this.menuLevel3.setName("Load Game"); 
        this.menuLevel3.setActionCommand("Load Game");
        this.menuLevel3.addActionListener(this.controller);
        this.menuItemNew.add(this.menuLevel3);
        
        this.fileMenu.add(this.menuItemNew);
        
        //menuitem exit
        this.menuItemExit = new JMenuItem("Beenden");
        this.menuItemExit.setName("Exit");
        this.menuItemExit.setActionCommand("Exit");
        this.menuItemExit.addActionListener(this.controller);
        this.fileMenu.add(this.menuItemExit);
        
        return this.fileMenu; 
    }
    
    /**
     * generates the Menu for changes
     * @return changeMenu
     */
    private JMenu createChangeMenu(){
        
        //submenu file
        this.changeMenu = new JMenu("Einstellungen");
        
        //menuItem 
        this.menuItemControl = new JMenuItem("Steuerung");
        this.menuItemControl.setName("Control");
        this.menuItemControl.setActionCommand("Control");
        this.menuItemControl.addActionListener(this.controller);
        this.changeMenu.add(this.menuItemControl);
        
        //menuitem exit
        this.menuItemLevel = new JMenuItem("LevelEditor");
        this.menuItemLevel.setName("LevelEditor");
        this.menuItemLevel.setActionCommand("LevelEditor");
        this.menuItemLevel.addActionListener(this.levelController);
        this.changeMenu.add(this.menuItemLevel);
        
        this.menuModus = new JMenu("Modus"); 
        
        this.subMenuPlayer1 = new JMenuItem("                    ");         
        this.subMenuPlayer2 = new JMenuItem("                    ");
        this.subMenuKI = new JMenuItem("                    ");
        
        this.checkBoxPlayer1 = new JCheckBox("Einzelspieler"); 
        this.checkBoxPlayer1.setSelected(true);
        this.checkBoxPlayer1.setActionCommand("Player1");
        this.checkBoxPlayer1.addActionListener(this.controller);
        this.subMenuPlayer1.add(this.checkBoxPlayer1); 

        this.checkBoxPlayer2 = new JCheckBox("Mehrspieler");
        this.checkBoxPlayer2.setName("Spieler2");
        this.checkBoxPlayer2.setActionCommand("Player2");
        this.checkBoxPlayer2.addActionListener(this.controller);
        this.subMenuPlayer2.add(this.checkBoxPlayer2); 
        
        this.checkBoxKI = new JCheckBox("KI");
        this.checkBoxKI.setName("KI");
        this.checkBoxKI.setActionCommand("KI");
        this.checkBoxKI.addActionListener(this.controller);
        this.subMenuKI.add(this.checkBoxKI); 
        
        this.menuModus.add(this.subMenuPlayer1); 
        this.menuModus.add(this.subMenuPlayer2); 
        this.menuModus.add(this.subMenuKI); 
             
        this.changeMenu.add(this.menuModus); 
             
        return this.changeMenu;
    }
    
    /**
     * generate the Menu for helping
     * @return helpMenu
     */
    private JMenu createHelpMenu(){
        
        //submenu file
        this.helpMenu = new JMenu("Über");
        
        //menuItem 
        this.menuItemInfo = new JMenuItem("Info");
        this.menuItemInfo.setName("Info");
        this.menuItemInfo.setActionCommand("Info");
        this.menuItemInfo.addActionListener(this.controller);
        this.helpMenu.add(this.menuItemInfo);
        
        //menuitem exit
        this.menuItemHelp = new JMenuItem("Hilfe");
        this.menuItemHelp.setName("Help");
        this.menuItemHelp.setActionCommand("Help");
        this.menuItemHelp.addActionListener(this.controller);
        this.helpMenu.add(this.menuItemHelp);
        
        return this.helpMenu; 
    }
    
    /**
     * creates the Video Menu
     * @return videoMenu
     */
    private JMenu createVideoMenu(){
        this.videoMenu = new JMenu("Video");
        
        this.menuStartVideo = new JMenuItem("Start");
        this.menuStartVideo.setName("startVideo");
        this.menuStartVideo.setActionCommand("startVideo");
        this.menuStartVideo.addActionListener(this.controller);
        this.videoMenu.add(this.menuStartVideo); 
        
        this.menuExitVideo = new JMenuItem("Beenden");
        this.menuExitVideo.setName("exitVideo");
        this.menuExitVideo.setActionCommand("exitVideo");
        this.menuExitVideo.addActionListener(this.controller);
        this.videoMenu.add(this.menuExitVideo); 
        
        return this.videoMenu; 
       
    }
    
    /**
     * set the CheckBoxOnePlayer option
     * @param a 
     */
    public void setCheckBoxOnePlayer(boolean a){
        this.checkBoxPlayer1.setSelected(a);
    }
    
    /**
     * get the CheckBox of one player option
     * @return bool
     */
    public boolean getCheckBoxOnePlayer(){
        return this.checkBoxPlayer1.isSelected();
    }
    
    /**
     * set the checkBox of the two player option
     * @param a 
     */
    public void setCheckBoxTwoPlayer(boolean a){
        this.checkBoxPlayer2.setSelected(a);
    }
    
    /**
     * get the checkbox information of the two player mode
     * @return bool
     */
    public boolean getCheckBoxTwoPlayer(){
        return this.checkBoxPlayer2.isSelected(); 
    }
    
    /**
     * set the checkbox of the computer mode
     * @param a 
     */
    public void setCheckBoxKI(boolean a){
        this.checkBoxKI.setSelected(a);
    }
    
    /**
     * gets the checkbox information if computer mode is active
     * @return bool
     */
    public boolean getCheckBoxKI(){
        return this.checkBoxKI.isSelected(); 
    }
    
    /**
     * set the points information to the display
     * @param x
     * @param y
     * @param points 
     */
    public void addDisplayedPoints(int x, int y, int points) {
        this.middlePanel.addDisplayedPoints(x, y, points);
    }
}
