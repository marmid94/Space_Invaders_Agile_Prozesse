
package spaceinvader.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * class which generate the HelpMenu of the Menu
 * @author Stefan Fruhwirth
 * @version 1.1
 */
public class Help extends JFrame{
    
    private final JTextArea textArea;
    private final JScrollPane scrollArea;
    private final JPanel panel; 
    
    /**
     * Konstructor
     */
    public Help() {         
        this.panel = new JPanel(); 
 
        this.textArea = new JTextArea();
        this.textArea.setFont(new Font("Serif", Font.ITALIC, 16));
        this.textArea.setLineWrap(true);
        this.textArea.setWrapStyleWord(true);  
        this.textArea.setBackground(Color.WHITE);
        this.textArea.setEditable(false);
        this.textArea.setDragEnabled(true);
        this.panel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Hilfehinweis"),BorderFactory.createEmptyBorder(5, 5, 5, 5)));
        
        this.scrollArea = new JScrollPane(this.textArea);
        this.scrollArea.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        this.scrollArea.setPreferredSize(new Dimension(280, 200));
        this.panel.add(this.scrollArea); 
        
        this.textArea.setText(this.getHelpText());
        
        this.add(this.panel); 
        
        this.setResizable(false);
        this.pack(); 
        this.setVisible(true);   
    }
    
    /**
     * get the text for the HelpInfo
     * @return String
     */
    private String getHelpText(){
        String help = "Das Ziel des Spiels ist das abschießen der Gegner.\n"
                + "Die Steuerung kann unter Einstellungen angepasst werden.\n"
                + "Das Spiel ist beendet, wenn alle Levels erfolgreich beendet sind, oder "
                + "alle Leben verbraucht sind.";
           
        return help; 
    }
}
