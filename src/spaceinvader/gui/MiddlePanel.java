package spaceinvader.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import javax.swing.JPanel;
import spaceinvader.model.*;

/**
 * class which generate the MiddlePanel of the MainFrame
 * @author Stefan Fruhwirth
 * @version 1.1
 */
class MiddlePanel extends JPanel {

    private final World world;
    private Map<Date, PointObject> displayedPoints;
    private final static int POINT_DURATION = 2000;
     
    /**
     * intern class for the Points
     */
    private class PointObject {
        public int x;
        public int y;
        public int points;

        public PointObject(int x, int y, int points) {
            this.x = x;
            this.y = y;
            this.points = points;
        }    
    }
    
    /**
     * Konstructor
     * @param world 
     */
    public MiddlePanel(World world) {
        this.world = world; 
        this.setBackground(Color.BLACK);
        this.displayedPoints = new ConcurrentHashMap<>();     
    }
    
    /**
     * event to paint all component 
     * @param g 
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        for(AbstractWorldObject object : this.world.getWorldObjects()) {
            g.drawImage(object.getImage(), object.getX(), object.getY(), this);
        }
          
        g.setColor(Color.white);
        Date minDate = new Date(new Date().getTime() - POINT_DURATION);
        
        for(Entry<Date, PointObject> pointObject : this.displayedPoints.entrySet()) {
            if(minDate.before(pointObject.getKey())) {
                g.drawString(Integer.toString(pointObject.getValue().points), 
                        pointObject.getValue().x, 
                        pointObject.getValue().y + 20);
            }
            else {
                this.displayedPoints.remove(pointObject.getKey());
            }
        }
    }
    
    /**
     * set the points to the panel
     * @param x
     * @param y
     * @param points 
     */
    void addDisplayedPoints(int x, int y, int points) {
        this.displayedPoints.put(new Date(), new PointObject(x, y, points));

    }
}
    

