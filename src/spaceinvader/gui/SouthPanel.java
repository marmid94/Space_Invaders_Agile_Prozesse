package spaceinvader.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import spaceinvader.model.GameController;
import spaceinvader.model.Weapon;
import spaceinvader.model.level.GameLibrary; 

/**
 * class which generate the SouthPanel of the MainFrame
 * @author Stefan Fruhwirth
 * @version 1.1
 */
class SouthPanel extends JPanel {
    
    private final GameController gameController; 
    
    /*
        for Live
    */     
    private final JPanel panelLive; 
    
    private final JLabel labelLive;
    private final JLabel[] labelLiveIcon;
    
    private final ImageIcon iconLive; 
    private Image imageLive;  
    
    /*
        for Weapon
    */
    private final JPanel panelWeapon, panelPlayerWeapon; 
    
    private final JLabel labelWeapon;      
    private JLabel[] labelArrayPlayer1, labelArrayPlayer2; 
    
    private List<Weapon> listWeapon; 

    /*
        for Munition
    */
    private final JPanel panelPoints; 
    private final JLabel labelPoints, labelPointCount; 
    
    /**
     * Konstructor,generate the Panel
     * @param live
     * @param levelStatus 
     */
    public SouthPanel(GameController gameController) {
        
        this.gameController = gameController; 
        
        this.setLayout(new BorderLayout(20, 20));
        this.setBackground(Color.DARK_GRAY);
        this.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
                
        /*
            Panel for Live
        */     
        this.panelLive = new JPanel();
        this.panelLive.setBackground(Color.DARK_GRAY);
        
        this.imageLive = GameLibrary.getInstance().getLifeItemImage();
        this.imageLive = this.imageLive.getScaledInstance(30, 30,Image.SCALE_DEFAULT);
        
        this.iconLive = new ImageIcon(this.imageLive);  
        
        this.labelLive = new JLabel("Leben"); 
        this.labelLive.setForeground(Color.WHITE);
        this.labelLive.setFont(new Font("Arial", Font.BOLD, 16));
        this.panelLive.add(this.labelLive); 
        
        this.labelLiveIcon = new JLabel[this.gameController.getFullLifes()]; 
        
        for(int i=0; i< this.gameController.getFullLifes(); i++){
            this.labelLiveIcon[i] = new JLabel(iconLive, JLabel.CENTER); 
            this.labelLiveIcon[i].setEnabled(false);
            this.panelLive.add(this.labelLiveIcon[i]); 
        }    
        
        this.add(this.panelLive, BorderLayout.LINE_START);
        
        /*
            Weapon
        */
        this.panelWeapon = new JPanel(); 
        
        this.panelPlayerWeapon = new JPanel();
        
        this.panelWeapon.setBackground(Color.DARK_GRAY);
        
        this.labelWeapon = new JLabel("Waffen");
        this.labelWeapon.setForeground(Color.WHITE);
        this.labelWeapon.setBackground(Color.DARK_GRAY);
        this.labelWeapon.setFont(new Font("Arial", Font.BOLD, 16));
        
        //this.panelWeapon.add(this.labelWeapon);
        this.panelWeapon.setBackground(Color.DARK_GRAY);
        this.panelWeapon.add(panelPlayerWeapon);
        
        
        this.add(this.panelWeapon, BorderLayout.CENTER);
        this.updateWeapon();
        
        /*
            Panel for Points
        */
        this.panelPoints = new JPanel(); 
        this.panelPoints.setBackground(Color.DARK_GRAY);
        
        this.labelPoints = new JLabel("Punkte"); 
        this.labelPoints.setForeground(Color.WHITE);
        this.labelPoints.setFont(new Font("Arial", Font.BOLD, 16));
        
        this.labelPointCount = new JLabel(); 
        this.labelPointCount.setForeground(Color.WHITE);
        this.labelPointCount.setFont(new Font("Arial", Font.BOLD, 16));
        
        this.panelPoints.add(this.labelPoints); 
        this.panelPoints.add(this.labelPointCount); 
        
        this.setPoints(0);
        this.add(this.panelPoints, BorderLayout.LINE_END);      
    }
    
    /**
     * set the live
     * @param live
     * @param statusLive 
     */
    public void setLifes(int lifes){
        
        for(int i=0; i< this.labelLiveIcon.length; i++){
            
            if(i < lifes){
                this.labelLiveIcon[i].setEnabled(true);
            }
            else{
                this.labelLiveIcon[i].setEnabled(false);
            }
        }    
    }

    /**
     * counter for munition shooting
     * @param count 
     */
    public void setPoints(int count){
        this.labelPointCount.setText(String.valueOf(count));
    }
    
    /**
     * update all weapons
     */
    public void updateWeapon(){

        this.panelPlayerWeapon.setBackground(Color.DARK_GRAY);
        this.panelPlayerWeapon.removeAll();
        
        // 1 Player
        if(this.gameController.getNumberOfPlayers() == 1){
             
            this.listWeapon = gameController.getPlayerWeapons(0);                     
            this.labelArrayPlayer1 = new JLabel[this.listWeapon.size()];
            
            this.panelPlayerWeapon.setLayout(new FlowLayout(0, 30, 0));
            
            for(int i=0; i<listWeapon.size(); i++){
                
                this.labelArrayPlayer1[i] = new JLabel(this.listWeapon.get(i).getName()); 
                this.labelArrayPlayer1[i].setName(this.listWeapon.get(i).getName());
                this.labelArrayPlayer1[i].setForeground(Color.WHITE);
                this.labelArrayPlayer1[i].setFont(new Font("Arial", Font.BOLD,16));
                
                this.panelPlayerWeapon.add(this.labelArrayPlayer1[i]); 
            }       
            this.panelWeapon.add(this.panelPlayerWeapon);
        }
        
        // 2 Player
        else if(this.gameController.getNumberOfPlayers() == 2){
            this.panelPlayerWeapon.setLayout(new GridLayout(2, 1, 10, 10));
            
            this.listWeapon = gameController.getPlayerWeapons(0);
            this.labelArrayPlayer1 = new JLabel[this.listWeapon.size()];
            
            this.listWeapon = this.gameController.getPlayerWeapons(1);
            this.labelArrayPlayer2 = new JLabel[this.listWeapon.size()];
            
            JPanel panel1 = new JPanel();
            panel1.setLayout(new FlowLayout(0, 30, 0));
            panel1.setBackground(Color.DARK_GRAY);
            
            for(int i=0; i<listWeapon.size(); i++){
                this.labelArrayPlayer1[i] = new JLabel(this.listWeapon.get(i).getName()); 
                this.labelArrayPlayer1[i].setName(this.listWeapon.get(i).getName());
                this.labelArrayPlayer1[i].setForeground(Color.WHITE);
                this.labelArrayPlayer1[i].setFont(new Font("Arial", Font.BOLD,16));
                
                panel1.add(this.labelArrayPlayer1[i]); 
            }    
            
            
            JPanel panel2 = new JPanel();
            panel2.setLayout(new FlowLayout(0, 30, 0));
            panel2.setBackground(Color.DARK_GRAY);
            for(int i=0; i<listWeapon.size(); i++){
                this.labelArrayPlayer2[i] = new JLabel(this.listWeapon.get(i).getName()); 
                this.labelArrayPlayer2[i].setName(this.listWeapon.get(i).getName());
                this.labelArrayPlayer2[i].setForeground(Color.WHITE);
                this.labelArrayPlayer2[i].setFont(new Font("Arial", Font.BOLD,16));
                
                panel2.add(this.labelArrayPlayer2[i]);  
            }   
            
            this.panelPlayerWeapon.add(panel1);
            this.panelPlayerWeapon.add(panel2);
        }
        
        this.updateChooseWeapon();
    }
    
    /**
     * update the choosen weapon
     */
    public void updateChooseWeapon(){
        
        if(this.gameController.getNumberOfPlayers() == 1){
            
            try{      
                int weaponIndex = this.gameController.getCurrentWeaponIndex();
                
                
                for(int i=0; i<this.listWeapon.size(); i++){

                    if(i == weaponIndex){
                        this.labelArrayPlayer1[i].setForeground(Color.YELLOW);

                    }
                    else {
                        this.labelArrayPlayer1[i].setForeground(Color.WHITE);
                    }
                    
                    
                }
            }
            catch(ArrayIndexOutOfBoundsException e){}
       
      
        }    
        else if(this.gameController.getNumberOfPlayers() == 2){
            
            try{      
                int weaponIndex1 = this.gameController.getCurrentWeaponIndex(0);
                int weaponIndex2 = this.gameController.getCurrentWeaponIndex(1);
                
                
                for(int i=0; i<this.listWeapon.size(); i++){

                    if(i == weaponIndex1){
                        this.labelArrayPlayer1[i].setForeground(Color.YELLOW);
                    }
                    else {
                        this.labelArrayPlayer1[i].setForeground(Color.WHITE);
                    }
                    
                    if(i == weaponIndex2){
                        this.labelArrayPlayer2[i].setForeground(Color.YELLOW);
                    }
                    else {
                        this.labelArrayPlayer2[i].setForeground(Color.WHITE);
                    }
                }
            }
            catch(ArrayIndexOutOfBoundsException e){}    
        }    
    }
}
    
