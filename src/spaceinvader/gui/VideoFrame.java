
package spaceinvader.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * class which generate the Video of the MainFrame
 * @author Stefan Fruhwirth
 * @version 1.1
 */
public class VideoFrame extends JFrame implements ActionListener{
    
    private JPanel panelSafe, panelExit; 
    private File file; 
    
    private JTextField textField; 
    
    private MakeVideo makePicture; 
    private MainFrame frame; 
    
    private JButton buttonSafe, buttonExit; 
    
    private Path moveSourcePath; 
    private Path moveTargetPath; 
    
    /**
     * Konstructor
     * @param frame 
     */
    public VideoFrame(MainFrame frame){   
        
        this.frame = frame; 
        
        this.panelSafe = new JPanel(); 
        this.panelExit = new JPanel(); 
        
        this.add(this.panelSafe); 
        this.add(this.panelExit); 
        
        this.setTitle("Video");
        this.pack();        
        this.setVisible(true);    
    }
    
    /**
     * safe the picture
     */
    public void safe(){  
        if(this.buttonExit != null){         
            this.remove(this.buttonExit);
        }     
        
        this.buttonSafe = new JButton("Speichern Pfad"); 
        this.buttonSafe.setActionCommand("safe");
        this.buttonSafe.addActionListener(this);
        
        this.panelSafe.add(this.buttonSafe);       
        
        this.add(this.panelSafe); 
        this.pack();
        this.setVisible(true);
    }
    
    /**
     * close the frame of the videoFrame
     */
    public void exit(){ 
        
        if(this.panelSafe != null){
            this.remove(this.panelSafe);
        }
        
        this.buttonExit = new JButton("Aufzeichnung beenden"); 
        this.buttonExit.setActionCommand("exit");
        this.buttonExit.addActionListener(this);
        
        this.panelExit.add(this.buttonExit); 
        
        this.add(this.panelExit);
        this.pack();
        this.setVisible(true);
    }
    
    /**
     * events form the panel of the videoFrame
     * @param e 
     */
    @Override
    public void actionPerformed(ActionEvent e) {
         switch(e.getActionCommand()){
             case "safe":
                JFileChooser chooser = new JFileChooser();
                chooser.setSelectedFile(this.file = new File("video.mp4"));
                chooser.addChoosableFileFilter(new FileNameExtensionFilter(null, "mp4"));
                
                if(chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
                   
                    this.makePicture = new MakeVideo(this.frame);
                    this.makePicture.startMakePicture(file.getAbsolutePath());
                    //this.makePicture.startMakePicture("/home/stefan/Dokumente/file");
                    
                    this.moveSourcePath = Paths.get("../..");
                    this.moveTargetPath = Paths.get(file.getAbsolutePath()); 
                }
                
                this.setVisible(false);                  
                break;
                 
             case "exit":
                this.makePicture.makeVideo();

                try {
                    Files.move(this.moveSourcePath, this.moveTargetPath);
                } catch (IOException ex) {

                }

                this.setVisible(false);
                    break;  
         }
    }
    
}
