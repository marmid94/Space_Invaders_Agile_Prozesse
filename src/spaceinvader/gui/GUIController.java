
package spaceinvader.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import javax.swing.filechooser.FileNameExtensionFilter;
import spaceinvader.model.*;
import spaceinvader.model.level.EnemyWave;
import spaceinvader.model.level.Game;
import spaceinvader.model.level.GameLibrary;
import spaceinvader.model.level.Level;
import spaceinvader.model.level.LevelLoader;

/**
 * class which generate the Controller of the MainFrame
 * @author Stefan Fruhwirth
 * @version 1.1
 */
public class GUIController implements ActionListener, KeyListener, GameListener, RepaintListener {
    
    private MainFrame frame;   
    private FeatureFrame feature;
    private VideoFrame videoFrame; 
    
    private GameController gameController;
    
    private Timer keyTimer;
    
    private boolean leftActive, rightActive, shootActive;
    private boolean leftActive2, rightActive2, shootActive2;
    
    private int leftKey1, rightKey1, shootKey1, weaponKey1; 
    private int leftKey2, rightKey2, shootKey2, weaponKey2; 
    private int playPauseKey, respawnKey;
    
    private int playerCounter; 
     
    private static final int MOVE_TEMPO = 5;
    
    /**
     * Konstructor
     * @param frame
     * @param gameController 
     */
    public GUIController(MainFrame frame, GameController gameController){
        this.frame = frame; 
        frame.addKeyListener(this);
        this.gameController = gameController; 
 
        this.keyTimer = new Timer(10, this);
        this.keyTimer.start();
      
        this.leftActive = this.rightActive = this.shootActive = false;
        this.leftActive2 = this.rightActive2 = this.shootActive2 = false;
       
        this.leftKey1 = KeyEvent.VK_A; 
        this.rightKey1 = KeyEvent.VK_D; 
        this.shootKey1 = KeyEvent.VK_W; 
        this.weaponKey1 = KeyEvent.VK_S; 
        
        this.leftKey2 = KeyEvent.VK_LEFT; 
        this.rightKey2 = KeyEvent.VK_RIGHT; 
        this.shootKey2 = KeyEvent.VK_UP;
        this.weaponKey2 = KeyEvent.VK_DOWN;    
        
        this.playPauseKey = KeyEvent.VK_SPACE;
        this.respawnKey = KeyEvent.VK_SPACE;
        
        this.playerCounter = 1; 
    }
  
    /**
    * events if a active is performed
    * @param e 
    */
    @Override
    public void actionPerformed(ActionEvent e) {
        
        if (e.getSource().equals(this.keyTimer)) {
            
            if(this.leftActive) {
                this.gameController.movePlayer(-MOVE_TEMPO, 0);
            }
            
            if(this.rightActive) {
                this.gameController.movePlayer(MOVE_TEMPO, 0);
            }
            
            if(this.shootActive) {
                this.gameController.shootWithPlayer(0);
            }
            
            if(this.leftActive2) {
                this.gameController.movePlayer(-MOVE_TEMPO, 1);
            }
            
            if(this.rightActive2) {
                this.gameController.movePlayer(MOVE_TEMPO, 1);
            }
            
            if(this.shootActive2) {
                this.gameController.shootWithPlayer(1);
            }
            
        }
        else {
                    
            switch(e.getActionCommand()){

                /*
                    Menu Action
                */
                case "Game1":                    
                    this.gameController.setGame(GameLibrary.getInstance().getDefaultGame("Game1"), this.playerCounter);
                    if(this.frame.getCheckBoxKI()) {
                        this.gameController.addAI(0);
                    }
                    this.frame.getSouthPanel().updateWeapon();
                    this.frame.getHeaderPanel().setLevel(1);
                                        
                    break;
                    
                case "Game2":       
                    this.gameController.setGame(GameLibrary.getInstance().getDefaultGame("Game2"), this.playerCounter);
                    if(this.frame.getCheckBoxKI()) {
                        this.gameController.addAI(0);
                    }
                    this.frame.getSouthPanel().updateWeapon();
                    this.frame.getHeaderPanel().setLevel(1);
                    
                    break; 
                    
                case "Load Game":
                    LevelLoader loader = new LevelLoader();
                
                    JFileChooser fileChooser = new JFileChooser();
                    fileChooser.setFileFilter(new FileNameExtensionFilter(null, "game"));

                    if (fileChooser.showOpenDialog(this.frame) == JFileChooser.APPROVE_OPTION) {

                        try {
                            Level level = loader.loadLevelConfig(fileChooser.getSelectedFile());
                            Game game = GameLibrary.getInstance().createEmpytGame();
                            game.addLevel(level);
                            
                            this.gameController.setGame(game, this.playerCounter);
                            if(this.frame.getCheckBoxKI()) {
                                this.gameController.addAI(0);
                            }
                            this.frame.getSouthPanel().updateWeapon();
                            this.frame.getHeaderPanel().setLevel(1);
                        } 
                        catch (Exception ex) {
                            JOptionPane.showMessageDialog(this.frame, "Die Datei konnte nicht geladen werden. Sie ist möglicherweise fehlerhaft.");
                        }
                    }
                    
                    
                    
                    
                    break; 

                case "Exit":
                    this.gameController.pause();
                    System.exit(0);
                    break;

                case "Control":
                    this.feature = new FeatureFrame(this, 
                        this.leftKey1, this.rightKey1, this.shootKey1, this.weaponKey1,
                        this.leftKey2, this.rightKey2, this.shootKey2, this.weaponKey2,
                    this.playPauseKey, this.respawnKey); 
                    break;

                case "Info":
                    new InfoFrame();
                    break;

                case "Help":
                    new HelpFrame(); 
                    break; 
                    
                    //Einzelspieler
                case "Player1":
                    if(this.frame.getCheckBoxOnePlayer()){
                        this.playerCounter = 1; 
                        this.frame.setCheckBoxTwoPlayer(false);         
                        this.frame.setCheckBoxKI(false);
                        
                        if(this.gameController.getGame() != null) {                   
                            this.gameController.setGame(this.gameController.getGame(), this.playerCounter);                    
                        }              
                    }
                    break; 
                    
                    //Multiplayer
                case "Player2":
                    if(this.frame.getCheckBoxTwoPlayer()){
                        this.playerCounter = 2;
                        this.frame.setCheckBoxOnePlayer(false);
                        this.frame.setCheckBoxKI(false);
                        
                        if(this.gameController.getGame() != null) {
                            this.gameController.setGame(this.gameController.getGame(), this.playerCounter);                       
                        }
                    }
                    break; 
                   
                    // KI
                case "KI":
                    if(this.frame.getCheckBoxKI()){
                        this.playerCounter = 1;
                        this.frame.setCheckBoxOnePlayer(false);
                        this.frame.setCheckBoxTwoPlayer(false);
                        
                        if(this.gameController.getGame() != null) {
                            this.gameController.setGame(this.gameController.getGame(), this.playerCounter);
                            this.gameController.addAI(0);                  
                        }
                    }
                    break; 
                    
                /*
                    Popup FeatureFrame handling
                */
                case "OK_Feature":
                    this.leftKey1 = this.feature.getKeyLeft1();
                    this.rightKey1 = this.feature.getKeyRight1(); 
                    this.shootKey1 = this.feature.getKeyShoot1(); 
                    this.weaponKey1 = this.feature.getKeyWeapon1();
                    this.leftKey2 = this.feature.getKeyLeft2();
                    this.rightKey2 = this.feature.getKeyRight2(); 
                    this.shootKey2 = this.feature.getKeyShoot2(); 
                    this.weaponKey2 = this.feature.getKeyWeapon2();
                    this.playPauseKey = this.feature.getKeyPlayPause();
                    this.respawnKey = this.feature.getKeyRespawn();
                    
                    this.feature.setVisible(false);
                               
                    break; 
                    
                case "Exit_Feature":
                    this.feature.setVisible(false);
                    break; 
                 
                    /*
                        Video handling
                    */
                case "startVideo":
                    this.videoFrame = new VideoFrame(this.frame); 
                    this.videoFrame.safe();    
                                        
                    break; 
                    
                case "exitVideo":
                    if(this.videoFrame != null){        
                        this.videoFrame.exit(); 
                    }                   
                    break; 

            }
        }
    }

    /**
     * event if a key is typed
     * @param e 
     */
    @Override
    public void keyTyped(KeyEvent e) {}

    /**
     * event if a key pressed
     * @param e 
     */
    @Override
    public void keyPressed(KeyEvent e) {
   
        if(e.getKeyCode() == this.leftKey1) {
            this.leftActive = true; 
        }
        if(e.getKeyCode() == this.rightKey1) {
            this.rightActive = true; 
        }
        if(e.getKeyCode() == this.shootKey1) {
            this.shootActive = true; 
            this.frame.getSouthPanel().updateChooseWeapon();
        }
        if(e.getKeyCode() == this.weaponKey1) {
            this.gameController.changePlayerWeapon(0);
            this.frame.getSouthPanel().updateChooseWeapon();
        }
        if(e.getKeyCode() == this.leftKey2) {
            this.leftActive2 = true; 
        }
        if(e.getKeyCode() == this.rightKey2) {
            this.rightActive2 = true; 
        }
        if(e.getKeyCode() == this.shootKey2) {
            this.shootActive2 = true; 
            this.frame.getSouthPanel().updateChooseWeapon();
        }
        if(e.getKeyCode() == this.weaponKey2) {
            this.gameController.changePlayerWeapon(1);      
            this.frame.getSouthPanel().updateChooseWeapon();
        }
        if(e.getKeyCode() == this.respawnKey) {
            this.gameController.respawn();
            this.gameController.startNextLevel();
        }
        if(e.getKeyCode() == this.playPauseKey) {    
            if(this.gameController.isPaused()) {
                this.gameController.start();
            }
            else {
                this.gameController.pause();
            }
        }     
    }

    /**
     * event if key is released
     * @param e 
     */
    @Override
    public void keyReleased(KeyEvent e) {
        
        if(e.getKeyCode() == this.leftKey1) {
            this.leftActive = false; 
        }
        if(e.getKeyCode() == this.rightKey1) {
            this.rightActive = false; 
        }
        if(e.getKeyCode() == this.shootKey1) {
            this.shootActive = false; 
        }
        if(e.getKeyCode() == this.leftKey2) {
            this.leftActive2 = false; 
        }
        if(e.getKeyCode() == this.rightKey2) {
            this.rightActive2 = false; 
        }
        if(e.getKeyCode() == this.shootKey2) {
            this.shootActive2 = false; 
        }
    }

    
    /**
     * event if the player win
     */
    @Override
    public void onWin() {
        this.frame.getHeaderPanel().setStatus("Gewonnen", true, true);
        
    }

    /**
     * event if the player is dead
     */
    @Override
    public void onGameOver() {
        this.frame.getHeaderPanel().setStatus("Game Over", true, true);
    }

    /**
     * event if a new level is started
     * @param level
     * @param levelIndex 
     */
    @Override
    public void onLevelStarted(Level level, int levelIndex) {
        this.frame.getHeaderPanel().setStatus("Level " + (levelIndex + 1), false, true);
    }

    /**
     * event if the level is finished
     * @param level
     * @param levelIndex 
     */
    @Override
    public void onLevelFinished(Level level, int levelIndex) {
        this.frame.getHeaderPanel().setStatus("Level abgeschlossen");
    }

    /**
     * event if the level is restarted
     * @param level
     * @param waveIndex 
     */
    @Override
    public void onRespawn(Level level, int waveIndex) {
        this.frame.getSouthPanel().updateWeapon();

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    /**
     * event if the player is dead
     * @param reason 
     */
    @Override
    public void onDeath(LifeLostReason reason) {
        
        if(reason == LifeLostReason.ENEMIES_REACHED_PLAYER) {
            this.frame.getHeaderPanel().setStatus("Die Feinde haben dich erreicht!", false, true);
        }
        else if(reason == LifeLostReason.PLAYER_HIT)
            this.frame.getHeaderPanel().setStatus("Du wurdest getroffen!", false, true);
    }

    /**
     * event if the player get points
     * @param points
     * @param reason 
     */
    @Override
    public void onPointsAchieved(int points, PointReason reason) {
        if(reason == PointReason.ENEMY_KILL) {
            this.frame.getHeaderPanel().setStatus("Feind abgeschossen - " + points + " Punkte");
        }
        else if(reason == PointReason.ITEM) {
            this.frame.getHeaderPanel().setStatus("Item zerstört - " + points + " Punkte");
        }
        else if(reason == PointReason.ACHIEVMENT) {
            this.frame.getHeaderPanel().setStatus("Achievment - " + points + " Punkte");
        }
        
    }

    /**
     * event if projectil hit something
     * @param target
     * @param projectil
     * @param damage
     * @param wasKilled
     * @param x
     * @param y 
     */
    @Override
    public void onHit(AbstractTarget target, Projectil projectil, int damage, boolean wasKilled, int x, int y) {
        if(wasKilled && PointItem.class.isInstance(target)) {
            this.frame.addDisplayedPoints(x, 
                    y,
                    ((PointItem)target).getWinPoints());
        }
        else if(wasKilled && Enemy.class.isInstance(target)) {
            this.frame.addDisplayedPoints(x, 
                    y,
                    ((Enemy)target).getWinPoints());
        }
    }

    @Override
    public void onHeal(AbstractTarget target, int heal, boolean wasFullHeal) {}

    /**
     * event if the player has a multi kill
     * @param kills
     * @param points 
     */
    @Override
    public void onMultiKill(int kills, int points) {
        if(kills == 2) {
            this.frame.getHeaderPanel().setStatus("Doppelabschuss - " + points + " Punkte", false, true);
        }
        else if(kills >= 3) {
            this.frame.getHeaderPanel().setStatus("" + kills + "-fach-Abschuss - " + points + " Punkte", false, true);
        }
        
    }

    /**
     * event to repaint the frame
     */
    @Override
    public void onRepaint() {
        this.frame.getMiddlePanel().repaint();
    }

    /**
     * event if the live changed
     * @param lifes 
     */
    @Override
    public void onLifesChanged(int lifes) {
        this.frame.getSouthPanel().setLifes(lifes);
    }

    /**
     * get the information if points are changed
     * @param points 
     */
    @Override
    public void onPointsChanged(int points) {
        this.frame.getSouthPanel().setPoints(points);
    }

    /**
     * get the event to add a new live to the player
     * @param numberOfLifes
     * @param reason 
     */
    @Override
    public void onLifesAdded(int numberOfLifes, LifeAddReason reason) {
        if(reason == LifeAddReason.ITEM && numberOfLifes == 1) {
            this.frame.getHeaderPanel().setStatus("Leben durch Item wiederhergestellt");
        }
    }

    /**
     * get the event if a new wave is started
     * @param wave
     * @param waveIndex 
     */
    @Override
    public void onWaveStarted(EnemyWave wave, int waveIndex) {
        this.frame.getHeaderPanel().setLevel(waveIndex + 1);
        this.frame.getHeaderPanel().setStatus("Welle " + (waveIndex + 1), false, true);
        this.frame.getSouthPanel().updateWeapon();
        this.frame.getSouthPanel().updateChooseWeapon();
    }

    /**
     * get the event if the weapon has changed
     * @param player
     * @param wepaon
     * @param wepaonIndex 
     */
    @Override
    public void onPlayerWeaponChanged(Player player, Weapon wepaon, int wepaonIndex) {
        this.frame.getSouthPanel().updateChooseWeapon();
    }

    /**
     * get the event if the position of the player has moved
     * @param player
     * @param xMove 
     */
    @Override
    public void onPlayerMoved(Player player, int xMove) {}

    @Override
    public void onTilt(Player player) {
        this.frame.getHeaderPanel().setStatus("Tilt", false, true);
    }
   
}
