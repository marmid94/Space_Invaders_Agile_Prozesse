
package spaceinvader.gui;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import spaceinvader.model.AbstractItem;
import spaceinvader.model.AbstractTarget;
import spaceinvader.model.Enemy;
import spaceinvader.model.GameController;
import spaceinvader.model.GameListener;
import spaceinvader.model.Player;
import spaceinvader.model.Projectil;
import spaceinvader.model.Weapon;
import spaceinvader.model.level.EnemyWave;

/**
 * Implements the sounds of the game 
 * @author Philip Müller
 * @version 1.1
 */
public class SoundController implements GameListener {
    private GameController gameController;
    
    /**
     * Konstructor
     * @param controller 
     */
    public SoundController(GameController controller) {
        this.gameController = controller;      
    }
  
    private Clip soundtrack;
  
    /**
     * event if the player win
     */
    @Override
    public void onWin() {
       try {
            Clip clip = AudioSystem.getClip();
            clip.open(AudioSystem.getAudioInputStream(
                    this.gameController.getGame().getWinSound()));
            clip.start();
        } 
        catch (LineUnavailableException | IOException | UnsupportedAudioFileException | IllegalArgumentException ex) {}
    }

    /**
     * event if the player is dead
     */
    @Override
    public void onGameOver() {
        try {
            Clip clip = AudioSystem.getClip();
            clip.open(AudioSystem.getAudioInputStream(
                    this.gameController.getGame().getGameOverSound()));
            clip.start();
        } 
        catch (LineUnavailableException | IOException | UnsupportedAudioFileException | IllegalArgumentException ex) {}
    }

    /**
     * event if the level is started
     * @param level
     * @param levelIndex 
     */
    @Override
    public void onLevelStarted(spaceinvader.model.level.Level level, int levelIndex) {
        try {
            this.soundtrack = AudioSystem.getClip();
            soundtrack.open(AudioSystem.getAudioInputStream(
                    this.gameController.getGame().getGameMusic()));
            
        } 
        catch (LineUnavailableException | IOException | UnsupportedAudioFileException | IllegalArgumentException ex) {}
            
    }

    @Override
    public void onLevelFinished(spaceinvader.model.level.Level level, int levelIndex) {}

    @Override
    public void onRespawn(spaceinvader.model.level.Level level, int waveIndex) {}

    /**
     * event if the level is started
     */
    @Override
    public void onStart() {
        
        try{
            this.soundtrack.loop(-1);
            this.soundtrack.start();
        }catch(IllegalStateException e){ }
    }

    /**
     * event if the level is stopped
     */
    @Override
    public void onStop() {
        try{
            this.soundtrack.stop();
        }catch(IllegalStateException e){ } 
    }

    /**
     * event if the player is dead
     * @param reason 
     */
    @Override
    public void onDeath(LifeLostReason reason) {
        try {
            Clip clip = AudioSystem.getClip();
            clip.open(AudioSystem.getAudioInputStream(
                    this.gameController.getGame().getDeathSound()));
            clip.start();
        } 
        catch (LineUnavailableException | IllegalArgumentException ex) {} 
        catch (IOException ex) {} catch (UnsupportedAudioFileException ex) {
            Logger.getLogger(SoundController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * event if the player get a new heart
     * @param numberOfLifes
     * @param reason 
     */
    @Override
    public void onLifesAdded(int numberOfLifes, LifeAddReason reason) { }

    /**
     * event if the points changed
     * @param points
     * @param reason 
     */
    @Override
    public void onPointsAchieved(int points, PointReason reason) {}

    /**
     * event if an enemy is hited
     * @param target
     * @param projectil
     * @param damage
     * @param wasKilled
     * @param x
     * @param y 
     */
    @Override
    public void onHit(AbstractTarget target, Projectil projectil, int damage, boolean wasKilled, int x, int y) {
        if(Enemy.class.isInstance(target)) {
            if(wasKilled) {
                try {
                    Clip clip = AudioSystem.getClip();
                    clip.open(AudioSystem.getAudioInputStream(
                            this.gameController.getGame().getKillSound()));
                    clip.start();
                } 
                catch (LineUnavailableException | IOException | UnsupportedAudioFileException | IllegalArgumentException ex) {}
            }
            else {
                try {
                    Clip clip = AudioSystem.getClip();
                    clip.open(AudioSystem.getAudioInputStream(
                            this.gameController.getGame().getHitSound()));
                    clip.start();
                } 
                catch (LineUnavailableException | IOException | UnsupportedAudioFileException | IllegalArgumentException ex) {}
            }
        }
        else if(AbstractItem.class.isInstance(target)) {
            try {
                    Clip clip = AudioSystem.getClip();
                    clip.open(AudioSystem.getAudioInputStream(
                            this.gameController.getGame().getItemSound()));
                    clip.start();
                } 
                catch (LineUnavailableException | IOException | UnsupportedAudioFileException | IllegalArgumentException ex) {}
            
        }
    }

    /**
     * event if the player get a new heart
     * @param target
     * @param heal
     * @param wasFullHeal 
     */
    @Override
    public void onHeal(AbstractTarget target, int heal, boolean wasFullHeal) {}

    /**
     * event if the player has a multi kill
     * @param kills
     * @param points 
     */
    @Override
    public void onMultiKill(int kills, int points) {}

    /**
     * event if a new Wave is started
     * @param wave
     * @param waveIndex 
     */
    @Override
    public void onWaveStarted(EnemyWave wave, int waveIndex) {
        try {
            Clip clip = AudioSystem.getClip();
            clip.open(AudioSystem.getAudioInputStream(
                    this.gameController.getGame().getNewWaveSound()));
            clip.start();
            
            
        } 
        catch (LineUnavailableException | IOException | UnsupportedAudioFileException | IllegalArgumentException ex) {         
        }
            
    }

    /**
     * event if a player has changed
     * @param player
     * @param wepaon
     * @param wepaonIndex 
     */
    @Override
    public void onPlayerWeaponChanged(Player player, Weapon wepaon, int wepaonIndex) {}

    /**
     * event if the player has moved
     * @param player
     * @param xMove 
     */
    @Override
    public void onPlayerMoved(Player player, int xMove) {}

    @Override
    public void onTilt(Player player) {}
}
