
package spaceinvader.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * class which generate the Frame of the InfoMenu
 * @author Stefan Fruhwirth
 * @version 1.1
 */
public class InfoFrame extends JFrame{
    
    private final JTextArea textArea;
    private final JScrollPane scrollArea;
    private final JPanel panel; 
    
    public InfoFrame() { 

        this.panel = new JPanel(); 
 
        this.textArea = new JTextArea();
        this.textArea.setFont(new Font("Serif", Font.ITALIC, 16));
        this.textArea.setLineWrap(true);
        this.textArea.setWrapStyleWord(true);  
        this.textArea.setBackground(Color.WHITE);
        this.textArea.setEditable(false);
        this.textArea.setDragEnabled(true);
        this.panel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Info"),BorderFactory.createEmptyBorder(5, 5, 5, 5)));
        
        this.scrollArea = new JScrollPane(this.textArea);
        this.scrollArea.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        this.scrollArea.setPreferredSize(new Dimension(280, 200));
        this.panel.add(this.scrollArea); 
        
        this.textArea.setText(this.getHelpText());
        
        this.add(this.panel); 
        
        this.setResizable(false);
        this.pack(); 
        this.setVisible(true);   
    }
    
    /**
     * get the Text for the InfoMenu
     * @return String
     */
    private String getHelpText(){
        String help = "Informationen\n"
                + "Das Spiel wurde programmiert von "
                + "- Philip Müller"
                + "- Stefan Fruhwirth"
                + "- Martin Schmid"
                + "- Fabian Haag "
                + "\n\n"
                + "Das Weitergeben des Spiels ist nur nach Abstimmung mit den Entwicklern gestattet";
        
        
        return help; 
    }
}
