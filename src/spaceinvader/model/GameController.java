package spaceinvader.model;

import java.util.ArrayList;
import java.util.List;
import spaceinvader.model.level.EnemyWave;
import spaceinvader.model.level.Game;
import spaceinvader.model.level.Level;

/**
 * The master controller for the game and its levels.
 * @author Martin Schmid
 * @author Fabian Haag
 * @author Philip Mueller
 * @author Stefan Fruhwirth
 * @version 1.0
 */
public class GameController {
    /**
     * The number of lifes that the player(s) has/have.
     */
    public static final int FULL_LIFES = 3;
    
    /**
     * The world thaht is controlled by the GameController.
     */
    private World world;
    
    /**
     * The event manager, handling all events.
     */
    private GameEventManager notificationManager;
    
    /**
     * The level initializer, starting the levels.
     */
    private LevelInitializer initializer;
    
    /**
     * The timing coordinator, managing all timed triggers.
     */
    private LevelTimingCoordinator timingCoordinator;
    
    /**
     * The collission handler, handling collissions between projectils and targets.
     */
    private CollisionHandler collisionHandler;
    
    /**
     * The currently played game.
     */
    private Game game;
    
    /**
     * The current level index.
     */
    private int currentLevelIndex;
    
    /**
     * The current wave index.
     */
    private int currentWaveIndex;
    
    /**
     * The Artificial intelligences that are currentyl controlling players.
     */
    private List<ArtificialIntelligence> AIs;
    
    /**
     * The current number of players.
     */
    private int numberOfPlayers;
    
    /**
     * States wether the player is currently alive.
     */
    private boolean isPlayerAlive;
    
    /**
     * States wether the level is finished.
     */
    private boolean isLevelFinished;
    
    
    /**
     * The current number of points.
     */
    private int points;
    
    /**
     * The current number of lifes.
     */
    private int lifes;

    /**
     * Constructor.
     */
    public GameController() {
            this.isPlayerAlive = false;
            this.numberOfPlayers = 0;
            this.isLevelFinished = true;
            this.points = 0;
            this.lifes = 0;
            this.currentLevelIndex = -1;
            this.currentWaveIndex = -1;
            this.game = null;
            this.AIs = new ArrayList<>();
    }
    
    /**
     * Inits the world with the given size.
     * @param width The width of the world.
     * @param height The height of the world.
     */
    public void init(int width, int height) {
        this.world = new World(width, height, this);
        this.notificationManager = new GameEventManager(this);
        this.initializer = new LevelInitializer(this);
        this.collisionHandler = new CollisionHandler(this);
        this.timingCoordinator = new LevelTimingCoordinator(this);
        
        AchievmentListener achievments = new AchievmentListener(this);
        this.notificationManager.addGameListener(achievments);
        
        EastereggListener easteregg = new EastereggListener(this);
        this.notificationManager.addGameListener(easteregg);
        
        TiltListener tilt = new TiltListener(this, 300, 1000, 100);
        this.notificationManager.addGameListener(tilt);
    }
    
    /**
     * Adds an AI for the given player.
     * @param playerIndex The index of the player that shall be controlled by an AI.
     */
    public void addAI(int playerIndex) {
        this.AIs.add(new ArtificialIntelligence(this, playerIndex));
    }

    /**
     * Returns the timing coordinator.
     * @return The timing coordinator.
     */
    LevelTimingCoordinator getTimingCoordinator() {
        return timingCoordinator;
    }

    /**
     * Returns the level initializer.
     * @return The level initializer.
     */
    LevelInitializer getInitializer() {
        return initializer;
    }

    /**
     * Returns the notification manager.
     * @return The notification manager.
     */
    public GameEventManager getNotificationManager() {
        return notificationManager;
    }

    /**
     * Returns the collission handler.
     * @return The collission handler.
     */
    CollisionHandler getCollisionHandler() {
        return collisionHandler;
    }
    
    /**
     * Returns the index of the current wave.
     * @return The index of the current wave.
     */
    public int getCurrentWaveIndex() {
        return currentWaveIndex;
    }
    
    /**
     * Returns the current wave.
     * @return The current wave.
     */
    public EnemyWave getCurrentWave() {
        //Return the wave if there is an active one or null if there is none.
        try {
            return this.game.getLevels().get(this.currentLevelIndex).getEnemyWaves().get(this.currentWaveIndex);
        }
        catch(IndexOutOfBoundsException e) {
            return null;
        }
    }

    /**
     * Returns the current number of players.
     * @return The current number of players.
     */
    public int getNumberOfPlayers() {
        return numberOfPlayers;
    }

    /**
     * Returns the world that is controlled.
     * @return The world that is controlled.
     */
    public World getWorld() {
        return world;
    }
    
    /**
     * Starts or continues the current level.
     */
    public void start() {
        //check wether the level can be started
        // - a game must be loaded
        // - a level must be active 
        // - the player must be alive
        // - the level must not be finished
        // - the level must currently be paused or stopped
        if(this.game != null && this.currentLevelIndex >= 0 && this.isPlayerAlive &&
                !this.isLevelFinished && this.timingCoordinator.isPaused()) {
                
            
            this.timingCoordinator.start();

            //start all the AIs
            for(ArtificialIntelligence AI : this.AIs) {
                AI.start();
            }
                
            //notify
            this.notificationManager.notifyStart();
            
        }
    }
    
    /**
     * Pauses the current level.
     */
    public void pause() {
        //check wether the level can be paused
        // - a game must be loaded
        // - a level must be active 
        // - the level must not be paused
        if(this.game != null && this.currentLevelIndex >= 0 && 
                !this.timingCoordinator.isPaused()) {
            
            this.timingCoordinator.pause();
            
            //pause all the AIs
            for(ArtificialIntelligence AI : this.AIs) {
                AI.stop();
            }
            
            
            //notify
            this.notificationManager.notifyStop();
        }
    }
    
    /**
     * Returns wether the level is currently paused.
     * @return True if the level is paused, false if not.
     */
    public boolean isPaused() {
        return this.timingCoordinator.isPaused();
    }
    
    /**
     * Returns the current game.
     * @return The current game.
     */
    public Game getGame() {
        return game;
    }

    /**
     * Returns wether the current level is finished.
     * @return True if it is finished, false if not.
     */
    public boolean isIsLevelFinished() {
        return isLevelFinished;
    }

    /**
     * Sets wether the current level is finished.
     * @param isLevelFinished States wether the level shall be finished.
     */
    void setIsLevelFinished(boolean isLevelFinished) {
        this.isLevelFinished = isLevelFinished;
    }
    
    /**
     * Returns wether the player is currently alive.
     * @return True if the player is alive, false if it is dead.
     */
    public boolean isIsPlayerAlive() {
        return isPlayerAlive;
    }
    
    /**
     * Set and init a new game.
     * @param game The game to init.
     * @param numberOfPlayers The number of players.
     */
    public void setGame(Game game, int numberOfPlayers) {
        //pause the current level
        this.pause();
        
        //delete the AIs
        this.AIs.clear();
        
        //set the number of players and the game
        this.numberOfPlayers = numberOfPlayers;
        this.game = game;
        
        //reset lifes and points
        this.resetLifes();
        this.resetPoints();
        
        //set and init the first level
        this.setCurrentLevel(0);
    }
    
    /**
     * Sets and inits the level with the given index.
     * @param levelIndex The index of the level to init.
     */
    public void setCurrentLevel(int levelIndex) {
        this.pause();
        
        this.timingCoordinator.reset();
        
        //if the level cannot be started => not level is set
        if(levelIndex < 0 ||
                levelIndex >= this.game.getLevels().size()) {
            this.currentLevelIndex = -1;
            this.currentWaveIndex = -1;
        }
        else {
            //set the level and the first wave
            this.currentLevelIndex = levelIndex;
            this.currentWaveIndex = 0;
            
            //init the level
            this.initializer.initLevel(this.getCurrentLevel(), this.numberOfPlayers, 0);
            this.isPlayerAlive = true;
        
            //notify
            this.notificationManager.notifyLevelStarted(this.getCurrentLevel(), this.currentLevelIndex);
        } 
    }
    
    /**
     * Returns the current level.
     * @return The current level.
     */
    public Level getCurrentLevel() {
        //Returns the current level or null if there is no current level
        try {
            return this.game.getLevels().get(this.currentLevelIndex);
        }
        catch(IndexOutOfBoundsException e) {
            return null;
        }
    }
    
    /**
     * Sets the current level to the next level.
     */
    public void setNextLevel() {
        //Only set the next level if there is a current level (-1 means no level)
        if(this.currentLevelIndex >= 0) {
            this.setCurrentLevel(this.currentLevelIndex + 1);
        }
    }
   
    /**
     * Starts the next level.
     */
    public void startNextLevel() {
        //Starts the next level only if the player is alive and the current level is finished
        if(this.isPlayerAlive && this.isLevelFinished) {
            this.setNextLevel();
            
        }
    }
    
    /**
     * Lets the player respawn.
     */
    public void respawn() {
        //Only let the player respawn if
        // - the player is dead
        // - the level is not finished
        // - the player has more lifes
        if(!this.isPlayerAlive && !this.isLevelFinished && this.getLifes() > 0) {
            this.timingCoordinator.reset();

            //init the level again
            this.initializer.initLevel(this.getCurrentLevel(), this.numberOfPlayers, this.currentWaveIndex);
        
            this.isPlayerAlive = true;
            
            //notify
            this.notificationManager.notifyRespawn(this.getCurrentLevel(), this.currentWaveIndex);
        }
    }
    
    /**
     * Sets and starts the next wave.
     */
    void setNextWave() {
        this.pause();
        
        this.currentWaveIndex++;
        if(this.currentWaveIndex >= this.getCurrentLevel().getEnemyWaves().size()) {
            //notify that the level is finished when there are no more waves
            this.notificationManager.notifyLevelFinished(this.getCurrentLevel(), this.currentLevelIndex);
        }
        else {
            //init the new wave
            this.initializer.initWave(this.currentWaveIndex);
            
            //start the new wave
            this.start();
        }
    }
    
    /**
     * Shoots with the first player if it exists.
     * @throws IndexOutOfBoundsException If there is no player.
     */
    public void shootWithPlayer() {
        this.shootWithPlayer(0);
    }
        
    /**
     * Shoots with the player given by playerIndex.
     * @param playerIndex The indes of the player to shoot with.
     * @throws IndexOutOfBoundsException If the player given by playerIndex does not exist.
     */
    public void shootWithPlayer(int playerIndex) {
        
        //ignore invalid shoots
        if(playerIndex < 0 || playerIndex >= this.numberOfPlayers ||
                !this.isPlayerAlive || this.isPaused()) {
            return;
        }
        
        try {
            this.world.getPlayers().get(playerIndex).shoot(); 
        }
        //ignore shooting if the player is currently dead
        catch (IndexOutOfBoundsException e) {}
    }  
    
    /**
     * Changes the weapon of the first player.
     */
    public void changePlayerWeapon() {
        this.changePlayerWeapon(0);
    }
        
    /**
     * Changes the weapon of the player with the given index.
     * @param playerIndex The index of the player to change the weapon.
     */
    public void changePlayerWeapon(int playerIndex) {
        
        try {
            //irgnore invalid changes
            if(playerIndex < 0 || playerIndex >= this.numberOfPlayers) {
                return;
            }
        
            if(this.isPlayerAlive && !this.isPaused()) {
            
                    this.world.getPlayers().get(playerIndex).changeWeapon();
                }
        }
        //ignore if the player is currently dead
        catch (IndexOutOfBoundsException e) {}
        
    }  
    
    /**
     * Moves the first player horicontally by x.
     * @param x The value to move the player.
     * @throws MapBorderException If the player would be outside the world.
     * @throws IndexOutOfBoundsException If there is no player.
     */
    public void movePlayer(int x)  {
        this.movePlayer(x, 0);
    }
    
    /**
     * Moves the player given by playerIndex horicontally by x.
     * @param x The value to move the player.
     * @param playerIndex The indes of the player to shoot with.
     * @throws MapBorderException If the player would be outside the world.
     * @throws IndexOutOfBoundsException If the player given by playerIndex does not exist.
     */
    public void movePlayer(int x, int playerIndex) {
       //only move the player if it is alive and the game is not paused
        if(this.isPlayerAlive && !this.isPaused()) {
            try {
                //irgnore invalid moves
                if(playerIndex < 0 || playerIndex >= this.numberOfPlayers) {
                    return;
                }
                
                //invert the move direction if the world is inverted
                if(this.world.isInverted()) {
                    x = -x;
                }
                
                int newPlayerPos = this.getPlayerPosition(playerIndex) + x;
                int playerWidth = this.world.getPlayers().get(playerIndex).getWidth();

                if((newPlayerPos >= 0) &&
                    ((newPlayerPos + playerWidth) <= this.world.getWidth())) {
                    //move the player if it does not leave the world
                    
                    this.world.getPlayers().get(playerIndex).moveX(x);
                }
                
                
            }
            //ignore if the player is currently dead
            catch (IndexOutOfBoundsException e) {}
       }
    }
    
    /**
     * Returns the x position of player 1.
     * @return The x position of player 1, -1 if the player is dead.
     */
    public int getPlayerPosition() {
        return this.getPlayerPosition(0);
    }
    
    /**
     * Returns the x position of the player with the given index.
     * @param playerIndex The index of the player.
     * @return The x position of player, -1 if the player is dead or does not exist.
     */
    public int getPlayerPosition(int playerIndex) {
        if(playerIndex < 0 || playerIndex >= this.numberOfPlayers) {
            throw new IndexOutOfBoundsException();
        }
        
        try {
        
            if(playerIndex < this.world.getPlayers().size() && playerIndex >= 0) {
                return this.world.getPlayers().get(playerIndex).getX();
            }
            else {
                return -1;
            }
        }
        //return -1 if the player is currently dead
        catch (IndexOutOfBoundsException e) { return -1; }
    }
    
    /**
     * Returns the weapons of the first player.
     * @return The weapons if the first player, null if the player is dead.
     */
    public List<Weapon> getPlayerWeapons() {
        return this.getPlayerWeapons(0);
    }
    
    /**
     * Returns the wepaons of the player with the given index.
     * @param playerIndex The index of the player.
     * @return The wepaons of the player, null if the player is dead or does not exist.
     */
    public List<Weapon> getPlayerWeapons(int playerIndex) {
        if(playerIndex < 0 || playerIndex >= this.numberOfPlayers) {
            throw new IndexOutOfBoundsException();
        }
        
        try {
        
            if(playerIndex < this.world.getPlayers().size() && playerIndex >= 0) {
                return this.world.getPlayers().get(playerIndex).getWeapons();
            }
            else {
                return null;
            }
        }
        //return null if the player is currently dead
        catch (IndexOutOfBoundsException e) { return null; }
    }
    
    /**
     * Returns the current weapon index of the first player.
     * @return The current weapon index of the first player, -1 if the player is dead.
     */
    public int getCurrentWeaponIndex() {
        return this.getCurrentWeaponIndex(0);
    }
    
    /**
     * Returns the current weapon index of the player with the given index.
     * @param playerIndex The index of the player
     * @return The current weapon index of the player, -1 if the player is dead or does not exist.
     */
    public int getCurrentWeaponIndex(int playerIndex) {
        try {
            return this.world.getPlayers().get(playerIndex).getCurrentWeaponIndex();
        }
        catch(IndexOutOfBoundsException e) {
            return -1;
        }
    }
    
    /**
     * Returns the number of current lifes.
     * @return The number of current lifes.
     */
    public int getLifes() {
        return this.lifes;
    }
    
    /**
     * Returns the number of full lifes
     * @return The number of full lifes.
     */
    public int getFullLifes() {
        return FULL_LIFES;
    }
    
    /**
     * Add new lifes.
     * @param numberOfLifes The number of lifes to add.
     */
    void addLifes(int numberOfLifes, GameListener.LifeAddReason reason) {
        this.lifes += numberOfLifes;
        
        //do not add more lifes than the number of full lifes
        if(this.lifes > this.getFullLifes()) {
            this.lifes = this.getFullLifes();
        }
        
        //notify
        this.notificationManager.notifiyLifesChanged(this.getLifes());
        this.notificationManager.notifyLifesAdded(numberOfLifes, reason);
        
    }
 
    /**
     * Remove one life and emit game over if there are no more lifes.
     */
    void removeLife(GameListener.LifeLostReason reason) {
        this.pause();
        
        //only remove life if:
        // - there is a life to remove
        // - the player is currently alive
        if((this.lifes > 0) && this.isPlayerAlive) {
            //remove the life
            this.lifes--;

            //set that the player is dead
            this.isPlayerAlive = false;
            
            //notify
            this.notificationManager.notifiyLifesChanged(this.lifes);
            this.notificationManager.notifyDeath(reason);

            //if there are no more lifes => game over
            if(this.lifes <= 0) {
                this.notificationManager.notifyGameOver();
            }
        }
    }
    
    /**
     * Reset the lifes to the number of full lifes.
     */
    void resetLifes() {
        
        this.lifes = this.getFullLifes();
        
        //notify
        this.notificationManager.notifiyLifesChanged(this.lifes);
    }

    /**
     * Returns the points.
     * @return The current points.
     */
    public int getPoints() {
        return this.points;
    }
    
    /**
     * Add points.
     * @param points The points to add. 
     */
    void addPoints(int points, GameListener.PointReason reason) {
        this.points += points;
        
        //notify
        this.notificationManager.notifiyPointsChanged(this.points);
        this.notificationManager.notifyPointsAchieved(points, reason);
    }
    
    /**
     * Clear the points (set the to 0).
     */
    void resetPoints() {

        this.points = 0;
        
        //notify
        this.notificationManager.notifiyPointsChanged(0);        
    }
    
    /**
     * Returns the size of the zone where the player loses a life if an enemy enters it.
     * @return The size of the deadzone.
     */
    int getDeadzone() {
        //the deadzone has the hight of the player height
        return this.getCurrentLevel().getPlayerPrototype(0).getHeight();
    }

    /**
     * Inits a mirror action.
     * @param time The time of the mirror action (time until it resets).
     */
    public void initMirror(int time) {
        this.timingCoordinator.initMirror(time);
    }   
    
    /**
     * Inits the reflecting world.
     * @param time The time of reflecting world (time until it resets).
     */
    public void initReflectingWorld(int time) {
        this.timingCoordinator.initReflecting(time);
    }
}