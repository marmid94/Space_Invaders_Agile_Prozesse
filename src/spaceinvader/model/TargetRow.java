package spaceinvader.model;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * A row of targets that are moved together.
 * @author Martin Schmid
 * @version 2.0
 */
public class TargetRow {
    /**
     * The targets controlled by the row.
     */
    private final List<AbstractTarget> targets;
    
    /**
     * The original index of the targets in the row.
     * The original is the index the target had in the row when it was added to the row.
     */
    private List<Integer> originalIndexList;
    
    /**
     * The world where the row belongs to.
     */
    private World world;
    
    /**
     * The initial x and y position of the row (upper left side).
     */
    private int x, y;
    
    /**
     * The current x offset to the initial x position to calculate the position of the next added target.
     */
    private int currentXOffset;
    
    /**
     * The horicontal space between two targets.
     */
    private final int targetXPadding;
    
    /**
     * Defines wethe the row is currently moving left or right.
     */
    private boolean isDirectionRight;
    
    /**
     * The default moving steps.
     */
    private final int horicontalStep, verticalStep;
    
    /**
     * Defines wether a vertical step is done when reaching the horicontal borders.
     */
    private final boolean moveVerticalOnBorders;

    /**
     * Constructor.
     * @param targetXPadding The horicontal space between two targets.
     * @param horicontalStep The default horicontal moving step.
     * @param verticalStep The default vertical moving step.
     * @param moveVerticalOnBorders Defines wether a vertical step is done when reaching the horicontal borders.
     */
    public TargetRow(int targetXPadding, 
            int horicontalStep, 
            int verticalStep, 
            boolean moveVerticalOnBorders) {
        
        
        this.targetXPadding = targetXPadding;
        this.horicontalStep = horicontalStep;
        this.verticalStep = verticalStep;
        this.moveVerticalOnBorders = moveVerticalOnBorders;
        
        this.targets = new CopyOnWriteArrayList<>();
        this.world = null;
        this.x = -1;
        this.y = -1;
        this.currentXOffset = 0;
        this.isDirectionRight = true;
        
        this.originalIndexList = new LinkedList<>();
    }
    
    /**
     * Places the row in a world.
     * @param world The world to place the row in.
     * @param x The x position of the first added target.
     * @param y The y position of the first added target.
     * @throws MapBorderException If the x or y position would be outside the world.
     * @throws IllegalStateException If the world has already been set.
     * @throws IllegalArgumentException If world is null.
     */
    void setWorld(World world, int x, int y) throws MapBorderException, IllegalStateException, IllegalArgumentException {
        if(this.world != null) {
            throw new IllegalStateException("setWorld() has already been called.");
        }
        if(world == null) {
            throw new IllegalArgumentException("world");
        }
        
            if((x < 0) || (x > world.getWidth()) || (y < 0) || (y > world.getHeight())) {
                throw new MapBorderException(
                        -1, 
                        -1, 
                        x, 
                        y, 
                        world.getWidth(), 
                        world.getHeight());
            }
  
            this.world = world;
            this.x = x;
            this.y = y;
        }
    
    
    /**
     * Returns wether the row contains targtes.
     * @return True if the row does not contain targtes, false if it does.
     */
    public boolean isEmpty() {
        return this.targets.isEmpty();
    }
    
    /**
     * Returns the targets beeing controlled by the row.
     * @return The targets beeing controlled by the row as unmodifiable list.
     */
    public List<AbstractTarget> getTargets() {
        return Collections.unmodifiableList(this.targets);
    }
    
    /**
     * Adds an enemy to the target row.
     * @param enemy The enemy to add.
     */
    void addEnemy(Enemy enemy) {
        this.addEnemy(enemy, 0, 0);
    }
    
    /**
     * Adds an enemy with the given offset to the target row.
     * @param enemy The enemy to add.
     * @param xOffset The xOffset of the enemy.
     * @param yOffset The yOffset of the enemy.
     */
    void addEnemy(Enemy enemy, int xOffset, int yOffset) {
        if(this.world == null) {
            throw new IllegalStateException("The world has to be set before calling add target.");
        }
        
        if(enemy == null) {
            throw new NullPointerException("The attribute enemy must not be null.");
        }
        
        this.world.addEnemy(enemy, 
                this.x + this.currentXOffset + xOffset, 
                this.y + yOffset);

        //add the index to the original index list
        this.originalIndexList.add(this.targets.size());
        
        this.targets.add(enemy);
        
       
        //update the current x offset so that the next element will be placed right to the current element
        this.currentXOffset += enemy.getWidth() + this.targetXPadding;
    }
    
    /**
     * Adds a new target to the row and the world.
     * It will be placed on the right side of the previous added element with targetXPadding between them.
     * @param target The target to add.
     * @throws MapBorderException If the added target would be outside the world.
     * @throws IllegalStateException If the world has not been set.
     * @throws IllegalArgumentException If target is null.
     */
    void addTarget(AbstractTarget target) throws MapBorderException, IllegalStateException, IllegalArgumentException {
        this.addTarget(target, 0, 0);
    }
    
    /**
     * Adds a new target to the row and the world.
     * It will be placed on the right side of the previous added element with targetXPadding between them.
     * @param target The target to add.
     * @param xOffset The horicontal offset from the position the target would normally be placed.
     * @param yOffset The vertical offset from the position the target would normally be placed.
     * @throws MapBorderException If the added target would be outside the world.
     * @throws IllegalStateException If the world has not been set.
     * @throws IllegalArgumentException If target is null.
     */
    void addTarget(AbstractTarget target, int xOffset, int yOffset) throws MapBorderException, IllegalStateException, IllegalArgumentException {
        if(this.world == null) {
            throw new IllegalStateException("The world has to be set before calling add target.");
        }
        
        if(target == null) {
            throw new NullPointerException("The attribute target must not be null.");
        }
        
        
        this.world.addTarget(target, 
                this.x + this.currentXOffset + xOffset, 
                this.y + yOffset);

        //add the index to the original index list
        this.originalIndexList.add(this.targets.size());
        
        this.targets.add(target);

        //update the current x offset so that the next element will be placed right to the current element
        this.currentXOffset += target.getWidth() + this.targetXPadding;
    }
    
    /**
     * Removes a target from the row.
     * It will not be removed form the world.
     * @param target The target to remove.
     */
    void removeTarget(AbstractTarget target) {
        int targetIndex = this.targets.indexOf(target);
        
        if(targetIndex >= 0) {
            //if the target still exists in the row 
            // => remove the index from the original index list
            this.originalIndexList.remove(targetIndex);
        
            this.targets.remove(target);
        }
    }
    
    /**
     * Moves all targets in the row horiconzally by the value of horicontalStep.
     * If it reaches the world borders it will stop or reflect depending on "doReverseOnBorders".
     * If it reaches the world border and "moveVertocallOnBorders" is true, moveY() will be called.
     * @throws MapBorderException If a target would be moved outside the world.
     * @throws IllegalStateException If the world has not been set.
     */
    void moveX() throws MapBorderException, IllegalStateException {
        this.moveX(this.horicontalStep);
    }
    
    /**
     * Moves all targets in the row vertically by the value of verticalStep.
     * @throws MapBorderException If a target would be moved outside the world.
     * @throws IllegalStateException If the world has not been set.
     */
    void moveY() throws MapBorderException, IllegalStateException {
        this.moveY(this.verticalStep);
    }
    
    /**
     * Moves all targets in the row horiconzally by x.
     * If it reaches the world borders it will stop or reflect depending on "doReverseOnBorders".
     * If it reaches the world border and "moveVertocallOnBorders" is true, moveY() will be called.
     * @param x The value to move the targets horicontally.
     * @throws MapBorderException If a target would be moved outside the world.
     * @throws IllegalStateException If the world has not been set.
     */
    void moveX(int x) throws MapBorderException, IllegalStateException {
        
        if(this.world == null) {
            throw new IllegalStateException("The world has to be set before calling moveX.");
        }
        
        
            int xRest;

            if(this.isDirectionRight) {
                //row is moving right
                
                xRest = (this.getRight() + x) - this.world.getWidth();
                if(xRest > 0) {
                    
                    //stop it on the right border
                    x = this.world.getWidth() - this.getRight();
                }
            }
            else {
                //row is moving left
                
                xRest = x - this.getLeft();
                if(xRest > 0) {
                    //if a target would leave the world
                    
                    //stop it on the left border
                    x = this.getLeft();
                }

                
            }

            if(!this.isDirectionRight) {
                x = -x;
            }
            
            //move all the targets
            for (AbstractTarget target : targets) {
                target.moveX(x);
            }
            
            if(xRest > 0) {
                
        
                
                //change direction when at the borders
                this.isDirectionRight = !this.isDirectionRight;
                if(this.moveVerticalOnBorders) {
                    //do a vertical move when on the borders
                    this.moveY();
                }
            }
            
            this.x += x;
        }
    
    
    /**
     * Moves all targets in the row vertically by y.
     * @param y The value to move the targets vertically.
     * @throws MapBorderException If a target would be moved outside the world.
     * @throws IllegalStateException If the world has not been set.
     */
    void moveY(int y) throws MapBorderException, IllegalStateException {
        if(this.world == null) {
            throw new IllegalStateException("The world has to be set before calling moveX.");
        }
        
           
        if(this.world.isInverted()) {
            y *= -1;
        }
        
        
            //check wether the targets can be moved without leaving the world
            if((this.getTop() + y) < 0) {
                throw new MapBorderException(this.x, 
                        this.y, 
                        this.x, 
                        this.y + y, 
                        this.world.getWidth(), 
                        this.world.getHeight());
            }
            else if((this.getBottom() + y) > this.world.getHeight()) {
                //remove targets that leave the world
                for(AbstractTarget target : this.targets) {
                    if((target.getY() + target.getHeight()) > this.world.getHeight()) {
                        this.world.removeTarget(target);
                    }
                }
            }

            //move all the targtes
            for (AbstractTarget target : targets) {
                try {
                    target.moveY(y);
                }
                catch(MapBorderException e) {
                    this.world.removeTarget(target);
                }
            }
            
            
            
            this.y += y;
            
            if(this.targets.size() <= 0) {
                throw new MapBorderException(-1, -1, -1, -1, this.world.getWidth(), 
                        this.world.getHeight());
            }
        }
    
    
    /**
     * Calculates the left position of the row.
     * @return The left position of the row.
     */
    private int getLeft() {
        int left = this.world.getWidth();
        for(AbstractTarget target : this.targets) {
            if(target.getX() < left) {
                left = target.getX();
            }
        }
        return left;
    }
    
    /**
     * Calculates the right position of the row.
     * @return The right position of the row.
     */
    private int getRight() {
        int right = this.x;
        for(AbstractTarget target : this.targets) {
            if((target.getX() + target.getWidth()) > right) {
                right = target.getX() + target.getWidth();
            }
        }
        
        return right;
    }
    
    /**
     * Calculates the top position of the row.
     * @return The top position of the row.
     */
    private int getTop() {
        int top = this.world.getHeight();
        for(AbstractTarget target : this.targets) {
            if(target.getY() < top) {
                top = target.getY();
            }
        }
        return top;
    }
    
    /**
     * Calculates the bottom position of the row.
     * @return The bottom position of the row.
     */
    private int getBottom() {
        int bottom = this.y;
        for(AbstractTarget target : this.targets) {
            if((target.getY() + target.getHeight()) > bottom) {
                bottom = target.getY() + target.getHeight();
            }
        }
        return bottom;
    }

    /**
     * Return the list of the original indexes.
     * @return The list of the original indexes.
     */
    public List<Integer> getOriginalIndexList() {
        return originalIndexList;
    }
}