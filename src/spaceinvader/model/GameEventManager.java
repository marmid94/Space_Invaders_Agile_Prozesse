package spaceinvader.model;

import java.util.LinkedList;
import java.util.List;
import spaceinvader.model.level.EnemyWave;
import spaceinvader.model.level.Level;

/**
 * Class managing events in a game.
 * @author Fabian Haag
 * @version 1.0
 */
public class GameEventManager {
    /**
     * The game controller.
     */
    private GameController controller;
    
    /**
     * The repaint listeners, listening to events.
     */
    private List<RepaintListener> repaintListeners;
    
    /**
     * The game listeners, listening to events.
     */
    private List<GameListener> gameListeners;
    
    /**
     * Constructor.
     * @param controller The game controller. 
     */
    public GameEventManager(GameController controller) {
        this.controller = controller;

        this.repaintListeners = new LinkedList<>();
        this.gameListeners = new LinkedList<>();
    }
    
    /**
     * Adds a repaint listener.
     * @param listener The listener that will be informed on repaint events.
     */
    public void addRepaintListener(RepaintListener listener) {
        this.repaintListeners.add(listener);
    }
    
    /**
     * Removes a repaint listener.
     * @param listener The listener to remove.
     */
    public void removeRepaintListener(RepaintListener listener) {
        this.repaintListeners.remove(listener);
    }
    
    /**
     * Adds a game listener.
     * @param listener The listener that will be informed on game events.
     */
    public void addGameListener(GameListener listener) {
        this.gameListeners.add(listener);
    }
    
    /**
     * Removes a game listener.
     * @param listener The listener to remove.
     */
    public void removeListener(GameListener listener) {
        this.gameListeners.remove(listener);
    }
    
    /**
     * Notify the repaint listeners that a repaint is needed.
     */
    void notifiyRepaint() {
        for(RepaintListener listener : this.repaintListeners) {
            listener.onRepaint();
        }
    }
    
    /**
     * Notify the repaint listeners that the number of lifes changed.
     * @param lifes The new number of lifes.
     */
    void notifiyLifesChanged(int lifes) {
        for(RepaintListener listener : this.repaintListeners) {
            listener.onLifesChanged(lifes);
        }
    }
    
    /**
     * Notify the repaint listeners that the number of points changed.
     * @param points The new number of points.
     */
    void notifiyPointsChanged(int points) {
        for(RepaintListener listener : this.repaintListeners) {
            listener.onPointsChanged(points);
        }
    }
    
    /**
     * Notify the game listeners that the game is won.
     */
    void notifyWin() {
        for(GameListener listener : this.gameListeners) {
            listener.onWin();
        }
    }
    
    /**
     * Notify the game listeners that the game is lost.
     */
    void notifyGameOver() {
        for(GameListener listener : this.gameListeners) {
            listener.onGameOver();
        }
    }

    /**
     * Notify the game listeners that a new level is started.
     * @param level The level that started.
     * @param levelIndex The index of the level that started.
     */
    void notifyLevelStarted(Level level, int levelIndex) {
        //the level just started => it is not finished
        this.controller.setIsLevelFinished(false);
        
        for(GameListener listener : this.gameListeners) {
            listener.onLevelStarted(level, levelIndex);
        }
    }
   
    /**
     * Notify the game listeners that the current level is finished.
     * @param level The level that was finished.
     * @param levelIndex The index of the level that was finished.
     */
    void notifyLevelFinished(Level level, int levelIndex) {
        //pause the game
        this.controller.pause();
        
        //safe that the level is finished
        this.controller.setIsLevelFinished(true);
        
        //inform the listeners
        for(GameListener listener : this.gameListeners) {
            listener.onLevelFinished(level, levelIndex);
        }
        
        //if it was the last level that is finished
        if((this.controller.getGame().getLevelIndex(level) + 1) >=
                this.controller.getGame().getLevels().size()) {
            
            
            if(this.controller.getCurrentLevel().isWinOnEnemiesDeadEnabled()) {
                //if win on enemies dead is enabled => the game is won
                for(GameListener listener : this.gameListeners) {
                    listener.onWin();
                }
            }
            else {
                //if not => the game is lost (not enough points but no more enemies to kill and get points)
                for(GameListener listener : this.gameListeners) {
                    listener.onGameOver();
                }
            }
        }
    }
    
    /**
     * Notify the game listeners that the player respawned.
     * @param level The current level.
     * @param waveIndex The index of the current wave.
     */
    void notifyRespawn(Level level, int waveIndex) {
        for(GameListener listener : this.gameListeners) {
            listener.onRespawn(level, waveIndex);
        }
    }

    /**
     * Notify the game listeners that th game is continued or started.
     */
    void notifyStart() {
        for(GameListener listener : this.gameListeners) {
            listener.onStart();
        }
    }
    
    /**
     * Notify the game listeners that the game is paused or stopped.
     */
    void notifyStop() {
        for(GameListener listener : this.gameListeners) {
            listener.onStop();
        }
    }
    
    /**
     * Notify the game listeners that the player was killed.
     * @param reason The reason why the player was killed.
     */
    void notifyDeath(GameListener.LifeLostReason reason) {
        for(GameListener listener : this.gameListeners) {
            listener.onDeath(reason);
        }
    }

    /**
     * Notify the game listeners that a player achieved points.
     * @param points The number of points that were achieved.
     * @param reason The reason of the points.
     */
    void notifyPointsAchieved(int points, GameListener.PointReason reason) {
        //inform the listeners
        for(GameListener listener : this.gameListeners) {
            listener.onPointsAchieved(points, reason);
        }
        
        //if win on points is enabled and enough points have been reached => next level
        if(this.controller.getCurrentLevel().isPointsToWinEnabled() &&
                this.controller.getPoints() >= this.controller.getCurrentLevel().getPointsToWin()) {
            //pause
            this.controller.pause();
        
            //set the last level
            this.controller.setCurrentLevel(this.controller.getGame().getLevels().size());
            
            this.controller.setIsLevelFinished(true);
            
            //inform that the gam is won
            for(GameListener listener : this.gameListeners) {
                listener.onWin();
            }
        }
    }
    
    /**
     * Notify the game listeners that a target was hit.
     * @param target The hit target.
     * @param projectil The projectil hitting the target.
     * @param damage The damage on the target.
     * @param wasKilled States wether the target was killed.
     */
    void notifyHit(AbstractTarget target, Projectil projectil, int damage, boolean wasKilled) {        
        int oldX = target.getX();
        int oldY = target.getY();
        
        if(wasKilled) {
            //if the target was killed => remove it
            this.controller.getWorld().removeTarget(target);
        }
        
        //inform the listeners
        for(GameListener listener : this.gameListeners) {
            listener.onHit(target, projectil, damage, wasKilled, oldX, oldY);
        }
        
        if(wasKilled) {
            
            if(Enemy.class.isInstance(target)) {
                //if an enemy was killed
                
                //get the enemy points
                this.controller.addPoints(((Enemy)target).getWinPoints(), 
                        GameListener.PointReason.ENEMY_KILL);
                
                //start the next wave if there are no more enemies in this wave
                if(this.controller.getWorld().getEnemies().isEmpty()) {
                    this.controller.setNextWave();
                }
                
                
            }
            else if(Player.class.isInstance(target)) {
                //if a player was killed => remove a life
                this.controller.removeLife(GameListener.LifeLostReason.PLAYER_HIT);
            }
        }
    }
    
    /**
     * Notify the game listeners that a target was healed.
     * @param target The healed target.
     * @param heal The number of healed points.
     * @param wasFullHeal True if the target was completely healed, false if not.
     */
    void notifyHeal(AbstractTarget target, int heal, boolean wasFullHeal) {
        for(GameListener listener : this.gameListeners) {
            listener.onHeal(target, heal, wasFullHeal);
        }
    }
    

   /**
     * Notify that a target has been moved.
     * @param target The moved target.
     */
    void notifyTargetMove(AbstractTarget movedTarget) {
        if(Enemy.class.isInstance(movedTarget)) {
            Enemy enemy = (Enemy)movedTarget;
            //if the moved target is an enemy
            // => check wether the enemy entered the deadzone (depending wether the world is inverted or not)
            if(this.controller.getWorld().isInverted()) {
                if((enemy.getY() <= this.controller.getDeadzone())) {
                    
                    this.controller.removeLife(GameListener.LifeLostReason.ENEMIES_REACHED_PLAYER);
                }
            }
            else {
                if((enemy.getY() + enemy.getHeight()) >= 
                        (this.controller.getWorld().getHeight() - this.controller.getDeadzone())) {
                    
                    this.controller.removeLife(GameListener.LifeLostReason.ENEMIES_REACHED_PLAYER);
                }
            }
        }
    }
    
    /**
     * Notify the game listeners that a multi kill was achieved.
     * @param kills The number of kills in the multi kill.
     * @param points The points achieved by the multi kill.
     */
    void notifyMultiKill(int kills, int points) {
        //add the points
        this.controller.addPoints(points, GameListener.PointReason.ACHIEVMENT);
        
        //inform the listeners
        for(GameListener listener : this.gameListeners) {
            listener.onMultiKill(kills, points);
        }
        
    }
    
    /**
     * Notify the game listeners that lifes have been added
     * @param numberOfLifes The number of lifes that have been added.
     * @param reason The reason, why lifes have been added.
     */
    void notifyLifesAdded(int numberOfLifes, GameListener.LifeAddReason reason) {
        for(GameListener listener : this.gameListeners) {
            listener.onLifesAdded(numberOfLifes, reason);
        }
    }
    
    /**
     * Notify the game listeners that a new wave has just started.
     * @param wave The started wave.
     * @param waveIndex The index of the started wave.
     */
    void notifyWaveStarted(EnemyWave wave, int waveIndex) {
        for(GameListener listener : this.gameListeners) {
            listener.onWaveStarted(wave, waveIndex);
        }
    }
    
    /**
     * Notify the game listeners that the selected player weapon changed.
     * @param player The player where the weapon changed.
     * @param wepaon The new selected weapon.
     * @param wepaonIndex The index of the new selected weapon.
     */
    void notifyPlayerWeaponChanged(Player player, Weapon wepaon, int wepaonIndex) {
        for(GameListener listener : this.gameListeners) {
            listener.onPlayerWeaponChanged(player, wepaon, wepaonIndex);
        }
    }
    
    /**
     * Notify the game listeners that a player moved.
     * @param player The player that moved.
     * @param xMove The x that the player was moved.
     */
    void notifyPlayerMoved(Player player, int xMove) {
        for(GameListener listener : this.gameListeners) {
            listener.onPlayerMoved(player, xMove);
        }
    }
    
    /** Notify the game listeners that a player move is blocked because of tilt.
     * @param player The player that is locked.
     */
    void notifyTilet(Player player) {
        for(GameListener listener : this.gameListeners) {
            listener.onTilt(player);
        }
    }
}
