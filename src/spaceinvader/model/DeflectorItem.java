package spaceinvader.model;

import java.awt.Image;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Item that creates a deflector when destroyed.
 * @author Fabian Haag
 * @version 1.0
 */
public class DeflectorItem extends AbstractItem {
    /**
     * States how long the deflector will live until it disappears.
     */
    private Deflector deflectorPrototype;
    
    /**
     * The GameController where the deflector will be added.
     */
    protected GameController controller;

    /**
     * Constructor.
     * @param image The image to paint for the object.
     * @param height The height of the object.
     * @param width The width of the object.
     * @param fullLivePoints The maximum live points of the object.
     * @param deflectorPrototype The prototype for the created deflector.
     */
    public DeflectorItem(Image image, int width, int height, int fullLivePoints, Deflector deflectorPrototype) {
        super(image, width, height, fullLivePoints);
        
        if(deflectorPrototype == null) {
            throw new NullPointerException("The argument deflectorPrototype must not be null.");
        }
        this.deflectorPrototype = deflectorPrototype;
    }
    
    @Override
    public void setController(GameController controller) {
        if(controller == null) {
            throw new NullPointerException("The argument controller must not be null.");
        }
        
        this.controller = controller;
    }
    
    @Override
    protected void performItemAction() {
        if(this.controller == null) {
            throw new IllegalStateException("The controller has to be set by the method setController before calling the method perform item action.");
        }
        
        for(Player player : this.controller.getWorld().getPlayers()) {
            try {
                Deflector deflector = (Deflector) this.deflectorPrototype.clone();
                this.controller.getWorld().addDeflector(deflector, player);
            } catch (CloneNotSupportedException ex) {
                Logger.getLogger(DeflectorItem.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}