package spaceinvader.model;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


/**
 * The world where all the objects are placed in.
 * @author Martin Schmid
 * @author Fabian Haag
 * @author Philip Müller
 * @version 2.0
 */
public class World {
    /**
     * The width and height of the world.
     */
    private int width, height;
    
    /**
     * The GameController controlling the world.
     */
    private GameController controller;
    
    /**
     * The objects managed by the world.
     * Those objects will be painted.
     */
    private final List<AbstractWorldObject> worldObjects;
    
    /**
     * The targets managed by the world.
     */
    private final List<AbstractTarget> targets;
    
    /**
     * The enemies managed by the world.
     */
    private final List<Enemy> enemies;
    
    /**
     * The players managed by the world.
     */
    private final List<Player> players;
    
    /**
     * The projectils managed by the world.
     */
    private final List<Projectil> projectils;
    
    /**
     * The target rows managed by the world.
     */
    private final List<TargetRow> targetRows;
    
    /**
     * The deflectors managed by the world.
     */
    private final List<Deflector> deflectors;
    
    /**
     * Stated wether the world is currently inverted (mirrored) or not.
     */
    private boolean isInverted;
    
    /**
     * Stated wether the world is currently reflecting (closed world).
     */
    private boolean isReflecting;
    
    /**
     * Constructor.
     * @param width The width of the world.
     * @param height The height of the world.
     * @param controller The GameController controlling the world.
     */
    public World(int width, int height, GameController controller) {
        if(width < 0) {
            throw new IllegalArgumentException("The value of the argument width must be >= 0.");
        }
        if(height < 0) {
            throw new IllegalArgumentException("The value of the argument width must be >= 0.");
        }
        if(controller == null) {
            throw new NullPointerException("The argument controller must not be null.");
        }
        
        this.width = width;
        this.height = height;
        
        this.controller = controller;
        
        //init the managed object lists
        //copy on write array lists will be used so that there are no concurrent modifications while the paint event is drawing.
        this.worldObjects = new CopyOnWriteArrayList<>();
        this.targets = new CopyOnWriteArrayList<>();
        this.enemies = new CopyOnWriteArrayList<>();
        this.players = new CopyOnWriteArrayList<>();
        this.projectils = new CopyOnWriteArrayList<>();
        this.targetRows = new CopyOnWriteArrayList<>();
        this.deflectors = new CopyOnWriteArrayList<>();
        
        //by default the world is not inverted
        this.isInverted = false;
    }
    
  
    /**
     * Returns the width of the world.
     * @return The width of the world.
     */
    public int getWidth() {
        return this.width;
    }
    
    /**
     * Returns the height of the world.
     * @return The height of the world.
     */
    public int getHeight() {
        return this.height;
    }

    /**
     * Returns the notification manager of the gamecontroller.
     * The notification manager is needed, so that the world objects can notify listeners.
     * @return The notification manager of the gamecontroller.
     */
    GameEventManager getNotificationManger() {
        return this.controller.getNotificationManager();
    }
    
    /**
     * Retursn wether the world is currently inverted (mirrored).
     * @return True, if the world is currently inverted, false if not.
     */
    public boolean isInverted() {
        return isInverted;
    }

    /**
     * Sets wether the world is currently inverted (mirrored).
     * @param isInverted States wether the world shall be inverted or not.
     */
    void setInverted(boolean isInverted) {
        if(isInverted != this.isInverted) {
            //if the invert state changes => invert the world
            
            this.invertWorld();
            
            this.isInverted = isInverted; 
        }
    }
    
    /**
     * Retursn wether the world is currently reflecting (closed world).
     * @return True, if the world is currently reflecting, false if not.
     */
    public boolean isReflecting() {
        return this.isReflecting;
    }

    /**
     * Sets wether the world is currently reflecting (closed world).
     * @param isReflecting  States wether the world shall be reflecting or not.
     */
    void setReflecting(boolean isReflecting) {
        this.isReflecting = isReflecting;
    }
    
    
    /**
     * Returns all the objects in the world.
     * @return A unmodifiable list of the objects in the world.
     */
    public List<AbstractWorldObject> getWorldObjects() {
        return Collections.unmodifiableList(this.worldObjects);
    }
    
    /**
     * Adds a world object to the world at the specified position.
     * @param worldObject The object to add.
     * @param x The x position of the object.
     * @param y The y position of the object.
     * @throws MapBorderException Thrown when the given position is outside of the world.
     */
    private void addWorldObject(AbstractWorldObject worldObject, int x, int y) throws MapBorderException{
        if(worldObject == null) {
            throw new NullPointerException("The argument worldObject must not be null.");
        }

        worldObject.setWorld(this, x, y);

        this.worldObjects.add(worldObject);
    }
    
    /**
     * Removes the given world object from the world.
     * @param worldObject  The object to remove.
     */
    private void removeWorldObject(AbstractWorldObject worldObject) {
        if(worldObject == null) {
            throw new NullPointerException("The argument worldObject must not be null.");
        }
        
        this.worldObjects.remove(worldObject);
        worldObject.resetWorld();
    }
    
    
    /**
     * Returns all the targets in the world.
     * @return A unmodifiable list of the targets in the world.
     */
    public List<AbstractTarget> getTargets() {
        return Collections.unmodifiableList(this.targets);
    }
    
    /**
     * Adds a target to the world at the specified position.
     * @param target The target to add.
     * @param x The x position of the target.
     * @param y The y position of the target.
     * @throws MapBorderException Thrown when the given position is outside of the world.
     */
    void addTarget(AbstractTarget target, int x, int y) throws MapBorderException {
        if(target == null) {
            throw new NullPointerException("The argument target must not be null.");
        }

        this.addWorldObject(target, x, y);
        
        this.targets.add(target);
    }
    
    /**
     * Removes the given target from the world.
     * @param target  The target to remove.
     */
    void removeTarget(AbstractTarget target) {
        if(target == null) {
            throw new NullPointerException("The argument target must not be null.");
        }
        
        this.targets.remove(target);
            
        this.removeWorldObject(target);
        
        
        //if it is an enemy => remove it from the enemy list as well
        if(Enemy.class.isInstance(target)) {
            this.enemies.remove((Enemy)target); 
        }
        //if it is a player => remove it from the player list as well
        else if(Player.class.isInstance(target)) {
            this.players.remove((Player)target); 
            for(Deflector deflector : this.deflectors) {
                if(deflector.getPlayer() == (Player)target) {
                    this.deflectors.remove(deflector);
                }
            }
        }
     
        //remove the target from the targetrow, if it is placed in one
        for(TargetRow row : this.targetRows) {
            row.removeTarget(target);
            if(row.isEmpty()) {
                //remove the target row it there are no more targets in it
                this.targetRows.remove(row);
            }
        }  
    }
    
    /**
     * Returns all the enemies in the world.
     * @return A unmodifiable list of the enemies in the world.
     */
    public List<Enemy> getEnemies() {
        return Collections.unmodifiableList(this.enemies);
    }
    
    /**
     * Adds an enemy to the world at the specified position.
     * @param enemy The enemy to add.
     * @param x The x position of the target.
     * @param y The y position of the target.
     * @throws MapBorderException Thrown when the given position is outside of the world.
     */
    void addEnemy(Enemy enemy, int x, int y) throws MapBorderException {
        if(enemy == null) {
            throw new NullPointerException("The argument enemy must not be null.");
        }

        this.addTarget(enemy, x, y);

        this.enemies.add(enemy);   
    }
    
    /**
     * Returns all the players in the world.
     * @return A unmodifiable list of the players in the world.
     */
    public List<Player> getPlayers() {
        return Collections.unmodifiableList(this.players);
    }
    
    /**
     * Adds a player to the world at the specified position.
     * @param player The player to add.
     * @param x The x position of the target.
     * @param y The y position of the target.
     * @throws MapBorderException Thrown when the given position is outside of the world.
     */
    void addPlayer(Player player, int x) throws MapBorderException {
      
        if(player == null) {
            throw new NullPointerException("The argument player must not be null.");
        }

        //if the world is inverted => add it at the top
        if(this.isInverted) {
            this.addTarget(player, x, 0);
        }
        //if the world is not inverted => add it at the bottom
        else {
            
            this.addTarget(player, x, this.getHeight() - player.getHeight());
            
        }

        this.players.add(player);  
    }
    
    /**
     * Returns all the projectils in the world.
     * @return A unmodifiable list of the projectils in the world.
     */
    public List<Projectil> getProjectils() {
        return Collections.unmodifiableList(this.projectils);
    }
    
    /**
     * Adds a projectil to the world at the specified position.
     * @param projectil The projectil to add.
     * @param x The x position of the target.
     * @param y The y position of the target.
     * @throws MapBorderException Thrown when the given position is outside of the world.
     */
    void addProjectil(Projectil projectil, int x, int y) throws MapBorderException {
        if(projectil == null) {
            throw new NullPointerException("The argument projectil must not be null.");
        }

        this.addWorldObject(projectil, x, y);

        this.projectils.add(projectil);
    }
    
    /**
     * Removes the given projectil from the world.
     * @param projectil  The projectil to remove.
     */
    void removeProjectil(Projectil projectil) {
        if(projectil == null) {
            throw new NullPointerException("The argument projectil must not be null.");
        }
        
        this.removeWorldObject(projectil);
        this.projectils.remove(projectil);
    }
    
    /**
     * Returns all the target rows in the world.
     * @return A unmodifiable list of the target rows in the world.
     */
    public List<TargetRow> getTargetRows() {
        return Collections.unmodifiableList(this.targetRows);
    }
    
    /**
     * Adds a target row to the world at the specified position.
     * @param row The row to add.
     * @param x The x position of the target.
     * @param y The y position of the target.
     * @throws MapBorderException Thrown when the given position is outside of the world.
     */
    void addTargetRow(TargetRow row, int x, int y) throws MapBorderException {
            if(row == null) {
                throw new NullPointerException("The argument row must not be null.");
            }

            row.setWorld(this, x, y);

            this.targetRows.add(row);
    }
    
    /**
     * Removes the given target row from the world.
     * @param row  The row to remove.
     */
    void removeTargetRow(TargetRow row) {
        if(row == null) {
            throw new NullPointerException("The argument row must not be null.");
        }
        
        this.targetRows.remove(row);
        for(AbstractTarget target : row.getTargets()) {
            this.removeTarget(target);
        }
    }
    
    /**
     * Adds a deflector to the given player in the world.
     * @param deflector The deflector to add.
     * @param player The player that is protected by the deflector.
     */
    void addDeflector(Deflector deflector, Player player) {
        if(deflector == null) {
                throw new NullPointerException("The argument deflector must not be null.");
        }

        
        deflector.setPlayer(player);

        this.addWorldObject(deflector, 0, 0);

        this.deflectors.add(deflector);
    }
    
    /**
     * Removes a deflector.
     * @param deflector The deflector to remove.
     */
    void removeDeflector(Deflector deflector) {
        if(deflector == null) {
                throw new NullPointerException("The argument deflector must not be null.");
        }
        
        this.deflectors.remove(deflector);
        this.removeWorldObject(deflector);
    }

    /**
     * Returns the deflectors managed by the world.
     * @return The deflectors managed by the world.
     */
    public List<Deflector> getDeflectors() {
        return this.deflectors;
    }
    
    /**
     * Remove all objects from the world.
     */
    void clearMap() {
            this.enemies.clear();
            this.players.clear();
            this.targets.clear();
            this.projectils.clear();
            this.worldObjects.clear(); 
            this.targetRows.clear();
    } 
    
    /**
     * Inverts the world.
     * Inverts the x and y position of all world objects.
     */
    private void invertWorld() {
        for(AbstractWorldObject object : this.worldObjects) {
            //Invert the x and y position for every object in the world
            int newX = this.getWidth() - (object.getX() + object.getWidth());
            int newY = this.getHeight()- (object.getY() + object.getHeight());
            
            object.setPosition(newX, newY);
        }
    }
}