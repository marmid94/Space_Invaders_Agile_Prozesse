package spaceinvader.model;

import java.awt.Image;

/**
 * A Deflector that protected the player against projectils.
 * @author Fabian Haag
 * @version 1.0
 */
public class Deflector extends AbstractWorldObject {
    /**
     * The player that is protected.
     */
    private Player player;
    
    /**
     * The distance to the player.
     */
    private int yDistanceToPlayer;
    
    /**
     * The remaining life time of the deflector.
     */
    private int remainingLifeTime;
    
    /**
     * Constructor.
     * @param image The image of the deflector.
     * @param width The width of the deflector.
     * @param height The height of the deflector.
     * @param yDistanceToPlayer The distance to the player.
     * @param lifeTime The life time of the delfector.
     */
    public Deflector(Image image, int width, int height, int yDistanceToPlayer, int lifeTime) {
        super(image, width, height);
        
        if(yDistanceToPlayer < 0) {
            throw new IllegalArgumentException("The value of the argument yDistanceToPlayer must be >= 0.");
        }
        if(lifeTime < 0) {
            throw new IllegalArgumentException("The value of the argument lifeTime must be >= 0.");
        }
        
        this.yDistanceToPlayer = yDistanceToPlayer;
        this.remainingLifeTime = lifeTime;
    }

    @Override
    public int getX() {
        if(super.getX() >= 0) {
            //if the delfecor is still alive => calculate the position by the player position
            int xPos = this.player.getX() + this.player.getWidth() / 2;
            xPos -= this.getWidth() / 2;
            
            if(xPos < 0) {
                xPos = 0;
            }
            else if(xPos + this.getWidth() > this.world.getWidth()) {
                xPos = this.world.getWidth() - this.getWidth();
            }
            return xPos;
        }
        else {
            //the deflector does not exist in the world anymore
            return -1;
        }
    }

    @Override
    public int getY() {
        if(super.getY() >= 0) {
            //if the delfecor is still alive => calculate the position by the player position
            int yPos;
            if(this.world.isInverted()) {
                yPos = this.player.getHeight() + this.yDistanceToPlayer;
            }
            else {
                //the deflector does not exist in the world anymore
                yPos = this.player.getY() - this.yDistanceToPlayer - this.getHeight();
            }    
            
            if(yPos < 0) {
                yPos = 0;
            }
            else if(yPos + this.getHeight() > this.world.getHeight()) {
                yPos = this.world.getHeight() - this.getHeight();
            }
            
            return yPos;
        }
        else {
            return -1;
        }
    }

    @Override
    public Image getImage() {
        if(this.player.getX() < 0) {
            return null;
        }
        else {
            return super.getImage(); 
        }
    }


    /** 
     * Returns the player that the deflector belongs to.
     * @return The player that the deflector belongs to.
     */
    public Player getPlayer() {
        return player;
    }
    
    /**
     * Sets the player that the deflector belongs to.
     * @param player The player that the deflector belongs to.
     */
    public void setPlayer(Player player) {
        if(player == null) {
            throw new NullPointerException("The argument player must not be null.");
        }
        
        this.player = player;
    }
    
    /**
     * Returns wether the affected projectil will be deflected.
     * @param projectil The affected projectil.
     * @return True if it will be deflected (destroyed), false if the projectil can pass.
     */
    boolean isDeflected(Projectil projectil) {
        return projectil.isEnemyProjectil();
    }
    
    /**
     * Updates (decrements) the life time and destroys the defelctor if there is no more life time.
     */
    void decrementLifeTime() {
        this.remainingLifeTime--;
        
        //remove the deflector if there is no more life time
        if(this.remainingLifeTime <= 0) {
            this.world.removeDeflector(this);
        }
    }

    @Override
    boolean checkHit(AbstractWorldObject otherObject) {
        return otherObject.position.intersects(this.getX(), this.getY(), this.getWidth(), this.getHeight());
    } 
}