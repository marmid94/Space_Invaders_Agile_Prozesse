package spaceinvader.model;

import java.awt.Image;
import java.awt.Rectangle;

/**
 * An object that can be placed in the world on a specific position.
 * @author Philip Müller
 * @version 1.1
 */
public abstract class AbstractWorldObject implements Cloneable {
    /**
    * The position and size of the object.
    */
    protected Rectangle position;
    
    /**
    * The world where the object is placed in.
    */
    protected World world;
    
    /**
     * The image to paint for the object.
     */
    protected Image image;
    
    /**
    * Constructor, setting the size and image of the object.
    * @param image The image to paint for the object.
    * @param height The height of the object.
    * @param width The width of the object.
    */
    public AbstractWorldObject(Image image, int width, int height) {
        this.world = null;
        this.image = image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        this.position = new Rectangle(0, 0, width, height);
    }
    
    
    /**
     * Set the world and position of the object.
     * Will be called by the world when the object is added.
     * @param world The world to add the object to.
     * @param x The x position of the object in the world.
     * @param y The y position of the object in the world.
     * @throws MapBorderException If the given x or y is out of the world.
    */
    void setWorld(World world, int x, int y) throws MapBorderException {
        if(world == null) {
            throw new NullPointerException("The argument world must not be null.");
        }
        
        this.world = world;
        
        this.setPosition(x, y);
    }
    
    /**
     * Resets the world.
     * After the operation the object does not belong to any world any more.
     */
    void resetWorld() {
        this.world = null;
        this.position.x = -1;
        this.position.y = -1;
    }

    /**
     * Sets the new position of the object in the world.
     * @param x The new x position.
     * @param y The new y position.
     */
    void setPosition(int x, int y) {
        if((x < 0) || ((x + this.getWidth()) > this.world.getWidth()) ||
                (y < 0) || ((y + this.getHeight()) > this.world.getHeight())) {
            //if the position is out of bounds
            throw new MapBorderException(-1, 
                    -1, 
                    x, 
                    y, 
                    world.getWidth(), 
                    world.getHeight());
        }

        this.position.x = x;
        this.position.y = y;
    }
    
    /**
     * Returns the x position of the object in the world.
     * @return The x position.
     */
    public int getX() {
        return this.position.x;
    }
    
    /**
     * Returns the y position of the object in the world.
     * @return The y position.
     */
    public int getY() {
        return this.position.y;
    }
    
    /**
     * Returns the height of the object.
     * @return The height of the object.
     */
    public int getWidth() {
        return this.position.width;
    }
    
    /**
     * Returns the width of the object.
     * @return The width of the object.
     */
    public int getHeight() {
        return this.position.height;
    }
    
    /**
     * Returns the image to be painted for this object.
     * @return The image to be painted for this object
     */
    public Image getImage() {
        return this.image;
    }
    
    /**
     * Moves the object horicontal.
     * No collision check will be done here.
     * @param x The value to move the object horicontal.
     * @throws MapBorderException If the object would be moved out of the world.
     */
    void moveX(int x) throws MapBorderException {
        synchronized(this.world) {

            int tmpX = this.position.x += x;
            if((tmpX < 0) ||((tmpX + this.getWidth()) > this.world.getWidth())) {
                throw new MapBorderException(this.position.x, 
                        this.position.y,
                        tmpX,
                        this.position.y,
                        this.world.getWidth(),
                        this.world.getHeight());
            }

            this.position.x = tmpX;
            
        }
    }
    
    /**
     * Moves the object vertical.
     * No collision check will be done here.
     * @param y The value to move the object vertical.
     * @throws MapBorderException If the object would be moved out of the world.
     */
    void moveY(int y) throws MapBorderException {
        synchronized(this.world) {
            
            int tmpY = this.position.y += y;
            if((tmpY < 0) ||((tmpY + this.getHeight()) > this.world.getHeight())) {
                throw new MapBorderException(this.position.x, 
                        this.position.y,
                        this.position.x,
                        tmpY,
                        this.world.getWidth(),
                        this.world.getHeight());
            }

            this.position.y = tmpY;
            
        }
        
    }
    
    /**
     * Returns wether the given other object collides with this object.
     * @param otherObject The object to check.
     * @return True if the other object collides with this, false if not.
     */
    boolean checkHit(AbstractWorldObject otherObject){
        return this.position.intersects(otherObject.position) || otherObject.position.intersects(this.position);
    }   
    
    @Override
    public AbstractWorldObject clone() throws CloneNotSupportedException {
        AbstractWorldObject object = (AbstractWorldObject) super.clone();
        
        object.position = new Rectangle(object.position);
        
        return object;
    }
}