package spaceinvader.model;

import java.util.List;
import java.util.Random;
import spaceinvader.model.level.EnemyWave;
import spaceinvader.model.level.Level;
import spaceinvader.model.level.LevelItem;

/**
 * The class for initializing a new level.
 * It generates all the world objects for a new level.
 * @author Martin Schmid
 * @version Fabian Haag
 * @version 1.0
 */
public class LevelInitializer {
    /**
     * The game controller to initialize the level for.
     */
    private GameController controller;
    
    /**
     * Tha paddings and steps for the spawned enemy rows.
     */
    private static final int ENEMY_X_PADDING = 5;
    private static final int ENEMY_Y_PADDING = 5;
    private static final int ENEMY_X_STEP = 1;

    /**
     * Constructor.
     * @param controller The GameController.
     */
    public LevelInitializer(GameController controller) {
        if(controller == null) {
            throw new NullPointerException("The argument controller must not be null.");
        }
        this.controller = controller;
    }
    
    /**
     * Inits a new level by spawning the enemy rows and the player(s).
     * @param level The level to init.
     * @param playerCount The number of players to spawn.
     * @param wave The wave of the level to init.
     */
    void initLevel(Level level, int playerCount, int wave) {
        //reset the world
        this.controller.getWorld().clearMap();
        this.controller.getWorld().setInverted(false);
        this.controller.getWorld().setReflecting(false);
        
        //spawn players and wave
        this.initPlayers(playerCount);
        this.initWave(wave);
        
        //notifiy for repaint
        this.controller.getNotificationManager().notifiyRepaint();
    }
    
    /**
     * Init an spawn a new wave.
     * @param waveIndex The index of the wave to spawn. 
     */
    void initWave(int waveIndex) {
        
        //only init a new wave if:
        // - a level is loaded
        // - the wave index is valid
        if(this.controller.getCurrentLevel() != null && waveIndex >= 0 &&
                waveIndex < this.controller.getCurrentLevel().getEnemyWaves().size()) {
            
            //get the wave for the wave index
            EnemyWave wave = this.controller.getCurrentLevel().getEnemyWaves().get(waveIndex);
            int yPos = 0;
            
            
            for(List<Enemy> row : wave.getEnemyRows()) {
                //spawn all the rows
                
                //create the row
                int height = getRowHeight(row);
                TargetRow createdRow = new TargetRow(
                        ENEMY_X_PADDING, 
                        ENEMY_X_STEP, 
                        height + ENEMY_Y_PADDING, 
                        true);

                //add the row (if inverted on the bottom, if not on the top
                if(this.controller.getWorld().isInverted()) {
                    this.controller.getWorld().addTargetRow(
                            createdRow, 
                            0, 
                            this.controller.getWorld().getHeight() - height - yPos);
                }
                else {
                    this.controller.getWorld().addTargetRow(
                            createdRow,
                            0, 
                            yPos);
                }


                //add the enmies to the row
                for(Enemy enemy : row) {
                    this.addEnemyToRow(enemy, createdRow);
                }

                
                //update the current yPos so that the next row will be placed under the previous row
                yPos += getRowHeight(row) + ENEMY_Y_PADDING;
                
            } 
        }
        
        
        //notify
        this.controller.getNotificationManager().notifiyRepaint();
        this.controller.getNotificationManager().notifyWaveStarted(this.controller.getCurrentWave(), waveIndex);
    }
    
    /**
     * Adds an enemy prototype to the given row.
     * @param enemyPrototype The enemy prototype to add.
     * @param row The row to add the enemy to.
     */
    private void addEnemyToRow(Enemy enemyPrototype, TargetRow row) {
        //calc the item propability sum
        int itemProbabilitySum = 0;
        for(LevelItem item : this.controller.getCurrentLevel().getItems()) {
            itemProbabilitySum += item.getProperbility();
        }
        
        //if there are no item to add => add a new enemy
        if(itemProbabilitySum == 0) {
            try {
                row.addEnemy((Enemy) enemyPrototype.clone());
            } catch (CloneNotSupportedException ex) {}
        }
        else {
            //randomly choose which item to add
            
            
            Random rand = new Random();
            int randValue = rand.nextInt(itemProbabilitySum);
            int currentValue = 0;
            int itemIndex = 0;
            
            LevelItem item;
            
            //choose the which item to add
            do {
                item = this.controller.getCurrentLevel().getItems().get(itemIndex);
                itemIndex++;
                currentValue += item.getProperbility();
            }
            while(currentValue < randValue);
            
            //radmomly decide wether to add the choosen item or an enemy
            if(item.getProperbility() > rand.nextInt(100)) {
                //add the item
                try {
                    item.getItemPrototype().setController(this.controller);
                    row.addTarget((AbstractTarget) item.getItemPrototype().clone());
                } catch (CloneNotSupportedException ex) {}
            }
            else {
                //add an enemy
                try {
                    row.addEnemy((Enemy) enemyPrototype.clone());
                } catch (CloneNotSupportedException ex) {}
            }
        }
    }

    /**
     * Returns the height of the row.
     * @param row The list of enemies in the row.
     * @return The height of the row.
     */
    private static int getRowHeight(List<Enemy> row) {
        //calc the maximum height of the enemies in the row
        int height = 0;
        for(Enemy enemy : row) {
            height = Math.max(height, enemy.getHeight());
        }
        return height;
    }
    
    /**
     * Inits the players.
     * @param playerCount The number of player to init. 
     */
    void initPlayers(int playerCount) {
        //only init players if a level is loaded
        if(this.controller.getCurrentLevel() != null) {
            //calc the x pos of the player depending on the number of players
            int xDelta = (int) Math.round((double)this.controller.getWorld().getWidth() / (double)(playerCount + 1));

            int xPos = xDelta - (this.controller.getCurrentLevel().getPlayerPrototype(0).getWidth() / 2);
          
            Player player;
            //get the prototypes for the players an add them
            for(int i = 0; i < playerCount; i++) {
                try {
                    //get the prototype
                    player = (Player)this.controller.getCurrentLevel().getPlayerPrototype(i).clone();

                    //add the player add the calculated x position but in the world
                    if(xPos < 0) {
                        this.controller.getWorld().addPlayer(player, 0);
                    }
                    else if((xPos + player.getWidth()) > this.controller.getWorld().getWidth()) {
                        this.controller.getWorld().addPlayer(player, this.controller.getWorld().getWidth() - player.getWidth());
                    }
                    else {
                        this.controller.getWorld().addPlayer(player, xPos);
                    }

                    xPos += xDelta;
                } catch (CloneNotSupportedException ex) {}
            }   
        }
    }
}