package spaceinvader.model;

import java.awt.Image;

/**
 * An enemy that shoots on the player
 * @author Philip Müller
 * @version 1.1
 */
public class Enemy extends AbstractTarget {
    /**
     * The weapon that the enemy shoots with.
     */
    protected Weapon weapon;
    
    /**
     * The priority of the enemy that is relevant for the enemy shooting algorithm of the world.
     */
    protected int priority;
    
    /**
     * The points that will be achieved when the enemy is killed.
     */
    protected int winPoints;
    
    
    /**
     * Constructor, setting the full live points, win points priority, the weapon, size and image of the target.
     * @param image The image to paint for the object.
     * @param height The height of the object.
     * @param width The width of the object.
     * @param fullLivePoints The maximum live points of the object.
     * @param winPoints The win points that are achieved when the enemy is killed.
     * @param priority The priority of the enemy.
     * @param weapon The weapon the enemy is shooting with.
     */
    public Enemy(Image image, 
            int width, 
            int height, 
            int fullLivePoints, 
            int winPoints,
            int priority, 
            Weapon weapon) {
        super(image, width, height, fullLivePoints);
        
        if(winPoints < 0) {
            throw new IllegalArgumentException("The value of the argument winPoints must >= 0.");
        }
        
        if(priority < 0 || priority > 1000) {
            throw new IllegalArgumentException("The value of the argument priority must >= 0 und <= 1000.");
        }
        
        if(weapon == null) {
            throw new NullPointerException("The argument weapon must not be null.");
        }
        
        this.winPoints = winPoints;
        this.priority = priority;
        this.weapon = weapon;
    }
    
    /**
     * Returns the weapon that the enemy shoots with.
     * @return The weapon that the enemy shoots with.
     */
    public Weapon getWeapon() {
        return this.weapon;
    }
    
    /**
     * Returns the priority of the enemy.
     * @return The priority of the enemy.
     */
    public int getPriority() {
        return this.priority;
    }
    
    /**
     * Returns the win points that will be achieved when the enemy is killed.
     * @return The win points of the enemy.
     */
    public int getWinPoints()
    {
        return this.winPoints;
    }
    
    /**
     * Shoots with the enemy.
     * Creates projectils from the enemy weapon and fires them.
     */
    void shoot() {
        if(this.world.isInverted()) {
            //create the projectil on the top and horicontal middle of the enemy
            this.weapon.createProjectil(true, 
                    this.getX() + ((this.getWidth() - this.weapon.getPrototype().getWidth()) / 2),
                    this.getY() - this.weapon.getPrototype().getHeight());
        }
        else {
            //create the projectil on the bottom and horicontal middle of the enemy
            this.weapon.createProjectil(true, 
                    this.getX() + ((this.getWidth() - this.weapon.getPrototype().getWidth()) / 2),
                    this.getY() + this.getHeight());
        }
    }

    @Override
    protected void destroy() {}

    @Override
    void setWorld(World world, int x, int y) throws MapBorderException {
        this.weapon.setWorld(world);
        super.setWorld(world, x, y); 
    }

    @Override
    public AbstractWorldObject clone() throws CloneNotSupportedException {
        Enemy object = (Enemy) super.clone(); 
        object.weapon = (Weapon) weapon.clone();
        return object;
    }
}