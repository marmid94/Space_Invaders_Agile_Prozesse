package spaceinvader.model;

import java.awt.Image;

/**
 * Item that inits the reflecting world when destroyed.
 * @author Martin Schmid
 * @version 1.0
 */
public class ReflectingWorldItem extends AbstractItem {
    /**
     * States how long the reflecting world shall last.
     */
    private int reflectingTime;
    
    /**
     * The GameController where the deflector will be added.
     */
    protected GameController controller;
    
    /**
     * Constructor.
     * @param image The image to paint for the object.
     * @param height The height of the object.
     * @param width The width of the object.
     * @param fullLivePoints The maximum live points of the object.
     * @param reflectingTime States how long the reflecting world shall last.
     */
    public ReflectingWorldItem(Image image, int width, int height, int fullLivePoints, int reflectingTime) {
        super(image, width, height, fullLivePoints);
        
        if(reflectingTime < 0) {
            throw new IllegalArgumentException("The value of the argument reflectingTime must be >= 0.");
        }
        this.reflectingTime = reflectingTime;
    }
    
    @Override
    public void setController(GameController controller) {
        if(controller == null) {
            throw new NullPointerException("The argument controller must not be null.");
        }
        
        this.controller = controller;
    }

    @Override
    protected void performItemAction() {
        if(this.controller == null) {
            throw new IllegalStateException("The controller has to be set by the method setController before calling the method perform item action.");
        }
        
        this.controller.initReflectingWorld(this.reflectingTime);
    }
    
}