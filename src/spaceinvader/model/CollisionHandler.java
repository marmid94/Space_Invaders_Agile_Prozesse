package spaceinvader.model;

import java.awt.geom.Ellipse2D;

/**
 * A class for recognizing and handling collission between projectils and targets.
 * @author Philip Mueller
 * @version 1.0
 */
public class CollisionHandler {
    /**
     * The GameController where the collission handler belongs to.
     */
    private GameController controller;

    /**
     * Constructor.
     * @param controller The GameController where the collission handler belongs to.
     */
    public CollisionHandler(GameController controller) {
        if(controller == null) {
            throw new NullPointerException("The argument controller must not be null.");
        }
        this.controller = controller;
    }
    
    /**
     * Detects collisions of all projectils in the world and does collision handling (hits, damage).
     */
    void detectAndHandleProjectilCollissions() {
        
        //for all projectils in the world
        for(Projectil projectil : this.controller.getWorld().getProjectils()) {
            if(!this.checkAndHandleDeflectorHits(projectil)) {
                
                //if the projectil wasn't destroyed by a deflector
                // => check wether it hit a target
                this.checkAndHandleTargetHits(projectil);
            }
        }
    }
    
    /**
     * Checks and handles hits between the given projectil and the deflectors in the world.
     * @param projectil The projectil to check.
     * @return True if a deflector destroyed the projectil, false if the projectil was not destroyed.
     */
    private boolean checkAndHandleDeflectorHits(Projectil projectil) {
        //for all the deflectors in the world
        for(Deflector deflector : this.controller.getWorld().getDeflectors()) {
            //if:
            // - the deflector can deflect the projectil
            // - the projectil hit the deflector
            // - the projectil is valid and has not already been removed
            if(deflector.isDeflected(projectil) && 
                    deflector.checkHit(projectil) &&
                    projectil.getX() >= 0 && projectil.getY() >= 0) {
                
                //remove the projectil (without hit or explosion)
                this.controller.getWorld().removeProjectil(projectil);
                
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * Checks and handles hits between the given projectil and the targets in the world.
     * @param projectil The projectil to check.
     */
    private void checkAndHandleTargetHits(Projectil projectil) {
        //for all the targets in the world
        for(AbstractTarget target : this.controller.getWorld().getTargets()) {

            //check for hit
            if(((!Player.class.isInstance(target) && !projectil.isDirectionDown()) ||
                (Player.class.isInstance(target) && projectil.isDirectionDown())) &&
                    target.checkHit(projectil)) {

                this.handleHit(projectil, target);
            }
        }
    }
    
    /**
     * Handles the hit between a projectil and a target.
     * Calculates damge and other hit projectils (e.g. for bombs)
     * @param projectil The affected projectil.
     * @param target The affected target.
     */
    private void handleHit(Projectil projectil, AbstractTarget target) {
        
        //only handle valid projectils, some might already have been removed from the world (x<0)
        if(projectil.getX() >= 0 && projectil.getY() >= 0) {
        
            if(projectil.getDamageRadius() == 0) {
                //simple projectil, no bomb => hit the affected target
                target.hit(projectil);
            }
            else {
                //bomb projectil => calculate the damage radius

                int hitX = projectil.getX() + (projectil.getWidth() / 2);
                int hitY = projectil.getY() + (projectil.getHeight() / 2);

                hitX -= projectil.getDamageRadius();
                hitY -= projectil.getDamageRadius();

                Ellipse2D hitCircle = new Ellipse2D.Double(
                    (double)hitX, 
                    (double)hitY, 
                    (double)(2 * projectil.getDamageRadius()), 
                    (double)(2 * projectil.getDamageRadius()));

                this.handleBombHit(hitCircle, projectil);
            }

            //remove the affected projectil (it was destroyed as well)
            this.controller.getWorld().removeProjectil(projectil);
        }
    }
    
    /**
     * Handles a bomb hit.
     * Hits all the targets in the damage radius given by hitCircle.
     * @param hitCircle The circle where targets will be affected.
     * @param projectil The affected projectil.
     */
    private void handleBombHit(Ellipse2D hitCircle, Projectil projectil) {
        //hit all targets in the damage circle
        for(AbstractTarget circleHitTarget : this.controller.getWorld().getTargets()) {
            if(hitCircle.intersects(circleHitTarget.position)) {
                circleHitTarget.hit(projectil);
            }
        } 
    }
}