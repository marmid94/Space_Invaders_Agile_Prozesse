package spaceinvader.model;

import spaceinvader.model.level.EnemyWave;
import spaceinvader.model.level.Level;

/**
 * The listener for all game events.
 * @author Stefan Fruhwirth
 * @version 1.0
 */
public interface GameListener {
    enum LifeLostReason { UNKNOWN, PLAYER_HIT, ENEMIES_REACHED_PLAYER }
    
    enum PointReason { UNKNOWN, ITEM, ENEMY_KILL, ACHIEVMENT}
    
    enum LifeAddReason { UNKNOWN, ITEM }
    
    void onWin();
    
    void onGameOver();

    void onLevelStarted(Level level, int levelIndex);
   
    void onLevelFinished(Level level, int levelIndex);

    void onRespawn(Level level, int waveIndex);

    void onStart();
    
    void onStop();
    
    void onDeath(LifeLostReason reason);
    
    void onLifesAdded(int numberOfLifes, LifeAddReason reason);

    void onPointsAchieved(int points, PointReason reason);
    
    void onHit(AbstractTarget target, Projectil projectil, int damage, boolean wasKilled, int x, int y);
    
    void onHeal(AbstractTarget target, int heal, boolean wasFullHeal);
    
    void onMultiKill(int kills, int points);
    
    void onWaveStarted(EnemyWave wave, int waveIndex);
    
    void onPlayerWeaponChanged(Player player, Weapon wepaon, int wepaonIndex);
    
    void onPlayerMoved(Player player, int xMove);
    
    void onTilt(Player player);
}