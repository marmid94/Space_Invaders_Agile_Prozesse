package spaceinvader.model.level;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import spaceinvader.model.Enemy;

/**
 * A manager that can load a level from the XML file (.game).
 * @author Fabian Haag
 * @author Martin Schmid
 * @version 1.0
 */
public class LevelLoader {
    public static final int DEFLECTOR_DISTANCE = 10;
    public static final int PLAYER_LIFES = 10;
    public static final int WAVE_SPEED = 10;
    
    private Level level;
    
    private class EnemyData {
        public int enemyLifes;
        public int enemyPoints;
        public int enemyFirerate;
        public String enemyWeapon;
        public String enemyType;
    }
    
    /**
     * Loads the level from the given source file.
     * @param sourceFile The source file of the level.
     * @return The loaded level.
     * @throws Exception Thrown if the level could not be loaded.
     */
    public Level loadLevelConfig(File sourceFile) throws Exception {
        //create a new level instance
        this.level = new Level();
        
        try {
           
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(sourceFile);
            
            doc.getDocumentElement().normalize();
            
            Element root = doc.getDocumentElement();
            
            NodeList list = root.getChildNodes();
            for (int i = 0; i < list.getLength(); i++) {
                
                this.parsePlayer(list.item(i));
                this.parseEnemyRows(list.item(i));
                this.parseItems(list.item(i));
                this.parseAchievments(list.item(i));
                this.parseWinType(list.item(i));
            }
            
            return this.level;
            
        } 
        catch (ParserConfigurationException | SAXException | IOException | ParseException e) {
            throw new Exception("The config file could not be loaded", e);
        }
    }
    
    private void parsePlayer(Node currentNode) throws ParseException {
        if (currentNode != null &&
            currentNode.getNodeType() == Node.ELEMENT_NODE &&
            currentNode.getNodeName().equals("player")) {

            List<String> playerWeapons = new ArrayList<>();

            if(currentNode.getFirstChild() != null &&
                currentNode.getFirstChild().getNodeType() == Node.ELEMENT_NODE &&
                currentNode.getFirstChild().getNodeName().equals("weapons")) {

                NodeList weaponNodes = currentNode.getFirstChild().getChildNodes();

                for(int i = 0; i < weaponNodes.getLength(); i++) {
                    if(weaponNodes.item(i).getNodeType() == Node.ELEMENT_NODE &&
                        weaponNodes.item(i).getNodeName().equals("weapon")) {
                        playerWeapons.add(weaponNodes.item(i).getTextContent());
                    }
                    else {
                        throw new ParseException("The element 'weapons' must only contain 'weapon' elemnts.", 0);
                    }
                }

            }
            else {
                throw new ParseException("The element 'player' must contain an element 'weapons'.", 1);
            }

            this.level.setPlayerPrototypes(
                    GameLibrary.getInstance().createPlayerPrototype(
                            PLAYER_LIFES, 
                            playerWeapons.toArray(new String[playerWeapons.size()]), 
                            0), 
                    GameLibrary.getInstance().createPlayerPrototype(
                            PLAYER_LIFES, 
                            playerWeapons.toArray(new String[playerWeapons.size()]), 
                            1));

        }
    }
    
    private String getRowName(Node currentNode) throws ParseException {
        if(currentNode != null &&
            currentNode.getNodeType() == Node.ELEMENT_NODE &&
            currentNode.getNodeName().equals("name")) {

            return currentNode.getTextContent();
        }
        else {
            throw new ParseException("The element 'row' must conatin an element 'name'", 0);
        }
    }
    
    private int getWaveIndex(Node currentNode) throws ParseException {
        if(currentNode != null &&
            currentNode.getNodeType() == Node.ELEMENT_NODE &&
            currentNode.getNodeName().equals("wave")) {

            try {
                return Integer.parseInt(currentNode.getTextContent());
            }
            catch(NumberFormatException e) {
                throw new ParseException("The element 'wave' must conatin an Integer value", 0);
            }
        }
        else {
            throw new ParseException("The element 'row' must conatin an element 'wave'", 0);
        }
    }
    
    private int getEnemyCount(Node currentNode) throws ParseException {
        if(currentNode != null &&
            currentNode.getNodeType() == Node.ELEMENT_NODE &&
            currentNode.getNodeName().equals("monstercount")) {

            try {
                return Integer.parseInt(currentNode.getTextContent());
            }
            catch(NumberFormatException e) {
                throw new ParseException("The element 'monstercount' must conatin an Integer value", 0);
            }
        }
        else {
            throw new ParseException("The element 'row' must conatin an element 'monstercount'", 0);
        }
    }
    
    private EnemyData getEnemyData(Node currentNode) throws ParseException {
        if(currentNode != null &&
            currentNode.getNodeType() == Node.ELEMENT_NODE &&
            currentNode.getNodeName().equals("monster")) {

            EnemyData resultData = new EnemyData();
            
            //enemy lifes
            Node tempNode = currentNode.getFirstChild();
            if(tempNode != null &&
                tempNode.getNodeType() == Node.ELEMENT_NODE &&
                tempNode.getNodeName().equals("lifes")) {

                try {
                    resultData.enemyLifes = Integer.parseInt(tempNode.getTextContent());
                }
                catch(NumberFormatException e) {
                    throw new ParseException("The element 'lifes' must conatin an Integer value", 0);
                }
            }
            else {
                throw new ParseException("The element 'monster' must conatin an element 'lifes'", 0);
            }

            //enemy type
            tempNode = tempNode.getNextSibling();
            if(tempNode != null &&
                tempNode.getNodeType() == Node.ELEMENT_NODE &&
                tempNode.getNodeName().equals("type")) {

                resultData.enemyType = tempNode.getTextContent();
            }
            else {
                throw new ParseException("The element 'monster' must conatin an element 'type'", 0);
            }

            //enemy points
            tempNode = tempNode.getNextSibling();
            if(tempNode != null &&
                tempNode.getNodeType() == Node.ELEMENT_NODE &&
                tempNode.getNodeName().equals("points")) {

                try {
                    resultData.enemyPoints = Integer.parseInt(tempNode.getTextContent());
                }
                catch(NumberFormatException e) {
                    throw new ParseException("The element 'points' must conatin an Integer value", 0);
                }
            }
            else {
                throw new ParseException("The element 'monster' must conatin an element 'points'", 0);
            }


            //enemy weapon
            tempNode = tempNode.getNextSibling();
            if(tempNode == null ||
                tempNode.getNodeType() != Node.ELEMENT_NODE ||
                !tempNode.getNodeName().equals("enemyweapon")) {

                throw new ParseException("The element 'monster' must conatin an element 'enemyweapon'", 0);
            }
            
            tempNode = tempNode.getFirstChild();
            if(tempNode != null &&
                tempNode.getNodeType() == Node.ELEMENT_NODE &&
                tempNode.getNodeName().equals("ammunition")) {

                resultData.enemyWeapon = tempNode.getTextContent();
            }
            else {
                throw new ParseException("The element 'enemyWeapon' must conatin an element 'ammunition'", 0);
            }


            //enemy fire rate
            tempNode = tempNode.getNextSibling();
            if(tempNode != null &&
                tempNode.getNodeType() == Node.ELEMENT_NODE &&
                tempNode.getNodeName().equals("firerate")) {

                try {
                    resultData.enemyFirerate = Integer.parseInt(tempNode.getTextContent());
                }
                catch(NumberFormatException e) {
                    throw new ParseException("The element 'firerate' must conatin an Integer value", 0);
                }
            }
            else {
                throw new ParseException("The element 'monsterWeapon' must conatin an element 'firerate'", 0);
            }
            
            
            return resultData;
        }
        else {
            throw new ParseException("The element 'row' must conatin an element 'monster'", 0);
        }
    }
    
    private void parseEnemyRows(Node currentNode) throws ParseException {
        if (currentNode != null &&
            currentNode.getNodeType() == Node.ELEMENT_NODE &&
            currentNode.getNodeName().equals("enemyWaves")) {
                //stores the waves by their indices
            SortedMap<Integer, EnemyWave> waves = new TreeMap<>();

            for (int i = 0; i < currentNode.getChildNodes().getLength(); i++) {
                Node tempNode = currentNode.getChildNodes().item(i);

                if(tempNode.getNodeType() == Node.ELEMENT_NODE &&
                    tempNode.getNodeName().equals("row")) {

                    //name element
                    tempNode = tempNode.getFirstChild();
                    this.getRowName(tempNode);

                    //wave element
                    tempNode = tempNode.getNextSibling();
                    int waveIndex = this.getWaveIndex(tempNode);
                    
                    //enemy count element
                    tempNode = tempNode.getNextSibling();
                    int enemyCount = this.getEnemyCount(tempNode);
                    
                    
                    //enemy element
                    tempNode = tempNode.getNextSibling();
                    EnemyData enemyData = this.getEnemyData(tempNode);
                        
                    if(!waves.containsKey(waveIndex)) {
                        EnemyWave wave = new EnemyWave();
                        wave.setSpeed(WAVE_SPEED);
                        waves.put(waveIndex, wave);
                    }
                    
                    EnemyWave currentWave = (EnemyWave)waves.get(waveIndex);
                    
                    List<Enemy> currentRow = new LinkedList<>();
                    
                    for(int enemyIndex = 0; enemyIndex < enemyCount; enemyIndex++) {
                        currentRow.add(
                            GameLibrary.getInstance().createEnemyPrototype(
                                    enemyData.enemyType, 
                                    enemyData.enemyLifes, 
                                    enemyData.enemyPoints, 
                                    enemyData.enemyFirerate, 
                                    enemyData.enemyWeapon));
                    }
                    
                    currentWave.addRow(currentRow);
                    
                }
                else {
                    throw new ParseException("The element 'enemyRows' must conatin 'row' elements.", 0);
                }
            }
            
            
            for(EnemyWave wave : waves.values()) {
                this.level.addEnemyWave(wave);
            }
        }
    }
    
    private LevelItem getLifeItem(Node currentNode) throws ParseException {
        if(currentNode != null &&
            currentNode.getNodeType() == Node.ELEMENT_NODE &&
            currentNode.getNodeName().equals("lifeItem")) {
            
            //activated
            Node tempNode = currentNode.getFirstChild();
            boolean activated;
            if(tempNode != null &&
                tempNode.getNodeType() == Node.ELEMENT_NODE &&
                tempNode.getNodeName().equals("activated")) {
                
                activated = tempNode.getTextContent().equals("true");
            }
            else {
                throw new ParseException("The element 'lifeItem' must conatin an element 'activated'.", 0);
            }
            
            //propability
            tempNode = tempNode.getNextSibling();
            int propability;
            if(tempNode != null &&
                tempNode.getNodeType() == Node.ELEMENT_NODE &&
                tempNode.getNodeName().equals("propability")) {
                
                try {
                    propability = Integer.parseInt(tempNode.getTextContent());
                }
                catch(NumberFormatException e) {
                    throw new ParseException("The element 'propability' must conatin a valid ineteger.", 0);
                }
            }
            else {
                throw new ParseException("The element 'lifeItem' must conatin an element 'propability'.", 0);
            }
            
            if(activated) {
                return GameLibrary.getInstance().createLifeItemPrototype(propability);
            }
            else {
                return null;
            }
            
        }
        else {
            return null;
        }
    }
    
    private LevelItem getPointItem(Node currentNode) throws ParseException {
        if(currentNode != null &&
            currentNode.getNodeType() == Node.ELEMENT_NODE &&
            currentNode.getNodeName().equals("pointItem")) {
            
            //activated
            Node tempNode = currentNode.getFirstChild();
            boolean activated;
            if(tempNode != null &&
                tempNode.getNodeType() == Node.ELEMENT_NODE &&
                tempNode.getNodeName().equals("activated")) {
                
                activated = tempNode.getTextContent().equals("true");
            }
            else {
                throw new ParseException("The element 'pointItem' must conatin an element 'activated'.", 0);
            }
            
            //points
            tempNode = tempNode.getNextSibling();
            int points;
            if(tempNode != null &&
                tempNode.getNodeType() == Node.ELEMENT_NODE &&
                tempNode.getNodeName().equals("points")) {
                
                try {
                    points = Integer.parseInt(tempNode.getTextContent());
                }
                catch(NumberFormatException e) {
                    throw new ParseException("The element 'points' must conatin a valid ineteger.", 0);
                }
            }
            else {
                throw new ParseException("The element 'pointItem' must conatin an element 'points'.", 0);
            }
            
            //propability
            tempNode = tempNode.getNextSibling();
            int propability;
            if(tempNode != null &&
                tempNode.getNodeType() == Node.ELEMENT_NODE &&
                tempNode.getNodeName().equals("propability")) {
                
                try {
                    propability = Integer.parseInt(tempNode.getTextContent());
                }
                catch(NumberFormatException e) {
                    throw new ParseException("The element 'propability' must conatin a valid ineteger.", 0);
                }
            }
            else {
                throw new ParseException("The element 'pointItem' must conatin an element 'propability'.", 0);
            }
            
            if(activated) {
                return GameLibrary.getInstance().createPointItemPrototype(propability, points);
            }
            else {
                return null;
            }
            
        }
        else {
            return null;
        }
    }
    
    private LevelItem getMirrorItem(Node currentNode) throws ParseException {
        if(currentNode != null &&
            currentNode.getNodeType() == Node.ELEMENT_NODE &&
            currentNode.getNodeName().equals("mirrorItem")) {
            
            //activated
            Node tempNode = currentNode.getFirstChild();
            boolean activated;
            if(tempNode != null &&
                tempNode.getNodeType() == Node.ELEMENT_NODE &&
                tempNode.getNodeName().equals("activated")) {
                
                activated = tempNode.getTextContent().equals("true");
            }
            else {
                throw new ParseException("The element 'mirrorItem' must conatin an element 'activated'.", 0);
            }
            
            //time
            tempNode = tempNode.getNextSibling();
            int time;
            if(tempNode != null &&
                tempNode.getNodeType() == Node.ELEMENT_NODE &&
                tempNode.getNodeName().equals("duration")) {
                
                try {
                    time = Integer.parseInt(tempNode.getTextContent());
                }
                catch(NumberFormatException e) {
                    throw new ParseException("The element 'duration' must conatin a valid ineteger.", 0);
                }
            }
            else {
                throw new ParseException("The element 'mirrorItem' must conatin an element 'duration'.", 0);
            }
            
            //propability
            tempNode = tempNode.getNextSibling();
            int propability;
            if(tempNode != null &&
                tempNode.getNodeType() == Node.ELEMENT_NODE &&
                tempNode.getNodeName().equals("propability")) {
                
                try {
                    propability = Integer.parseInt(tempNode.getTextContent());
                }
                catch(NumberFormatException e) {
                    throw new ParseException("The element 'propability' must conatin a valid ineteger.", 0);
                }
            }
            else {
                throw new ParseException("The element 'mirrorItem' must conatin an element 'propability'.", 0);
            }
            
            if(activated) {
                return GameLibrary.getInstance().createMirrorItemPrototype(propability, time);
            }
            else {
                return null;
            }
            
        }
        else {
            return null;
        }
    }
    
    private LevelItem getReflectingItem(Node currentNode) throws ParseException {
        if(currentNode != null &&
            currentNode.getNodeType() == Node.ELEMENT_NODE &&
            currentNode.getNodeName().equals("reflectingItem")) {
            
            //activated
            Node tempNode = currentNode.getFirstChild();
            boolean activated;
            if(tempNode != null &&
                tempNode.getNodeType() == Node.ELEMENT_NODE &&
                tempNode.getNodeName().equals("activated")) {
                
                activated = tempNode.getTextContent().equals("true");
            }
            else {
                throw new ParseException("The element 'reflectingItem' must conatin an element 'activated'.", 0);
            }
            
            //time
            tempNode = tempNode.getNextSibling();
            int time;
            if(tempNode != null &&
                tempNode.getNodeType() == Node.ELEMENT_NODE &&
                tempNode.getNodeName().equals("duration")) {
                
                try {
                    time = Integer.parseInt(tempNode.getTextContent());
                }
                catch(NumberFormatException e) {
                    throw new ParseException("The element 'duration' must conatin a valid ineteger.", 0);
                }
            }
            else {
                throw new ParseException("The element 'reflectingItem' must conatin an element 'duration'.", 0);
            }
            
            //propability
            tempNode = tempNode.getNextSibling();
            int propability;
            if(tempNode != null &&
                tempNode.getNodeType() == Node.ELEMENT_NODE &&
                tempNode.getNodeName().equals("propability")) {
                
                try {
                    propability = Integer.parseInt(tempNode.getTextContent());
                }
                catch(NumberFormatException e) {
                    throw new ParseException("The element 'propability' must conatin a valid ineteger.", 0);
                }
            }
            else {
                throw new ParseException("The element 'reflectingItem' must conatin an element 'propability'.", 0);
            }
            
            if(activated) {
                return GameLibrary.getInstance().createReflectingItemPrototype(propability, time);
            }
            else {
                return null;
            }
            
        }
        else {
            return null;
        }
    }
    
    private LevelItem getDeflectorItem(Node currentNode) throws ParseException {
        if(currentNode != null &&
            currentNode.getNodeType() == Node.ELEMENT_NODE &&
            currentNode.getNodeName().equals("deflectorItem")) {
            
            //activated
            Node tempNode = currentNode.getFirstChild();
            boolean activated;
            if(tempNode != null &&
                tempNode.getNodeType() == Node.ELEMENT_NODE &&
                tempNode.getNodeName().equals("activated")) {
                
                activated = tempNode.getTextContent().equals("true");
            }
            else {
                throw new ParseException("The element 'deflectorItem' must conatin an element 'activated'.", 0);
            }
            
            //points
            tempNode = tempNode.getNextSibling();
            int time;
            if(tempNode != null &&
                tempNode.getNodeType() == Node.ELEMENT_NODE &&
                tempNode.getNodeName().equals("duration")) {
                
                try {
                    time = Integer.parseInt(tempNode.getTextContent());
                }
                catch(NumberFormatException e) {
                    throw new ParseException("The element 'duration' must conatin a valid integer.", 0);
                }
            }
            else {
                throw new ParseException("The element 'deflectorItem' must conatin an element 'duration'.", 0);
            }
            
            //propability
            tempNode = tempNode.getNextSibling();
            int propability;
            if(tempNode != null &&
                tempNode.getNodeType() == Node.ELEMENT_NODE &&
                tempNode.getNodeName().equals("propability")) {
                
                try {
                    propability = Integer.parseInt(tempNode.getTextContent());
                }
                catch(NumberFormatException e) {
                    throw new ParseException("The element 'propability' must conatin a valid ineteger.", 0);
                }
            }
            else {
                throw new ParseException("The element 'deflectorItem' must conatin an element 'propability'.", 0);
            }
            
            if(activated) {
                return GameLibrary.getInstance().createDeflectorItemPrototype(propability, time, DEFLECTOR_DISTANCE);
            }
            else {
                return null;
            }
            
        }
        else {
            return null;
        }
    }
    
    private void parseItems(Node currentNode) throws ParseException {
        if (currentNode != null &&
            currentNode.getNodeType() == Node.ELEMENT_NODE &&
            currentNode.getNodeName().equals("items")) {
            
            //go trhough all the items
            for(int i = 0; i < currentNode.getChildNodes().getLength(); i++) {
                LevelItem item = this.getLifeItem(currentNode.getChildNodes().item(i));
                
                if(item == null) {
                    item = this.getPointItem(currentNode.getChildNodes().item(i));
                }
                
                if(item == null) {
                    item = this.getMirrorItem(currentNode.getChildNodes().item(i));
                }
                
                if(item == null) {
                    item = this.getReflectingItem(currentNode.getChildNodes().item(i));
                }
                
                if(item == null) {
                    item = this.getDeflectorItem(currentNode.getChildNodes().item(i));
                }
                
                
                if(item != null) {
                    this.level.addItem(item);
                }
            }
        }
    }
    
    private void parseAchievments(Node currentNode) throws ParseException {
        if (currentNode != null &&
            currentNode.getNodeType() == Node.ELEMENT_NODE &&
            currentNode.getNodeName().equals("achievements")) {
                
            for(int i = 0; i < currentNode.getChildNodes().getLength(); i++) {
                Node tempNode = currentNode.getChildNodes().item(i);
                
                //get kill count
                int killCount;
                if(tempNode.getNodeType() == Node.ELEMENT_NODE &&
                    tempNode.getNodeName().equals("double")) {
                    killCount = 2;
                }
                else if(tempNode.getNodeType() == Node.ELEMENT_NODE &&
                    tempNode.getNodeName().equals("triple")) {
                    killCount = 3;
                }
                else {
                    throw new ParseException("The element 'achievements' must contain an element 'double' or 'triple'.", 0);
                }
                
                //activated
                tempNode = tempNode.getFirstChild();
                boolean activated = false;
                if(tempNode != null &&
                    tempNode.getNodeType() == Node.ELEMENT_NODE &&
                    tempNode.getNodeName().equals("activated")) {
                    
                    activated = tempNode.getTextContent().equals("true");
                }
                else {
                    throw new ParseException("The element 'double' or 'triple' must contain an element 'activated'.", 0);
                }
                
                //time
                tempNode = tempNode.getNextSibling();
                int time;
                if(tempNode != null &&
                    tempNode.getNodeType() == Node.ELEMENT_NODE &&
                    tempNode.getNodeName().equals("time")) {
                    
                    try {
                        time = Integer.parseInt(tempNode.getTextContent());
                    }
                    catch(NumberFormatException e) {
                        throw new ParseException("The element 'time' must contain a valid integer.", 0);
                    }
                }
                else {
                    throw new ParseException("The element 'double' or 'triple' must contain an element 'time'.", 0);
                }
                
                //points
                tempNode = tempNode.getNextSibling();
                int points;
                if(tempNode != null &&
                    tempNode.getNodeType() == Node.ELEMENT_NODE &&
                    tempNode.getNodeName().equals("points")) {
                    
                    try {
                        points = Integer.parseInt(tempNode.getTextContent());
                    }
                    catch(NumberFormatException e) {
                        throw new ParseException("The element 'points' must contain a valid integer.", 0);
                    }
                }
                else {
                    throw new ParseException("The element 'double' or 'triple' must contain an element 'points'.", 0);
                }
                
                
                if(activated) {
                    this.level.addMultiKillAchievment(new MultiKillAchievment(killCount, time, points));
                }
            }
        }
                
    }
    
    private void parseWinType(Node currentNode) throws ParseException {
        if (currentNode != null &&
            currentNode.getNodeType() == Node.ELEMENT_NODE &&
            currentNode.getNodeName().equals("winTypes")) {
            
            //first child is "winOnAllEnemiesDead"
            Node tempNode = currentNode.getFirstChild();

            if(tempNode != null &&
                tempNode.getNodeType() == Node.ELEMENT_NODE &&
                tempNode.getNodeName().equals("winOnAllEnemiesDead")) {

                level.setWinOnEnemiesDead(tempNode.getTextContent().equals("true"));
            }
            else {
                throw new ParseException("The element 'winTypes' must contain an element 'winOnAllEnemiesDead'.", 2);
            }

            //next sibling is "winOnPoints"
            tempNode = tempNode.getNextSibling();
            if(tempNode != null &&
                tempNode.getNodeType() == Node.ELEMENT_NODE &&
                tempNode.getNodeName().equals("winOnPoints")) {

                try {
                    int winPoints = Integer.parseInt(tempNode.getTextContent());
                
                    if (winPoints >= 0) {
                        level.enablePointsToWin(winPoints);
                    }
                    else {
                        level.disablePointsToWin();
                    }
                }
                catch(NumberFormatException e) {
                    throw new ParseException("The element 'winOnPoints' must contain a valid integer.", 3);
                }
            }
            else {
                throw new ParseException("The element 'winTypes' must contain an element 'winOnPoints'.", 4);
            } 
        }
    }
}