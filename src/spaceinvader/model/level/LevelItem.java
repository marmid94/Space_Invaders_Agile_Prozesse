package spaceinvader.model.level;

import spaceinvader.model.AbstractItem;

/**
 * An item and the propability that it appears in a level.
 * @author Fabian Haag
 * @author Martin Schmid
 * @version 1.0
 */
public class LevelItem {
    /**
     * The item prototype.
     */
    private AbstractItem itemPrototype;
    
    /**
     * The item propability.
     */
    private int propability;

    /**
     * Constructor.
     * @param itemPrototype The item prototype.
     * @param properbility The item propability.
     */
    public LevelItem(AbstractItem itemPrototype, int properbility) {
        if(properbility < 0) {
            properbility = 0;
        }
        else if(properbility > 100) {
            properbility = 100;
        }
        this.itemPrototype = itemPrototype;
        this.propability = properbility;
    }

    /**
     * Returns the item prototype.
     * @return The item prototype.
     */
    public AbstractItem getItemPrototype() {
        return itemPrototype;
    }

    /**
     * Returns the item propability.
     * @return The item propability.
     */
    public int getProperbility() {
        return propability;
    }
}