package spaceinvader.model.level;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import spaceinvader.gui.LevelEditor.EnemyRow;
import spaceinvader.gui.LevelEditor.LevelEditorController;

/**
 * A manager that can save a level from the LevelEditor to the XML file (.game).
 * @author Fabian Haag
 * @author Martin Schmid
 * @version 1.0
 */
public class LevelSaver {
    private String levelName;
    private String winOnAllEnemiesDeadBool;
    private String winOnPoints;
    private String pointItemboolean;
    private String pointItemPoints;
    private String pointItempropability;
    private String mirrorItemboolean;
    private String mirrorItemDuration;
    private String mirrorItempropability;
    private String lifeItemboolean;
    private String lifeItempropability;
    private String reflectingItemboolean;
    private String reflectingItemDuration;
    private String reflectingItempropability;
    private String deflectorItemboolean;
    private String deflectorItemDuration;
    private String deflectorItempropability;
    private String doubleboolean;
    private String doubletime;
    private String doublePoints;
    private String tripleboolean;
    private String tripletime;
    private String triplePoints;
    private List<String> weaponslist = new ArrayList();
    private List<EnemyRow> enemyRows = new ArrayList();
    
    /**
     * Safes the level from the level editor into a level file.
     * @param targetFile The file to save the level.
     * @param levelEditor The level editor.
     * @throws Exception Thrown if the level could not be safed.
     */
    public void saveLevelConfig(File targetFile, LevelEditorController levelEditor) throws Exception {
        getData(levelEditor);
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // root elements with all attributes
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("level");
            Attr attr = doc.createAttribute("name");
            attr.setValue(levelName);
            rootElement.setAttributeNode(attr);
            doc.appendChild(rootElement);

            //player element with all children
            Element player = doc.createElement("player");
            Element temp;
            Element weapons = doc.createElement("weapons");
            
            for(String weaponName : this.weaponslist) {
                Element weapon = doc.createElement("weapon");
                weapon.appendChild(doc.createTextNode(weaponName));
                weapons.appendChild(weapon);
            }
            
            player.appendChild(weapons);
            rootElement.appendChild(player);

            //winTypes with all children
            Element winTypes = doc.createElement("winTypes");
            temp = doc.createElement("winOnAllEnemiesDead");
            temp.appendChild(doc.createTextNode(winOnAllEnemiesDeadBool));
            winTypes.appendChild(temp);
            temp = doc.createElement("winOnPoints");
            temp.appendChild(doc.createTextNode(winOnPoints));
            winTypes.appendChild(temp);
            rootElement.appendChild(winTypes);

            //items with all children
            Element items = doc.createElement("items");
            temp = doc.createElement("pointItem");
            temp.appendChild(doc.createElement("activated")).appendChild(doc.createTextNode(pointItemboolean));
            temp.appendChild(doc.createElement("points")).appendChild(doc.createTextNode(pointItemPoints));
            temp.appendChild(doc.createElement("propability")).appendChild(doc.createTextNode(pointItempropability));
            items.appendChild(temp);
            temp = doc.createElement("mirrorItem");
            temp.appendChild(doc.createElement("activated")).appendChild(doc.createTextNode(mirrorItemboolean));
            temp.appendChild(doc.createElement("duration")).appendChild(doc.createTextNode(mirrorItemDuration));
            temp.appendChild(doc.createElement("propability")).appendChild(doc.createTextNode(mirrorItempropability));
            items.appendChild(temp);
            temp = doc.createElement("lifeItem");
            temp.appendChild(doc.createElement("activated")).appendChild(doc.createTextNode(lifeItemboolean));
            temp.appendChild(doc.createElement("propability")).appendChild(doc.createTextNode(lifeItempropability));
            items.appendChild(temp);
            temp = doc.createElement("reflectingItem");
            temp.appendChild(doc.createElement("activated")).appendChild(doc.createTextNode(reflectingItemboolean));
            temp.appendChild(doc.createElement("duration")).appendChild(doc.createTextNode(reflectingItemDuration));
            temp.appendChild(doc.createElement("propability")).appendChild(doc.createTextNode(reflectingItempropability));
            items.appendChild(temp);
            temp = doc.createElement("deflectorItem");
            temp.appendChild(doc.createElement("activated")).appendChild(doc.createTextNode(deflectorItemboolean));
            temp.appendChild(doc.createElement("duration")).appendChild(doc.createTextNode(deflectorItemDuration));
            temp.appendChild(doc.createElement("propability")).appendChild(doc.createTextNode(deflectorItempropability));
            items.appendChild(temp);
            
            rootElement.appendChild(items);

            //achievements with all children
            Element achievements = doc.createElement("achievements");
            temp = doc.createElement("double");
            temp.appendChild(doc.createElement("activated")).appendChild(doc.createTextNode(doubleboolean));
            temp.appendChild(doc.createElement("time")).appendChild(doc.createTextNode(doubletime));
            temp.appendChild(doc.createElement("points")).appendChild(doc.createTextNode(doublePoints));
            achievements.appendChild(temp);
            temp = doc.createElement("triple");
            temp.appendChild(doc.createElement("activated")).appendChild(doc.createTextNode(tripleboolean));
            temp.appendChild(doc.createElement("time")).appendChild(doc.createTextNode(tripletime));
            temp.appendChild(doc.createElement("points")).appendChild(doc.createTextNode(triplePoints));
            achievements.appendChild(temp);
            rootElement.appendChild(achievements);

            //enemywaves with all children
            Element enemyWaves = doc.createElement("enemyWaves");
            //iterate trhough list
            for(EnemyRow enemyRow : this.enemyRows) {
                Element row = doc.createElement("row");
                temp = doc.createElement("name");
                temp.appendChild(doc.createTextNode(enemyRow.getName()));
                row.appendChild(temp);
                
                temp = doc.createElement("wave");
                temp.appendChild(doc.createTextNode(Integer.toString(enemyRow.getWave())));
                row.appendChild(temp);
                
                temp = doc.createElement("monstercount");
                temp.appendChild(doc.createTextNode(Integer.toString(enemyRow.getNumber())));
                row.appendChild(temp);

                Element monster = doc.createElement("monster");
                temp = doc.createElement("lifes");
                temp.appendChild(doc.createTextNode(Integer.toString(enemyRow.getLivePoints())));
                monster.appendChild(temp);
                
                temp = doc.createElement("type");
                temp.appendChild(doc.createTextNode(enemyRow.getType()));
                monster.appendChild(temp);
                
                temp = doc.createElement("points");
                temp.appendChild(doc.createTextNode(Integer.toString(enemyRow.getPoints())));
                monster.appendChild(temp);
                

                Element enemyweapon = doc.createElement("enemyweapon");
                temp = doc.createElement("ammunition");
                temp.appendChild(doc.createTextNode(enemyRow.getWeapon()));
                enemyweapon.appendChild(temp);
                
                temp = doc.createElement("firerate");
                temp.appendChild(doc.createTextNode(Integer.toString(enemyRow.getRateOfFire())));
                enemyweapon.appendChild(temp);
                

                monster.appendChild(enemyweapon);
                row.appendChild(monster);
                enemyWaves.appendChild(row);
            }
            rootElement.appendChild(enemyWaves);

            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(targetFile);

            

            transformer.transform(source, result);

           
                
        } 
        catch (ParserConfigurationException | TransformerException e) {
            throw new Exception("Could not save the level.", e);
        }
    }
    
    /**
     * Gets the data from the level editor GUI.
     * @param controller The controller of the level editor.
     */
    private void getData(LevelEditorController controller) {
        //All options defined within PanelDescription
        
        this.levelName = controller.getLevelName();
 
        this.weaponslist = controller.getActivePlayerWeapons();
                
        //All options defined within PanelWinSituation
        this.winOnAllEnemiesDeadBool = Boolean.toString(controller.isWinOnAllEnemiesDeadEnabled());
        this.winOnPoints = Integer.toString(controller.getPointsForWin());
                
        //All options defined within PanelItems
        this.pointItemboolean = Boolean.toString(controller.getPointItemActive());
        this.pointItemPoints = Integer.toString(controller.getPointItemPoints());
        this.pointItempropability = Integer.toString(controller.getPointItemPorpability());
        this.mirrorItemboolean = Boolean.toString(controller.getMirrorItemActive());
        this.mirrorItemDuration = Integer.toString(controller.getMirrorItemTime());
        this.mirrorItempropability = Integer.toString(controller.getMirrorItemPorpability());
        this.lifeItemboolean = Boolean.toString(controller.getLifeItemActive());
        this.lifeItempropability = Integer.toString(controller.getLifeItemPorpability());
        this.reflectingItemboolean = Boolean.toString(controller.getReflectingItemActive());
        this.reflectingItemDuration = Integer.toString(controller.getReflectingItemTime());
        this.reflectingItempropability = Integer.toString(controller.getReflectingItemPorpability());
        this.deflectorItemboolean = Boolean.toString(controller.getDeflectorItemActive());
        this.deflectorItemDuration = Integer.toString(controller.getDeflectorItemTime());
        this.deflectorItempropability = Integer.toString(controller.getDeflectorItemPorpability());
        
        //All options within the PanelAchievemts
        this.doubleboolean = Boolean.toString(controller.isDoubleKillActive());
        this.doubletime = Integer.toString(controller.getDoubleKillTime());
        this.doublePoints = Integer.toString(controller.getDoubleKillPoints());
        this.tripleboolean = Boolean.toString(controller.isTripleKillActive());
        this.tripletime = Integer.toString(controller.getTripleKillTime());
        this.triplePoints = Integer.toString(controller.getTripleKillPoints());
                
       //All options within the PanelMonster in one big list
       //(name, wave, monstercount, lifes, type, points, ammunition, firerate)
        this.enemyRows = controller.getEnemyRows();
    }
}
