package spaceinvader.model.level;

/**
 * Class representing a multi kill achievment.
 * @author Fabian Haag
 * @author Martin Schmid.
 * @version 1.0
 */
public class MultiKillAchievment {
    /**
     * The number of kills needed for the achievment.
     */
    private int numberOfKills;
    
    /**
     * The time of the kills.
     */
    private int time;
    
    /**
     * The points of the achievment.
     */
    private int points;

    /**
     * Constructor.
     * @param numberOfKills The number of kills needed for the achievment.
     * @param time The time of the kills.
     * @param points The points of the achievment.
     */
    public MultiKillAchievment(int numberOfKills, int time, int points) {
        this.numberOfKills = numberOfKills;
        this.time = time;
        this.points = points;
    }

    /**
     * Returns the points of the achievment.
     * @return The points of the achievment.
     */
    public int getPoints() {
        return points;
    }

    /**
     * Returns the number of kills needed for the achievment.
     * @return The number of kills.
     */
    public int getNumberOfKills() {
        return numberOfKills;
    }

    /**
     * Returns the time in that the kills have to be made.
     * @return The time of the kills.
     */
    public int getTime() {
        return time;
    }

    /**
     * Sets the time in that the kills have to be made.
     * @param time The time of the kills.
     */
    public void setTime(int time) {
        this.time = time;
    }

    /**
     * Sets the points of the achievment.
     * @param points The points of the achievment.
     */
    public void setPoints(int points) {
        this.points = points;
    }
}