package spaceinvader.model.level;

import java.awt.Image;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.imageio.ImageIO;
import spaceinvader.model.Deflector;
import spaceinvader.model.DeflectorItem;
import spaceinvader.model.Enemy;
import spaceinvader.model.LifeItem;
import spaceinvader.model.MirrorItem;
import spaceinvader.model.Player;
import spaceinvader.model.PointItem;
import spaceinvader.model.Projectil;
import spaceinvader.model.ReflectingWorldItem;
import spaceinvader.model.Weapon;



public class GameLibrary  {
    private static GameLibrary instance;
    private GameLibrary() {
        this.defaultGames = new HashMap<>();
        this.enemyImages = new HashMap<>();
        this.enemyWeapons = new HashMap<>();
        this.tetrisEnemies = new HashMap<>();
    
        this.player1Image = player2Image = null;
        this.player1TetrisImage = player2TetrisImage = null;
        this.playerWeapons = new HashMap<>();
        this.playerTetrisWeapon = null;
    
        this.pointItemImage = null;
        this.mirrorItemImage = null;
        this.lifeItemImage = null;
        
        this.easterEggGame = null;
    }
    
    private URL gameMusic, deathSound, winSound, gameOverSound, killSound, hitSound, 
            newWaveSound, itemSound, tetrisMusic;
    
    
    private final Map<String, Game> defaultGames;
    
    private final Map<String, Image> enemyImages;
    private final Map<String, Weapon> enemyWeapons;
    
    private final Map<String, Enemy> tetrisEnemies;
    private Image player1TetrisImage, player2TetrisImage;
    private Weapon playerTetrisWeapon;
    
    private Image player1Image, player2Image;
    private final Map<String, Weapon> playerWeapons;
    
    private Image pointItemImage;
    private Image mirrorItemImage;
    private Image lifeItemImage;
    private Image reflectingItemImage;
    private Image deflectorItemImage;
    private Image deflectorImage;
    
    private Game easterEggGame;
    private Game empytGame;
    
    
    public static void initLibrary() {
        GameLibrary.instance = new GameLibrary();
        
        
        
        
        try {
            instance.loadImages();
        } catch (IOException ex) {
            System.exit(-1);
        }
        
        
        try {
            instance.loadSounds();
        } catch (IOException ex) {
            System.exit(-1);
        }
        
        instance.createGames();
        instance.createTetrisGame();
    }
    
    
    private void loadSounds() throws IOException {
        this.deathSound = getClass().getResource(
                "/spaceinvader/music/lifeLost.wav");
        this.gameOverSound = getClass().getResource(
                "/spaceinvader/music/gameOver.wav");
        this.hitSound = getClass().getResource(
                "/spaceinvader/music/enemyHit.wav");
        this.killSound = getClass().getResource(
                "/spaceinvader/music/enemyKill.wav");
        this.itemSound = getClass().getResource(
                "/spaceinvader/music/item.wav");
        this.winSound = getClass().getResource(
                "/spaceinvader/music/winSound.wav");
        this.newWaveSound = getClass().getResource(
                "/spaceinvader/music/newWave.wav");
        this.gameMusic = getClass().getResource(
                "/spaceinvader/music/backgroundMusic.wav");
        this.tetrisMusic = getClass().getResource(
                "/spaceinvader/music/Tetris.wav");
    }
    
    private void loadImages() throws IOException {
        //enemies-------------------------------------------
        
        this.enemyImages.put("Enemy1", ImageIO.read(
                GameLibrary.class.getResourceAsStream("/spaceinvader/images/enemy1.png")));
        this.enemyImages.put("Enemy2", ImageIO.read(
                GameLibrary.class.getResourceAsStream("/spaceinvader/images/enemy2.png")));
        this.enemyImages.put("Enemy3", ImageIO.read(
                GameLibrary.class.getResourceAsStream("/spaceinvader/images/enemy3.png")));
        this.enemyImages.put("Enemy4", ImageIO.read(
                GameLibrary.class.getResourceAsStream("/spaceinvader/images/enemy4.png")));
        
        
        Image tetris1Image = ImageIO.read(
                GameLibrary.class.getResourceAsStream("/spaceinvader/images/tetris1.png"));
        Image tetris2Image = ImageIO.read(
                GameLibrary.class.getResourceAsStream("/spaceinvader/images/tetris2.png"));
        Image tetris3Image = ImageIO.read(
                GameLibrary.class.getResourceAsStream("/spaceinvader/images/tetris3.png"));
        Image tetris4Image = ImageIO.read(
                GameLibrary.class.getResourceAsStream("/spaceinvader/images/tetris4.png"));
        Image tetris5Image = ImageIO.read(
                GameLibrary.class.getResourceAsStream("/spaceinvader/images/tetris5.png"));
        
        
        Image enemyProjectil = ImageIO.read(
                GameLibrary.class.getResourceAsStream("/spaceinvader/images/enemyProjectil.png"));
        Image enemyBomb = ImageIO.read(
                GameLibrary.class.getResourceAsStream("/spaceinvader/images/enemyBomb.png"));
        Image enemyTetrisProjectil = ImageIO.read(
                GameLibrary.class.getResourceAsStream("/spaceinvader/images/tetris-enemyProjectil.png"));
        
        
        while((tetris1Image.getWidth(null) < 0) ||
                (tetris2Image.getWidth(null) < 0) ||
                (tetris3Image.getWidth(null) < 0) ||
                (tetris4Image.getWidth(null) < 0) ||
                (tetris5Image.getWidth(null) < 0) ||
                (enemyProjectil.getWidth(null) < 0) ||
                (enemyBomb.getWidth(null) < 0) ||
                (enemyTetrisProjectil.getWidth(null) < 0)) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException ex) {}
        }
        
        this.enemyWeapons.put("Pistole", new Weapon(
                new Projectil(enemyProjectil, enemyProjectil.getWidth(null), enemyProjectil.getHeight(null),
                        10, 0, 5, true), 100, "Pistole"));
        this.enemyWeapons.put("Bombe", new Weapon(
                new Projectil(enemyBomb, enemyBomb.getWidth(null), enemyBomb.getHeight(null),
                        10000, 10000, 1, true), 1000, "Bombe"));
        this.enemyWeapons.put("Automatikwaffe", new Weapon(
                new Projectil(enemyProjectil, enemyProjectil.getWidth(null), enemyProjectil.getHeight(null),
                        1, 0, 20, true), 4, "Automatik"));
        Weapon enemyTetrisWeapon = new Weapon(
                new Projectil(enemyTetrisProjectil, enemyTetrisProjectil.getWidth(null), enemyTetrisProjectil.getHeight(null),
                        10, 0, 5, true), 100, "Pistole");
        
        
        this.tetrisEnemies.put("Tetris1", new Enemy(tetris1Image, 
                tetris1Image.getWidth(null), 
                tetris1Image.getHeight(null),
                10, 100, 5, enemyTetrisWeapon));
        this.tetrisEnemies.put("Tetris2", new Enemy(tetris2Image, 
                tetris2Image.getWidth(null), 
                tetris2Image.getHeight(null),
                10, 150, 10, enemyTetrisWeapon));
        this.tetrisEnemies.put("Tetris3", new Enemy(tetris3Image, 
                tetris3Image.getWidth(null), 
                tetris3Image.getHeight(null),
                10, 200, 15, enemyTetrisWeapon));
        this.tetrisEnemies.put("Tetris4", new Enemy(tetris4Image, 
                tetris4Image.getWidth(null), 
                tetris4Image.getHeight(null),
                10, 250, 20, enemyTetrisWeapon));
        this.tetrisEnemies.put("Tetris5", new Enemy(tetris5Image, 
                tetris5Image.getWidth(null), 
                tetris5Image.getHeight(null),
                10, 500, 30, enemyTetrisWeapon));
        
        //Player----------------------------------------------
        this.player1Image = ImageIO.read(
                GameLibrary.class.getResourceAsStream("/spaceinvader/images/player1.png"));
        this.player2Image = ImageIO.read(
                GameLibrary.class.getResourceAsStream("/spaceinvader/images/player2.png"));
        
        this.player1TetrisImage = ImageIO.read(
                GameLibrary.class.getResourceAsStream("/spaceinvader/images/tetris-player1.png"));
        this.player2TetrisImage = ImageIO.read(
                GameLibrary.class.getResourceAsStream("/spaceinvader/images/tetris-player2.png"));
        
        Image playerProjectil = ImageIO.read(
                GameLibrary.class.getResourceAsStream("/spaceinvader/images/playerProjectil.png"));
        Image playerBomb = ImageIO.read(
                GameLibrary.class.getResourceAsStream("/spaceinvader/images/playerBomb.png"));
        Image playerTetrisProjectil = ImageIO.read(
                GameLibrary.class.getResourceAsStream("/spaceinvader/images/tetris-playerProjectil.png"));
        
        while((playerProjectil.getWidth(null) < 0) ||
                (playerBomb.getWidth(null) < 0) ||
                (playerTetrisProjectil.getWidth(null) < 0)) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException ex) {}
        }
        
        
        this.playerWeapons.put("Pistole", new Weapon(
                new Projectil(playerProjectil, playerProjectil.getWidth(null), playerProjectil.getHeight(null),
                        10, 0, 10, true), 30, "Pistole"));
        this.playerWeapons.put("Bombe", new Weapon(
                new Projectil(playerBomb, playerBomb.getWidth(null), playerBomb.getHeight(null),
                        10000, 100, 1, true), 500, "Bombe"));
        this.playerWeapons.put("Automatikwaffe", new Weapon(
                new Projectil(playerProjectil, playerProjectil.getWidth(null), playerProjectil.getHeight(null),
                        1, 0, 20, true), 4, "Automatik"));
        this.playerTetrisWeapon = new Weapon(
                new Projectil(playerTetrisProjectil, playerTetrisProjectil.getWidth(null), playerTetrisProjectil.getHeight(null),
                        10, 0, 10, true), 30, "Tetris");
        
        
        //Items-----------
        this.pointItemImage = ImageIO.read(
                GameLibrary.class.getResourceAsStream("/spaceinvader/images/pointItem.png"));
        
        this.mirrorItemImage = ImageIO.read(
                GameLibrary.class.getResourceAsStream("/spaceinvader/images/mirrorItem.png"));
        
        this.lifeItemImage = ImageIO.read(
                GameLibrary.class.getResourceAsStream("/spaceinvader/images/lifeItem.png"));
        
        this.reflectingItemImage = ImageIO.read(
                GameLibrary.class.getResourceAsStream("/spaceinvader/images/reflectingItem.png"));
        
        this.deflectorItemImage = ImageIO.read(
                GameLibrary.class.getResourceAsStream("/spaceinvader/images/deflectorItem.png"));
        
        this.deflectorImage = ImageIO.read(
                GameLibrary.class.getResourceAsStream("/spaceinvader/images/deflector.png"));
        
        
        
        //wait until all images are loaded
        while((this.enemyImages.get("Enemy1").getWidth(null) < 0) ||
                (this.enemyImages.get("Enemy2").getWidth(null) < 0) ||
                (this.enemyImages.get("Enemy3").getWidth(null) < 0) ||
                (this.enemyImages.get("Enemy4").getWidth(null) < 0) ||
                (this.player1Image.getWidth(null) < 0) ||
                (this.player2Image.getWidth(null) < 0) ||
                (this.player1TetrisImage.getWidth(null) < 0) ||
                (this.player2TetrisImage.getWidth(null) < 0) ||
                (this.pointItemImage.getWidth(null) < 0) ||
                (this.lifeItemImage.getWidth(null) < 0) ||
                (this.mirrorItemImage.getWidth(null) < 0) ||
                (this.reflectingItemImage.getWidth(null) < 0) ||
                (this.deflectorItemImage.getWidth(null) < 0) ||
                (this.deflectorImage.getWidth(null) < 0)) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException ex) {}
        }
    }
    
    private void createTetrisGame() {
        this.easterEggGame = new Game(tetrisMusic, 
                deathSound, 
                winSound, 
                gameOverSound, 
                killSound, 
                hitSound, 
                newWaveSound, 
                itemSound);
        
        Level level = new Level();
        
        List<Weapon> playerWeapons = new LinkedList<>();
        playerWeapons.add(this.playerTetrisWeapon);
        
        Player player1 = new Player(this.player1TetrisImage,
                this.player1TetrisImage.getWidth(null), 
                this.player1TetrisImage.getHeight(null),
                1, 
                playerWeapons);
        Player player2 = new Player(this.player2TetrisImage,
                this.player2TetrisImage.getWidth(null), 
                this.player2TetrisImage.getHeight(null),
                1, 
                playerWeapons);
        
       
        
        level.setPlayerPrototypes(player1, player2);
        level.disablePointsToWin();
        level.setWinOnEnemiesDead(true);
     
        
        
        List<Enemy> row1 = new LinkedList<>();
        
        for(int i = 0; i < 2; i++) {
            row1.add(this.tetrisEnemies.get("Tetris1"));
        }
        for(int i = 0; i < 6; i++) {
            row1.add(this.tetrisEnemies.get("Tetris2"));
        }
        for(int i = 0; i < 2; i++) {
            row1.add(this.tetrisEnemies.get("Tetris1"));
        }
        
        List<Enemy> row2 = new LinkedList<>();
        
        for(int i = 0; i < 5; i++) {
            row2.add(this.tetrisEnemies.get("Tetris3"));
        }
        for(int i = 0; i < 5; i++) {
            row2.add(this.tetrisEnemies.get("Tetris4"));
        }
        
        List<Enemy> row3 = new LinkedList<>();
        
        for(int i = 0; i < 2; i++) {
            row3.add(this.tetrisEnemies.get("Tetris5"));
        }
        for(int i = 0; i < 6; i++) {
            row3.add(this.tetrisEnemies.get("Tetris2"));
        }
        for(int i = 0; i < 2; i++) {
            row3.add(this.tetrisEnemies.get("Tetris5"));
        }
        
        
        
        EnemyWave wave = new EnemyWave();
        wave.setSpeed(5);
        wave.addRow(row1);
        wave.addRow(row2);
        wave.addRow(row1);
        
        level.addEnemyWave(wave);
        
        wave = new EnemyWave();
        wave.setSpeed(5);
        wave.addRow(row3);
        wave.addRow(row3);
        wave.addRow(row2);
        
        level.addEnemyWave(wave);
        
        
        this.easterEggGame.addLevel(level);
    }
    
    private void createGames() {
        this.empytGame = new Game(gameMusic, 
                deathSound, 
                winSound, 
                gameOverSound, 
                killSound, 
                hitSound, 
                newWaveSound, 
                itemSound);
        
        Game game1 = new Game(gameMusic, 
                deathSound, 
                winSound, 
                gameOverSound, 
                killSound, 
                hitSound, 
                newWaveSound, 
                itemSound);
        
        Level level = new Level();
        level.addMultiKillAchievment(new MultiKillAchievment(2, 100, 50));
        level.addMultiKillAchievment(new MultiKillAchievment(3, 200, 100));
        
        String[] playerWeapons = {"Pistole", "Automatikwaffe", "Bombe"};
        
        level.setPlayerPrototypes(this.createPlayerPrototype(10, playerWeapons, 0), 
                this.createPlayerPrototype(10, playerWeapons, 1));
        level.disablePointsToWin();
        level.setWinOnEnemiesDead(true);
        
        level.addItem(this.createPointItemPrototype(10, 500));
        level.addItem(this.createMirrorItemPrototype(10, 1500));
        level.addItem(this.createLifeItemPrototype(10));
        level.addItem(this.createDeflectorItemPrototype(10, 1000, 10));
        level.addItem(this.createReflectingItemPrototype(10, 500));
        
        
        List<Enemy> row1 = new LinkedList<>();
        
        for(int i = 0; i < 10; i++) {
            row1.add(this.createEnemyPrototype("Enemy1", 10, 100, 10, "Pistole"));
        }
        
        List<Enemy> row2 = new LinkedList<>();
        
        for(int i = 0; i < 10; i++) {
            row2.add(this.createEnemyPrototype("Enemy2", 20, 250, 10, "Pistole"));
        }
        
        List<Enemy> row3 = new LinkedList<>();
        
        for(int i = 0; i < 10; i++) {
            row3.add(this.createEnemyPrototype("Enemy3", 10, 150, 20, "Pistole"));
        }
        
        
        
        EnemyWave wave = new EnemyWave();
        wave.setSpeed(5);
        wave.addRow(row1);
        wave.addRow(row1);
        wave.addRow(row1);
        
        level.addEnemyWave(wave);
        
        wave = new EnemyWave();
        wave.setSpeed(5);
        wave.addRow(row1);
        wave.addRow(row2);
        wave.addRow(row2);
        
        level.addEnemyWave(wave);
        
        wave = new EnemyWave();
        wave.setSpeed(5);
        wave.addRow(row2);
        wave.addRow(row3);
        wave.addRow(row3);
        
        level.addEnemyWave(wave);
        
        
        game1.addLevel(level);
        
        this.defaultGames.put("Game1", game1);
        
        
        
        
        Game game2 = new Game(gameMusic, 
                deathSound, 
                winSound, 
                gameOverSound, 
                killSound, 
                hitSound, 
                newWaveSound, 
                itemSound);
        
        level = new Level();
        level.addMultiKillAchievment(new MultiKillAchievment(2, 100, 100));
        level.addMultiKillAchievment(new MultiKillAchievment(3, 200, 300));
        
        String[] playerWeapons2 = {"Pistole", "Automatikwaffe"};
        
        level.setPlayerPrototypes(this.createPlayerPrototype(10, playerWeapons2, 0), 
                this.createPlayerPrototype(10, playerWeapons2, 1));
        level.disablePointsToWin();
        level.setWinOnEnemiesDead(true);
        
        level.addItem(this.createPointItemPrototype(5, 5000));
        level.addItem(this.createMirrorItemPrototype(10, 1000));
        
        
        
        row1 = new LinkedList<>();
        
        for(int i = 0; i < 10; i++) {
            row1.add(this.createEnemyPrototype("Enemy1", 10, 300, 30, "Pistole"));
        }
        
        row2 = new LinkedList<>();
        
        for(int i = 0; i < 3; i++) {
            row2.add(this.createEnemyPrototype("Enemy1", 10, 300, 30, "Pistole"));
        }
        
        for(int i = 0; i < 4; i++) {
            row2.add(this.createEnemyPrototype("Enemy2", 50, 100, 50, "Automatikwaffe"));
        }
        
        for(int i = 0; i < 3; i++) {
            row2.add(this.createEnemyPrototype("Enemy1", 10, 300, 30, "Pistole"));
        }
        
        row3 = new LinkedList<>();
        
        for(int i = 0; i < 5; i++) {
            row3.add(this.createEnemyPrototype("Enemy1", 10, 300, 30, "Pistole"));
            row3.add(this.createEnemyPrototype("Enemy3", 30, 5000, 0, "Pistole"));
        }
        
        
        
        wave = new EnemyWave();
        wave.setSpeed(5);
        wave.addRow(row1);
        wave.addRow(row1);
        wave.addRow(row1);
        
        level.addEnemyWave(wave);
        
        wave = new EnemyWave();
        wave.setSpeed(5);
        wave.addRow(row3);
        wave.addRow(row1);
        wave.addRow(row3);
        
        level.addEnemyWave(wave);
        
        wave = new EnemyWave();
        wave.setSpeed(5);
        wave.addRow(row2);
        wave.addRow(row3);
        wave.addRow(row2);
        
        level.addEnemyWave(wave);
        
        
        game2.addLevel(level);
        
        this.defaultGames.put("Game2", game2);
    }
    
    public static GameLibrary getInstance() {
        if(GameLibrary.instance == null) {
            GameLibrary.initLibrary();
        }
        return GameLibrary.instance;
    }
    
    
    
    public Game getDefaultGame(String name) {
        return this.defaultGames.get(name);
    }

    public Game getEastereggGame() {
        return this.easterEggGame;
    }
    
    public Map<String, Game> getDefaultGames() {
        return defaultGames;
    }

    public Map<String, Image> getEnemyImages() {
        return enemyImages;
    }

    public Map<String, Weapon> getEnemyWeapons() {
        return enemyWeapons;
    }

    public Map<String, Weapon> getPlayerWeapons() {
        return playerWeapons;
    }
    
    
    
    
    
    public Enemy createEnemyPrototype(String enemyType,  
            int lifePoints, 
            int winPoints,
            int fireRate, 
            String weaponType) {
        return new Enemy(this.enemyImages.get(enemyType), 
                this.enemyImages.get(enemyType).getWidth(null), 
                this.enemyImages.get(enemyType).getHeight(null), 
                lifePoints, winPoints, fireRate, 
                this.enemyWeapons.get(weaponType));
        
    }
    
    public Player createPlayerPrototype(int lifePoints, String[] weaponTypes, int playerNumber) {
        List<Weapon> weapons = new LinkedList<>();
        for(String weaponType : weaponTypes) {
            weapons.add(this.playerWeapons.get(weaponType));
        }
        Image playerImage;
        if(playerNumber % 2 == 0) {
            playerImage = this.player1Image;
        }
        else {
            playerImage = this.player2Image;
        }
        
        return new Player(playerImage, 
                playerImage.getWidth(null),
                playerImage.getHeight(null),
                lifePoints, weapons);
    }
    
    public LevelItem createPointItemPrototype(int properbility, int points) {
        PointItem item = new PointItem(this.pointItemImage, 
                this.pointItemImage.getWidth(null), 
                this.pointItemImage.getHeight(null), 
                1, points);
        
        return new LevelItem(item, properbility);
    }
    
    public LevelItem createMirrorItemPrototype(int properbility, int time) {
        MirrorItem item = new MirrorItem(this.mirrorItemImage, 
                this.mirrorItemImage.getWidth(null), 
                this.mirrorItemImage.getHeight(null), 
                time);
        
        return new LevelItem(item, properbility);
    }
    
    public LevelItem createLifeItemPrototype(int properbility) {
        LifeItem item = new LifeItem(this.lifeItemImage, 
                this.lifeItemImage.getWidth(null), 
                this.lifeItemImage.getHeight(null));
        
        return new LevelItem(item, properbility);
    }
    
    public LevelItem createReflectingItemPrototype(int properbility, int time) {
        ReflectingWorldItem item = new ReflectingWorldItem(this.reflectingItemImage, 
                this.reflectingItemImage.getWidth(null), 
                this.reflectingItemImage.getHeight(null),
                1,
                time);
        
        return new LevelItem(item, properbility);
    }
    
    public LevelItem createDeflectorItemPrototype(int properbility, int time, int distance) {
        Deflector deflector = new Deflector(this.deflectorImage, 
                this.deflectorImage.getWidth(null),
                this.deflectorImage.getHeight(null),
                distance, 
                time);
        
        DeflectorItem item = new DeflectorItem(this.deflectorItemImage, 
                this.deflectorItemImage.getWidth(null), 
                this.deflectorItemImage.getHeight(null),
                1,
                deflector);
        
        return new LevelItem(item, properbility);
    }
    
    
    
    public Image getLifeItemImage() {
        try {
            return ImageIO.read(
                GameLibrary.class.getResourceAsStream("/spaceinvader/images/lifeItem.png"));
        }
        catch (IOException e) {
            return null;
        }
    }

    public Game createEmpytGame() {
        this.empytGame.clearLevels();
        return empytGame;
    }
    
    


    
    
   
}
