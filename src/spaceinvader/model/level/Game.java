package spaceinvader.model.level;

import java.util.ArrayList;
import java.util.List;
import java.net.URL;

/**
 * A class representing a game with its sounds and levels.
 * @author Philip Mueller
 * @version 1.0
 */
public class Game {

    /**
     * Constructor.
     * @param gameMusic The background music of the game.
     * @param deathSound The death sound.
     * @param winSound The win sound.
     * @param gameOverSound The game over sound.
     * @param killSound The kill sound.
     * @param hitSound The hit sound.
     * @param newWaveSound The sound for new waves.
     * @param itemSound The item hit sound.
     */
    public Game(URL gameMusic, 
            URL deathSound, 
            URL winSound, 
            URL gameOverSound, 
            URL killSound, 
            URL hitSound, 
            URL newWaveSound, 
            URL itemSound) {
        this.gameMusic = gameMusic;
        this.deathSound = deathSound;
        this.winSound = winSound;
        this.gameOverSound = gameOverSound;
        this.killSound = killSound;
        this.hitSound = hitSound;
        this.newWaveSound = newWaveSound;
        this.itemSound = itemSound;
        
        
        this.levels = new ArrayList<>();
    }


    /**
     * The URLs to the game sounds.
     */
    private URL gameMusic, deathSound, winSound, gameOverSound, killSound, hitSound, 
            newWaveSound, itemSound;
    
    /**
     * The levels of the game.
     */
    private List<Level> levels;

    /**
     * Returns the levels of the game.
     * @return The levels of the game.
     */
    public List<Level> getLevels() {
        return levels;
    }
    
    /**
     * Returns the level index of the given level.
     * @param level The level to get the index of.
     * @return The index of the given level, -1 if the level does not exist in the game.
     */
    public int getLevelIndex(Level level) {
        if(this.levels.contains(level)) {
            return this.levels.indexOf(level);
        }
        else {
            return -1;
        }
    }
    
    /**
     * Adds a level to the game (after the previous levels).
     * @param level The level to add.
     */
    public void addLevel(Level level) {
        if(level == null) {
            throw new IllegalArgumentException("level is null");
        }
        this.levels.add(level);
    }

    /**
     * Returns URL of the death sound.
     * @return The URL of the death sound.
     */
    public URL getDeathSound() {
        return deathSound;
    }

    /**
     * Returns URL of background music.
     * @return The URL of the background music.
     */
    public URL getGameMusic() {
        return gameMusic;
    }

    /**
     * Returns URL of the gamve over sound.
     * @return The URL of the game over sound.
     */
    public URL getGameOverSound() {
        return gameOverSound;
    }

    /**
     * Returns URL of the hit sound.
     * @return The URL of the hit sound.
     */
    public URL getHitSound() {
        return hitSound;
    }

    /**
     * Returns URL of the item hit sound.
     * @return The URL of the item hit sound.
     */
    public URL getItemSound() {
        return itemSound;
    }

    /**
     * Returns URL of the kill sound.
     * @return The URL of the kill sound.
     */
    public URL getKillSound() {
        return killSound;
    }

    /**
     * Returns URL of the new wave sound.
     * @return The URL of the new wave sound.
     */
    public URL getNewWaveSound() {
        return newWaveSound;
    }

    /**
     * Returns URL of the win sound.
     * @return The URL of the win sound.
     */
    public URL getWinSound() {
        return winSound;
    }
    
    /**
     * Removes all the levels from the game.
     */
    public void clearLevels() {
        this.levels.clear();
    }   
}