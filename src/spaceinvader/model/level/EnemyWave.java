package spaceinvader.model.level;

import java.util.LinkedList;
import java.util.List;
import spaceinvader.model.Enemy;

/**
 * Class representing an enemy wave.
 * @author Fabian Haag
 * @author Martin Schmid
 * @version 1.0
 */
public class EnemyWave {
    /**
     * The speed of the enemy wave.
     */
    private int speed;
    
    /**
     * The enemy rows of the wave.
     */
    private List<List<Enemy>> enemyRows;

    /**
     * Constructor.
     */
    public EnemyWave() {
        this.speed = 0;
        this.enemyRows = new LinkedList<>();
    }

    /**
     * Returns the speed of the enemy wave.
     * @return The speed of the enemy wave.
     */
    public int getSpeed() {
        return speed;
    }

    /**
     * Sets the speed of the enemy wave.
     * @param speed The speed of the enemy wave.
     */
    public void setSpeed(int speed) {
        if(speed < 0) {
            speed = 0;
        }
        else if(speed > 100) {
            speed = 100;
        }
        this.speed = speed;
    }
    
    /**
     * Adds an enemy row to the wave.
     * @param rowPrototype The row to add.
     */
    public void addRow(List<Enemy> rowPrototype) {
        if(rowPrototype == null) {
            rowPrototype = new LinkedList<>();
        }
        this.enemyRows.add(rowPrototype);
    }

    /**
     * Returns the enemy rows of the wave.
     * @return The enemy rows of the wave.
     */
    public List<List<Enemy>> getEnemyRows() {
        return enemyRows;
    }
}