package spaceinvader.model.level;

import java.util.LinkedList;
import java.util.List;
import spaceinvader.model.Player;

/**
 * Class representing a level.
 * @author Martin Schmid
 * @author Fabian Haag
 * @version 1.0
 */
public class Level {
    /**
     * State wther the level is won when all enemies are dead.
     */
    private boolean winOnEnemiesDead;
    
    /**
     * The points needed to win the game.
     */
    private int pointsToWin;
    
    /**
     * The prototypes of the players.
     */
    private Player playerPrototype1, playerPrototype2;
    
    /**
     * The waves of the level.
     */
    private List<EnemyWave> enemyWaves;
    
    /**
     * The active items of the level.
     */
    private List<LevelItem> items;
    
    /**
     * The active multi kill achievments of the level.
     */
    private List<MultiKillAchievment> achievments;

    /**
     * Constructor.
     */
    public Level() {
        this.winOnEnemiesDead = false;
        this.pointsToWin = -1;
        
        this.playerPrototype1 = this.playerPrototype2 = null;
        this.enemyWaves = new LinkedList<>();
        this.items = new LinkedList<>();
        this.achievments = new LinkedList<>();
    }

    /**
     * Returns the player prototype for the given player index.
     * @param playerNumber The index of the player to get the prototype for.
     * @return The player prototype.
     */
    public Player getPlayerPrototype(int playerNumber) {
        Player playerPrototype;
        if(playerNumber % 2 == 0) {
            playerPrototype = this.playerPrototype1;
        }
        else {
            playerPrototype = this.playerPrototype2;
        }
        return playerPrototype;
    }

    /**
     * Sets the player prototype for player 1 and 2.
     * @param playerPrototype1 The prototype for player 1 (index 0).
     * @param playerPrototype2 The prototype for player 2 (index 1).
     */
    public void setPlayerPrototypes(Player playerPrototype1, Player playerPrototype2) {
        this.playerPrototype1 = playerPrototype1;
        this.playerPrototype2 = playerPrototype2;
    }
    
    /**
     * Returns Returns the enemy waves of the level.
     * @return The enemy waves of the level.
     */
    public List<EnemyWave> getEnemyWaves() {
        return enemyWaves;
    }

    /**
     * Adds an enemy wave.
     * @param wave The wave to add.
     */
    public void addEnemyWave(EnemyWave wave) {
        this.enemyWaves.add(wave);
    }

    /**
     * Returns the items of the level.
     * @return The items of the level.
     */
    public List<LevelItem> getItems() {
        return items;
    }
    
    /**
     * Adds an item to the level.
     * @param item The item to add.
     */
    public void addItem(LevelItem item) {
        this.items.add(item);
    }
    
    /**
     * Returns the points needed to win.
     * @return The points needed to win or -1 if the level can not be won by points.
     */
    public int getPointsToWin() {
        return pointsToWin;
    }

    /**
     * Returns wether the level is won when enough points are reached.
     * @return True if the game is won, false if not.
     */
    public boolean isPointsToWinEnabled() {
        return pointsToWin > 0;
    }
    
    /**
     * Enable that the level is won when enough points are achieved.
     * @param pointsToWin The points needed to win.
     */
    public void enablePointsToWin(int pointsToWin) {
        if(pointsToWin <= 0) {
            pointsToWin = 1;
        }
        this.pointsToWin = pointsToWin;
    }
    
    /**
     * Disables that the level is won when enough points are achieved.
     */
    public void disablePointsToWin() {
        this.pointsToWin = -1;
    }

     /**
     * Returns wether the level is won when all enemies are dead.
     * @return True if the game is won, false if not.
     */
    public boolean isWinOnEnemiesDeadEnabled() {
        return this.winOnEnemiesDead;
    }
    
     /**
     * Sets wether the level is won when all enemies are dead.
     * @param winOnEnemiesDead The new state.
     */
    public void setWinOnEnemiesDead(boolean winOnEnemiesDead) {
        this.winOnEnemiesDead = winOnEnemiesDead;
    }

    /**
     * Returns the multi kill achievments of the level.
     * @return The multi kill achievments of the level.
     */
    public List<MultiKillAchievment> getMultiKillAchievments() {
        return achievments;
    }
    
    /**
     * Adds a multi kill achievment to the level.
     * @param achievment The achievment to add.
     */
    public void addMultiKillAchievment(MultiKillAchievment achievment) {
        this.achievments.add(achievment);
    }   
}