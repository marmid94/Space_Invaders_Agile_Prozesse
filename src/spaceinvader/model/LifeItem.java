package spaceinvader.model;

import java.awt.Image;

/**
 * An item that heals a player life when destroyed.
 * @author Fabian Haag
 * @version 1.0
 */
public class LifeItem extends AbstractItem {
    /**
     * The controller where the life will be healed.
     */
    private GameController controller;
    
    /**
     * Constructor.
     * @param image The image to paint for the object.
     * @param height The height of the object.
     * @param width The width of the object.
     */
    public LifeItem(Image image, 
            int width, 
            int height) {
        super(image, width, height, 1);
        this.controller = null;
    }

    @Override
    public void setController(GameController controller) {
        if(controller == null) {
            throw new NullPointerException("The argument controller must not be null.");
        }
        
        this.controller = controller;
    }
    
    @Override
    protected void performItemAction() {
        if(this.controller == null) {
            throw new IllegalStateException("The controller has to be set by the method setController before calling the method performItemAction.");
        }
        
        //heal the life
        this.controller.addLifes(1, GameListener.LifeAddReason.ITEM);
    }
}