package spaceinvader.model;

import java.awt.Image;
import java.util.LinkedList;
import java.util.List;

/**
 * A player (shooting position) where the user can shoot from.
 * @author Philip Müller
 * @version 2.0
 */
public class Player extends AbstractTarget {
    /**
     * The weapon that the player shoots with.
     */
    private List<Weapon> weapons;
    
    /**
     * The index of the currently selected weapon.
     */
    private int currentWeaponIndex;
    
    /**
     * States how long the moving of the player will be locked.
     */
    private int remainingMoveLockTime;

    /**
     * Constructor, setting the full live points, the weapon, size and image of the target.
     * @param image The image to paint for the object.
     * @param height The height of the object.
     * @param width The width of the object.
     * @param fullLivePoints The maximum live points of the object.
     * @param weapons
     */
    public Player(Image image, 
            int width, 
            int height, 
            int fullLivePoints, 
            List<Weapon> weapons) {
        super(image, width, height, fullLivePoints);
        
        if(weapons == null) {
            throw new NullPointerException("The argument weapons must not be null.");
        }
        if(weapons.size() <= 0) {
            throw new IllegalArgumentException("The argument wepaons must minimum contain one weapon.");
        }
        this.weapons = weapons;
        this.currentWeaponIndex = 0;
        this.remainingMoveLockTime = 0;
    }
    
    /**
     * Returns the weapon that the player has currently selected to shoot with.
     * @return The weapon that the player has currently selected to shoot with.
     */
    public Weapon getCurrentWeapon() {
        return this.weapons.get(this.currentWeaponIndex);
    }

    /**
     * Returns the index of the currently selected weapon.
     * @return The index of the currently selected weapon.
     */
    public int getCurrentWeaponIndex() {
        return currentWeaponIndex;
    }
    
    /**
     * Returns all the weapons of the player.
     * @return All the weapons of the player.
     */
    public List<Weapon> getWeapons() {
        return this.weapons;
    }
    
    /**
     * Sets the weapons that the player can select to shoot with.
     * @param weapon The weapons that the player can select to shoot with.
     */
    void setWeapons(List<Weapon> weapons) {
        if(weapons == null) {
            throw new NullPointerException("The argument weapons must not be null.");
        }
        if(weapons.size() <= 0) {
            throw new IllegalArgumentException("The argument wepaons must minimum contain one weapon.");
        }
        
        for(Weapon weapon : weapons) {
            weapon.setWorld(this.world);
        }
        this.weapons = weapons;
    }
    
    /**
     * Shoots with currently selected weapon.
     * Creates projectils from the selected player weapon and fires them.
     */
    void shoot() {
        if(this.world.isInverted()) {
            //create the projectil on the bottom and horicontal middle of the player
            this.getCurrentWeapon().createProjectil(false, 
                    this.getX() + ((this.getWidth() - this.getCurrentWeapon().getPrototype().getWidth()) / 2),
                    this.getY() + this.getHeight());
        }
        else {
            //create the projectil on the top and horicontal middle of the player
            this.getCurrentWeapon().createProjectil(false, 
                    this.getX() + ((this.getWidth() - this.getCurrentWeapon().getPrototype().getWidth()) / 2),
                    this.getY() - this.getCurrentWeapon().getPrototype().getHeight());
        }
            
        
    }

    /**
     * Change the currently selected weapon.
     * Select the next weapon of the player weapon, or the first if the last weapon is currently selected.
     */
    void changeWeapon() {
        this.currentWeaponIndex++;
        
        /**
         * Select the first weapon if the last weapon was selected before.
         */
        if(this.currentWeaponIndex >= this.weapons.size()) {
            this.currentWeaponIndex = 0;
        }
        
        //notify
        this.world.getNotificationManger().notifyPlayerWeaponChanged(this, 
                this.getCurrentWeapon(), 
                this.currentWeaponIndex);
    }
    
    @Override
    void setWorld(World world, int x, int y) throws MapBorderException {
        super.setWorld(world, x, y); 
        
        for(Weapon weapon : this.weapons) {
            weapon.setWorld(world);
        }
    }

    @Override
    public AbstractWorldObject clone() throws CloneNotSupportedException {
        Player object = (Player) super.clone(); 
        List<Weapon> clonedWeapons = new LinkedList<>();
        for(Weapon weapon : this.weapons) {
            clonedWeapons.add((Weapon)weapon.clone());
        }
        object.weapons = clonedWeapons;
        return object;
    }

    @Override
    protected void destroy() {}  

    @Override
    void moveX(int x) throws MapBorderException {
        if(this.remainingMoveLockTime <= 0) {
            super.moveX(x); 
            
            //notify
            this.world.getNotificationManger().notifyPlayerMoved(this, x);
        }
    }
    
    /**
     * Locks the moving of the player for the given time.
     * @param lockTime The time that the moving will be locked.
     */
    void lockPlayerMove(int lockTime) {
        this.remainingMoveLockTime = lockTime;
    }
    
    /**
     * Updates (decrements) the move lock timer.
     */
    void updatePlayerMoveTimer() {
        if(this.remainingMoveLockTime > 0) {
            this.remainingMoveLockTime--;
        }
    }
}