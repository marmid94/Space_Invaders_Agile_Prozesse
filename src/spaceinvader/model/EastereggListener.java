package spaceinvader.model;

import spaceinvader.model.level.EnemyWave;
import spaceinvader.model.level.GameLibrary;
import spaceinvader.model.level.Level;

/**
 * A Listener for activating the easter egg game.
 * @author Philip Mueller
 * @version 1.0
 */
public class EastereggListener implements GameListener {
    /**
     * The game controller.
     */
    private final GameController controller;

    /**
     * Constructor.
     * @param controller The game controller.
     */
    public EastereggListener(GameController controller) {
        if(controller == null) {
            throw new NullPointerException("The argument controller must not be null.");
        }
        
        this.controller = controller;
    }
    
    @Override
    public void onWin() {}

    @Override
    public void onGameOver() {}

    @Override
    public void onLevelStarted(Level level, int levelIndex) {}

    @Override
    public void onLevelFinished(Level level, int levelIndex) {}

    @Override
    public void onRespawn(Level level, int waveIndex) {}

    @Override
    public void onStart() {}

    @Override
    public void onStop() {}

    @Override
    public void onDeath(LifeLostReason reason) {}

    @Override
    public void onLifesAdded(int numberOfLifes, LifeAddReason reason) {}

    @Override
    public void onPointsAchieved(int points, PointReason reason) {}

    @Override
    public void onHit(AbstractTarget target, Projectil projectil, int damage, boolean wasKilled, int x, int y) {
        
        //if an enemy was killed => check wether the easteregg shall start and then start it
        if(wasKilled && Enemy.class.isInstance(target) && this.checkEasterEgg()) {
            this.startEasterEgg();
        }
    
    }

    @Override
    public void onHeal(AbstractTarget target, int heal, boolean wasFullHeal) {}

    @Override
    public void onMultiKill(int kills, int points) {}

    @Override
    public void onWaveStarted(EnemyWave wave, int waveIndex) {}
    
    /**
     * Checks wether the enemy have a pattern where the easteregg shall start.
     * @return True if the easteregg shall start, false if not.
     */
    private boolean checkEasterEgg() {
        //check wether there are exactly 3 rows
        if(this.controller.getWorld().getTargetRows().size() == 3) {
            TargetRow row1 = this.controller.getWorld().getTargetRows().get(0);
            TargetRow row2 = this.controller.getWorld().getTargetRows().get(1);
            TargetRow row3 = this.controller.getWorld().getTargetRows().get(2);
            
            
            //check row sizes
            // - minimum 3 enemies per row
            // - all rows have the same size
            if(!(row1.getTargets().size() >= 3 &&
                    row1.getTargets().size() == row2.getTargets().size() &&
                    row2.getTargets().size() == row3.getTargets().size())) {
                
                return false;
            }
            
            
            int rowSize = row1.getTargets().size();
            
            //check left element
            // - the left element of all rows must have the same original index
            int leftIndex = row1.getOriginalIndexList().get(0);
            if(!( row2.getOriginalIndexList().get(0) == leftIndex &&
                    row3.getOriginalIndexList().get(0) == leftIndex)) {
                return false;
            }
            
            
            //check right element
            // - the right element of all rows must have the same original index
            int rightIndex = row1.getOriginalIndexList().get(rowSize - 1);
            if(!(  row2.getOriginalIndexList().get(rowSize - 1) == rightIndex &&
                    row3.getOriginalIndexList().get(rowSize - 1) == rightIndex)) {
                return false;
            }
            
           
            
            //check for exactly 2 gaps
            // - there have to be exactly two gaps in every row
            if(! (((rightIndex - leftIndex) - rowSize) == 1)) {
                return false;
            }
            
            
            
            
            //check left gap position
            // - the left gap has to be the second element in every row
            if(! (row1.getOriginalIndexList().get(1) == (leftIndex + 2) &&
                    row2.getOriginalIndexList().get(1) == (leftIndex + 2) &&
                    row3.getOriginalIndexList().get(1) == (leftIndex + 2))) {
                return false;
            }
            
            
            //check right gap position
            // - the right gap has to be the second last element in every row
            if(! (row1.getOriginalIndexList().get(rowSize - 2) == (rightIndex - 2) &&
                    row2.getOriginalIndexList().get(rowSize - 2) == (rightIndex - 2) &&
                    row3.getOriginalIndexList().get(rowSize - 2) == (rightIndex - 2))) {
                return false;
            }
                
            //all conditions are true => start easter egg
            return true;
 
        }
        //not all conditions are true => do not start the easteregg
        return false;
    }
    
    /**
     * Starts the easter egg.
     */
    private void startEasterEgg() {
        //pause the current game.
        this.controller.pause();
        
        //load the easteregg game with the current number of players
        this.controller.setGame(GameLibrary.getInstance().getEastereggGame(), 
                this.controller.getNumberOfPlayers());
        
        //start the easter egg game
        this.controller.start();
    }

    @Override
    public void onPlayerWeaponChanged(Player player, Weapon wepaon, int wepaonIndex) {}

    @Override
    public void onPlayerMoved(Player player, int xMove) {}

    @Override
    public void onTilt(Player player) {}
}