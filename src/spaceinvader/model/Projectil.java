package spaceinvader.model;

import java.awt.Image;

/**
 * A Projectil that was shot from a weapon and can hit target objects.
 * @author Philip Müller
 * @version 1.0
 */
public class Projectil extends AbstractWorldObject {
    /**
     * The damage that the projectil produces.
     */
    private int damage;
    
    /**
     * The radius around the hit point where the projectil is producing damage.
     */
    private int damageRadius;
    
    /**
     * The speed the projectil is moving with.
     */
    private int speed;
    
    /**
     * States wether the move direction is inverted.
     */
    private boolean isDirectionInverted;
    
    /**
     * Defines wether the projectil was shot from a player or an enemy.
     */
    private boolean isEnemyProjectil;
    
    /**
    * Constructor, setting the size and image of the object.
    * @param image The image to paint for the object.
    * @param height The height of the object.
    * @param width The width of the object.
    * @param damage The damage the projectil has.
    * @param damageRadius The radius around the hit point where the projectil produces damage.
    * @param speed The speed the projectil is moving with.
    * @param isEnemyProjectil Defines wether the projectil was shot from a player or an enemy.
    */
    public Projectil(Image image,
            int width, 
            int height,
            int damage,
            int damageRadius,
            int speed,
            boolean isEnemyProjectil) {
        super(image, width, height);
        
        if(damage < 0) {
            throw new IllegalArgumentException("The value of the argument damage must be >= 0.");
        }
        if(damageRadius < 0) {
            throw new IllegalArgumentException("The value of the argument damageRadius must be >= 0.");
        }
        if(speed <= 0) {
            throw new IllegalArgumentException("The value of the argument speed must be > 0.");
        }
        
        this.damage = damage;
        this.damageRadius = damageRadius;
        this.speed = speed;
        this.isEnemyProjectil = isEnemyProjectil;
        
        this.isDirectionInverted = false;
    }
    
    /**
     * Creates a new projectil from a projectil prototype.
     * @param projectil The projectil prototype.
     * @param isEnemyProjectil States wether the projectil shall be an enemy projectil.
     * @return The created projectil.
     */
    public static Projectil createFromPrototype(Projectil projectil, boolean isEnemyProjectil) {
        return new Projectil(projectil.image,
                projectil.getWidth(), 
                projectil.getHeight(), 
                projectil.getDamage(), 
                projectil.getDamageRadius(),
                projectil.getSpeed(), 
                isEnemyProjectil);
    }
    
    /**
     * Returns the damage that the projectil produces.
     * @return The damage that the projectil produces.
     */
    public int getDamage() {
        return this.damage;
    }
    
    /**
     * Returns the radius around the hit point where the projectil is producing damage.
     * @return The radius around the hit point where the projectil is producing damage.
     */
    public int getDamageRadius() {
        return this.damageRadius;
    }
    
    /**
     * Returns the speed the projectil is moving with.
     * @return The speed the projectil is moving with.
     */
    public int getSpeed() {
        return this.speed;
    }
    
    /**
     * Returns wether the projectil was shot from an enemy or a player.
     * @return True if the projectil was shot from an enemy, false if it was shot from a player.
     */
    public boolean isEnemyProjectil() {
        return this.isEnemyProjectil;
    }

    /**
     * Returns wether the move direction is inverted.
     * @return True if the direction is inverted, false if not.
     */
    public boolean isIsDirectionInverted() {
        return isDirectionInverted;
    }

    /**
     * Inverts the move direction of the projectil.
     */
    public void invertDirection() {
        this.isDirectionInverted = !this.isDirectionInverted;
    }
    
    /**
     * Returns wether the projectil is currently moving downwards.
     * @return True if the projectil is moving downwards, false if upwards.
     */
    public boolean isDirectionDown() {
        return this.isDirectionInverted != this.isEnemyProjectil;
    }
}