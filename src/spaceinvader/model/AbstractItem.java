package spaceinvader.model;

import java.awt.Image;

/**
 * An item that can be destroyed and performs an action when destroyed.
 * @author Philip Müller
 * @version 1.1
 */
public abstract class AbstractItem extends AbstractTarget {
    /**
     * Constructor.
     * @param image The image to paint for the object.
     * @param height The height of the object.
     * @param width The width of the object.
     * @param fullLivePoints The maximum live points of the object.
     */
    public AbstractItem(Image image, int width, int height, int fullLivePoints) {
        super(image, width, height, fullLivePoints);
    }

    @Override
    protected void destroy() {
        this.performItemAction();
    }

    /**
     * Abstract method that will be called internally when the item was destroyed.
     * It will be implemented in the derived classes to perform the item action.
     */
    abstract protected void performItemAction();
    
    /**
     * Sets the GameController of the item.
     * The GameController can be used in the performItemAction method, e.g. so add points.
     * The basic item does not need a controller, so the method is empty.
     * It can be overwritten if a controller is needed.
     * @param controller The GameController of the item.
     */
    public void setController(GameController controller) {}
}