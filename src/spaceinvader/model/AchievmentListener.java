package spaceinvader.model;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;
import spaceinvader.model.level.EnemyWave;
import spaceinvader.model.level.Level;
import spaceinvader.model.level.MultiKillAchievment;

/**
 * A game listener that regocgnizes and handles multi kill achievments.
 * @author Martin Schmid
 * @version 1.0
 */
public class AchievmentListener implements GameListener, ActionListener {
    /**
     * Number of kills that occurred in the current multi kill run.
     */
    private int kills;
    
    /**
     * The time that elapsed in the current multi kill run.
     */
    private int timeCounter;
    
    /**
     * A timer for measuring the elapsed time.
     */
    private Timer timer;

    /**
     * The game controller where points will be added on achievments.
     */
    private GameController controller;

    /**
     * Constructor.
     * @param controller The game controller where points will be added on achievments.
     */
    public AchievmentListener(GameController controller) {
        this.kills = 0;
        this.timeCounter = 0;
        this.controller = controller;
        
        //Init the timer (interval every 10ms)
        this.timer = new Timer(10, this);
        this.timer.setRepeats(true);
    }
    
    @Override
    public void onWin() {}

    @Override
    public void onGameOver() {}

    @Override
    public void onLevelStarted(Level level, int levelIndex) {
        //reset the multi kill run => no multikills between two levels possible
        this.kills = 0;
        this.timeCounter = 0;
    }

    @Override
    public void onLevelFinished(Level level, int levelIndex) {}

    @Override
    public void onRespawn(Level level, int waveIndex) {
        //reset the multi kill run => no multikills after death possible
        this.kills = 0;
        this.timeCounter = 0;
    }

    @Override
    public void onStart() {
        this.timer.start();
    }

    @Override
    public void onStop() {
        this.timer.stop();
    }

    @Override
    public void onDeath(LifeLostReason reason) {}

    @Override
    public void onPointsAchieved(int points, PointReason reason) {}

    @Override
    public void onHit(AbstractTarget target, Projectil projectil, int damage, boolean wasKilled, int x, int y) {
        if(wasKilled && Enemy.class.isInstance(target)) {
            //if an enemy was killed => check for multikill
            
            //states wether a multikill timer is still active
            //e.g. the 3-kill timer is still active after a double kill
            boolean isAnyCounterActive = false;
            
            //if there have been no kills in the current run => a new run begins
            if(this.kills == 0) {
                this.timeCounter = 0;
            }
            
            //one more kill happened in the current run
            this.kills++;
            
            //check all the multikill achievment: double kill, 3-kill, ...
            for(MultiKillAchievment achievment : this.controller.getCurrentLevel().getMultiKillAchievments()) {

                
                if(kills == achievment.getNumberOfKills() &&
                        this.timeCounter <= achievment.getTime()) {
                    //if there have been enough kills in the given time
                    // => the achievment is reached
                    
                    //add the achievment points
                    this.controller.addPoints(achievment.getPoints(), PointReason.ACHIEVMENT);
                    
                    //notify that a multi kill occurred
                    this.controller.getNotificationManager().notifyMultiKill(kills, achievment.getPoints());
                }
                else if(this.timeCounter <= achievment.getTime() && achievment.getNumberOfKills() > kills) {
                    //if there haven't been enough kills for this achievment, but there is still enough time to get those kills
                    // => don't stop the current multi kill run
                    isAnyCounterActive = true;
                }
            }
            
            //if there are no more active counters for the run
            // => start a new run
            if(!isAnyCounterActive) {
                this.kills = 0;
            }
        }
    
    }

    @Override
    public void onHeal(AbstractTarget target, int heal, boolean wasFullHeal) {}

    @Override
    public void onMultiKill(int kills, int points) {}

    @Override
    public void actionPerformed(ActionEvent e) {
        //increment the timer
        if(e.getSource() == this.timer) {
            this.timeCounter++;
        }
    }

    @Override
    public void onLifesAdded(int numberOfLifes, LifeAddReason reason) {}

    @Override
    public void onWaveStarted(EnemyWave wave, int waveIndex) {}

    @Override
    public void onPlayerWeaponChanged(Player player, Weapon wepaon, int wepaonIndex) {}

    @Override
    public void onPlayerMoved(Player player, int xMove) {}

    @Override
    public void onTilt(Player player) {}
    
}