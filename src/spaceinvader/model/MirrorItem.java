package spaceinvader.model;

import java.awt.Image;

/**
 * An item that inits the mirror when destroyed.
 * @author Fabian Haag
 * @version 1.0
 */
public class MirrorItem extends AbstractItem {
    /**
     * The controller where the mirror action is started.
     */
    private GameController controller;
    
    /**
     * States how long the mirroring will last.
     */
    private int mirrorTime;
    
    /**
     * Constructor.
     * @param image The image to paint for the object.
     * @param height The height of the object.
     * @param width The width of the object.
     * @param mirrorTime States how long the mirroring will last.
     */
    public MirrorItem(Image image, 
            int width, 
            int height, 
            int mirrorTime) {
        super(image, width, height, 1);
        
        if(mirrorTime < 0) {
            throw new IllegalArgumentException("The value of the argument mirrorTime must be >= 0.");
        }
        this.mirrorTime = mirrorTime;
    }

    @Override
    public void setController(GameController controller) {
        if(controller == null) {
            throw new NullPointerException("The argument controller must not be null.");
        }
        
        this.controller = controller;
    }
    
    @Override
    protected void performItemAction() {
        if(this.controller == null) {
            throw new IllegalStateException("The controller has to be set by the method setController before calling the method performItemAction.");
        }

        //init the mirror action
        this.controller.initMirror(this.mirrorTime);
    }
}