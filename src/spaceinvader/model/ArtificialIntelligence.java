package spaceinvader.model;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.Timer;

/**
 * An simple AI controlling a player.
 * @author Philip Mueller
 * @version 1.0
 */
public class ArtificialIntelligence implements ActionListener {
    private enum Direction { LEFT, RIGHT, STOP, UNKNOWN }
    
    /**
     * The timer for the AI.
     */
    private final Timer timer;
    
    /**
     * The game controller.
     */
    private final GameController controller;
    
    /**
     * The index of the player that is controlled by the AI.
     */
    private final int playerIndex;
    
    /**
     * A counter that counts how many steps have been elapsed since the last AI compution.
     */
    private int stepCounter;
    
    /**
     * The current direction that will be used until the next AI compution.
     */
    private Direction currentDirection;
    
    /**
     * The result of the last AI compution.
     */
    private MoveCheckResult currentResult;
    
    /**
     * A random generator used for all random values.
     */
    private final Random rand;
    
    /**
     * Defines how many recursive layers will be used in AI computions.
     */
    private static final int CHECK_DEPTH = 8;
    
    /**
     * Defines the move speed that is used when moving the player.
     */
    private static final int MOVE_X_SPEED = 10;
    
    /**
     * Defines how often the AI compution is executed (0 means every step, 1 every second,...).
     */
    private static final int STEPS_PER_CHECK = 5;

    /**
     * COnstructor.
     * @param controller The game controller.
     * @param playerIndex The index of the player that is controlled.
     */
    public ArtificialIntelligence(GameController controller, int playerIndex) {
        //init the timer with 20ms
        this.timer = new Timer(20, this);
        
        this.controller = controller;
        this.playerIndex = playerIndex;
        this.stepCounter = 0;
        this.currentDirection = Direction.STOP;
        rand = new Random();
    }
    
    
    /**
     * Struct that represents the result of a AI compution.
     */
    private class MoveCheckResult {
        /**
         * States wether the player would be killed.
         */
        public boolean isKilled;
        
        /**
         * The distance of the nearest projectil.
         */
        public int nearestProjectilDistance;
        
        /**
         * The speed of the nearest projectil.
         */
        public int nearestProjectilSpeed;

        /**
         * The previous result (one recursive layer deeper).
         */
        public MoveCheckResult nextResult;
        
        /**
         * The recommended direction.
         */
        public Direction nextDirection;
    }
    
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == this.timer) {
            //shoot and move if the player is not dead and the game is not paused.
            if(!this.controller.isPaused() && this.controller.isIsPlayerAlive()) {
                try {
                    this.shoot();
                    this.move();
                }
                catch(IndexOutOfBoundsException ex) {}
            }
        }
    }
    
    /**
     * Starts the AI.
     */
    public void start() {
        this.timer.start();
    }
    
    /**
     * Pauses the AI.
     */
    public void stop() {
        this.timer.stop();
    }
    
    /**
     * Shoots with the player.
     */
    private void shoot() {
        this.controller.shootWithPlayer(playerIndex);
    }
    
    /**
     * Moves the player in the calculated direction.
     */
    private void move() {
        //move player in the previous calculated direction.
        switch(this.currentDirection) {
            case LEFT:
                if(this.controller.getWorld().isInverted()) {
                    this.controller.movePlayer(MOVE_X_SPEED, playerIndex);
                }
                else {
                    this.controller.movePlayer(-MOVE_X_SPEED, playerIndex);
                }
                break;
            case RIGHT:
                if(this.controller.getWorld().isInverted()) {
                    this.controller.movePlayer(-MOVE_X_SPEED, playerIndex);
                }
                else {
                    this.controller.movePlayer(MOVE_X_SPEED, playerIndex);
                }
                break;
        }
        
        this.stepCounter--;
        
        if(this.stepCounter <= 0) {
            this.stepCounter = STEPS_PER_CHECK;
            
            //if a new AI compution shall be performed
            
            //randomly decide if a random value will be used (artificial errors) or a real compution is performed
            int val = rand.nextInt(100);
            if(val < 2) {
                this.currentDirection = Direction.LEFT;
            }
            else if(val < 4) {
                this.currentDirection = Direction.RIGHT;
            }
            else if(val < 10) {
                this.currentDirection = Direction.STOP;
            }
            else {
                
                //do the compution
                MoveCheckResult result = this.checkMove(
                        this.controller.getPlayerPosition(playerIndex),
                        STEPS_PER_CHECK,
                        0);
                if(result.nextDirection != Direction.UNKNOWN) {
                    //if the direction is known => use the direction
                    this.currentDirection = result.nextDirection;
                    this.currentResult = result;
                }
                else {
                    
                    //if the direction cannot be decided => use a random direction
                    this.currentDirection = Direction.STOP;
                    switch(this.rand.nextInt(7)) {
                        case 0:
                        case 1:
                        case 2:
                            this.currentDirection = Direction.STOP;
                            break;
                        case 3:
                            this.currentDirection = Direction.LEFT;
                            break;
                        case 4:
                        case 5:
                        case 6:
                            this.currentDirection = Direction.RIGHT;
                            break;
                    }    
                }
            }
        }
    }
    
    /**
     * Decide which of the given results is better.
     * @param left The result of moving left.
     * @param right The result of moving right.
     * @param stop The result of not moving.
     * @return The preferred direction.
     */
    private Direction getPreferredDirection(MoveCheckResult left, MoveCheckResult right, MoveCheckResult stop) {
        Direction tmpDirection = this.checkKilled(left, right, stop);
        
        //if only one direciton survies => use that direction
        if(tmpDirection != Direction.UNKNOWN) {
            return tmpDirection;
        }
        
        //if one direction has the lowest kill propability => use that direction
        tmpDirection = this.checkKillPropability(left, right, stop);
        if(tmpDirection != Direction.UNKNOWN) {
            return tmpDirection;
        }
        
        //check on which side are more enemies
        return this.checkEnemySide(this.controller.getPlayerPosition(playerIndex));
    }
    
    /**
     * Checks were the player is not killed.
     * @param left The result of moving left.
     * @param right The result of moving right.
     * @param stop The result of not moving.
     * @return The preferred direction or UNKNOWN when there is no preferred direction.
     */
    private Direction checkKilled(MoveCheckResult left, MoveCheckResult right, MoveCheckResult stop) {
        //loop as long as decisions can be made
        while(!(left == null && right == null && stop == null)) {
            if(left == null || left.isKilled) {
                //not left
                if((right == null || right.isKilled) && !(stop == null || stop.isKilled)) {
                    //not right as well, but stop is ok => stop
                    return Direction.STOP;
                }
                else if((stop == null || stop.isKilled) && !(right == null || right.isKilled)) {
                    //not stop as well, but right is ok => right
                 
                    return Direction.RIGHT;
                }
                else if(!(stop == null || stop.isKilled) && !(right == null || right.isKilled)) {
                    //stop and right ok => stop or right (prefer stop)
                    return Direction.STOP;
                }
                else {
                    //neither stop nor right ok => unknown
                    return Direction.UNKNOWN;
                }
            }
            else if(right == null || right.isKilled) {
                //not right but left is ok (previous if condition)
                
                if(stop == null || stop.isKilled) {
                    //not stop as well => left
                    return Direction.LEFT;
                }
                else {
                    //stop and left are ok => stop or left (prefer stop)
                    return Direction.STOP;
                }
            }
            else if(stop == null || stop.isKilled) {
                //not stop but right and left are ok => left or right (random)
                if(this.rand.nextBoolean()) {
                    return Direction.LEFT;
                }
                else {
                    return Direction.RIGHT;
                }
                //left, right and stop are ok 
                // -> go with next layer
            }
        
            if(left != null) {
                left = left.nextResult;
            }
            if(stop != null) {
                stop = stop.nextResult;
            }
            if(right != null) {
                right = right.nextResult;
            }
        }
        
        //no preferred direction could be found
        return Direction.UNKNOWN;
    }
    
    /**
     * Checks were the player kill propability is the lowest.
     * @param left The result of moving left.
     * @param right The result of moving right.
     * @param stop The result of not moving.
     * @return The preferred direction or UNKNOWN when there is no preferred direction.
     */
    private Direction checkKillPropability(MoveCheckResult left, MoveCheckResult right, MoveCheckResult stop) {
        //loop as long as decisions can be made
        while(!(left == null && right == null && stop == null)) {
            if(left == null) {
                //not left
                if(right == null && stop != null) {
                    return Direction.STOP;
                }
                else if(stop == null && right != null) {
                    return Direction.RIGHT;
                }
                else if(!(stop == null && right == null)) {
                    if(stop.nearestProjectilDistance < right.nearestProjectilDistance) {
                        return Direction.RIGHT;
                    }
                    else {
                        return Direction.STOP;
                    }
                }
                //else -> both null
                // => UNKNWON
            }
            else if(right == null) {
                //not right, left != null
                
                if(stop == null) {
                    return Direction.LEFT;
                }
                else if(stop.nearestProjectilDistance < left.nearestProjectilDistance) {
                    return Direction.LEFT;
                }
                else  {
                    return Direction.STOP;
                }  
            }
            else if(stop == null) {
                if(rand.nextBoolean()) {
                    return Direction.LEFT;
                }
                else {
                    return Direction.RIGHT;
                }
            }
            else {
                if(left.nearestProjectilDistance < right.nearestProjectilDistance) {
                    if(stop.nearestProjectilDistance < right.nearestProjectilDistance) {
                        return Direction.RIGHT;
                    }
                    else {
                        return Direction.STOP;
                    }
                }
                else if(left.nearestProjectilDistance > right.nearestProjectilDistance) {
                    if(stop.nearestProjectilDistance < left.nearestProjectilDistance) {
                        return Direction.LEFT;
                    }
                    else {
                        return Direction.STOP;
                    }
                }
                else {
                    //right == left
                    if(stop.nearestProjectilDistance > right.nearestProjectilDistance) {
                        return Direction.STOP;
                    }
                    else if(stop.nearestProjectilDistance < right.nearestProjectilDistance) {
                        if(this.rand.nextBoolean()) {
                            return Direction.LEFT;
                        }
                        else {
                            return Direction.RIGHT;
                        }
                    }
                    //else -> stop.nearestProjectilDistance == right.nearestProjectilDistance
                    // => UNKNOWN
                }
            }
            
            if(left != null) {
            left = left.nextResult;
            }
            if(stop != null) {
                stop = stop.nextResult;
            }
            if(right != null) {
                right = right.nextResult;
            }
        }
        
        //no preferred direction could be found
        return Direction.UNKNOWN;
    }
    
    /**
     * Checks on which side are the most enemies.
     * @param xPos The current player position.
     * @return The preferred side or UNKNOWN if there is no preferred side.
     */
    private Direction checkEnemySide(int xPos) {
        int playerWidth = this.controller.getWorld().getPlayers().get(playerIndex).getWidth();
        
        //counter for number of enemies on each side
        int leftEnemyCount, rightEnemyCount;
        leftEnemyCount = rightEnemyCount = 0;
        int leftEnemyPoints, rightEnemyPoints;
        leftEnemyPoints = rightEnemyPoints = 0;
        
        //go through all the enemies and check wether they are right or left of xPos
        for(Enemy enemy : this.controller.getWorld().getEnemies()) {
            //if left add to left counters
            if(enemy.getX() < xPos) {
                leftEnemyCount++;
                leftEnemyPoints += enemy.getWinPoints();
            }
            //if right add to right counters
            if(enemy.getX() + enemy.getWidth() > xPos + playerWidth) {
                rightEnemyCount++;
                rightEnemyPoints += enemy.getWinPoints();
            }
        }
        
        //if points to win is enabled => decide side depending on enemy points
        if(this.controller.getCurrentLevel().isPointsToWinEnabled()) {
            if(leftEnemyPoints == rightEnemyPoints) {
                return Direction.UNKNOWN;
            }
            if(leftEnemyPoints > rightEnemyPoints) {
                return Direction.LEFT;
            }
            else {
                return Direction.RIGHT;
            }
        }
        //if win on all enemies dead is enabled )> decide side depending on number of enemies
        else if(this.controller.getCurrentLevel().isWinOnEnemiesDeadEnabled()) {
            if(leftEnemyCount == rightEnemyCount) {
                return Direction.UNKNOWN;
            }
            if(leftEnemyCount > rightEnemyCount) {
                return Direction.LEFT;
            }
            else {
                return Direction.RIGHT;
            }
        }
    
        
        return Direction.UNKNOWN;
    }
    
    /**
     * Does AI compution for the given position.
     * @param newX The position to check.
     * @param steps The number of steps that the player is moved on every recursive layer.
     * @param depth The current recursive depth.
     * @return The AI compution result.
     */
    private MoveCheckResult getMoveResult(int newX, int steps, int depth) {
        int playerWidth = this.controller.getWorld().getPlayers().get(playerIndex).getWidth();
        int playerHeight = this.controller.getWorld().getPlayers().get(playerIndex).getHeight();
        
        //calc the position => the player cannot be outside of the map
        if(newX < 0) {
            newX = 0;
        }
        else if(newX + playerWidth > this.controller.getWorld().getWidth()) {
            newX = this.controller.getWorld().getWidth() - playerWidth;
        }
        
        MoveCheckResult result = new MoveCheckResult();
        
        
        //calc the nearest projecitl distance and speed
        result.nearestProjectilDistance = this.controller.getWorld().getHeight();
        result.nearestProjectilSpeed = 0;
        
        //check all projecitls
        for(Projectil projectil : this.controller.getWorld().getProjectils()) {
            //if:
            // - projectil is an enemy projectil
            // - ptojectil is in the area that is dangerous for the player
            if(((projectil.isDirectionDown() && !this.controller.getWorld().isInverted()) ||
                    (!projectil.isDirectionDown() && this.controller.getWorld().isInverted()))&&
                    (projectil.getX() + projectil.getWidth()) >= newX &&
                    projectil.getX() <= (newX + playerWidth)) {
              
                //calc the future distance of the projectil
                int currentDistance = this.getProjectilDistance(projectil, steps * depth);
                
                //if the projectil will still be in the world and is nearer than the nearest projectil
                if(currentDistance >= (-playerHeight) &&
                        currentDistance < result.nearestProjectilDistance) {
                    
                    //this projectil is the new nearest projectil
                    result.nearestProjectilDistance = currentDistance;
                    result.nearestProjectilSpeed = projectil.getSpeed();
                }
            }
        }
        
        //if the player would be killed (projecitl too near)
        if(result.nearestProjectilDistance <= result.nearestProjectilSpeed * steps) {
            result.isKilled = true;
        }
        else {
            result.isKilled = false;
        }
        
       
        return result;
    }
    
    /**
     * Recursivly simulates and check possible movings.
     * @param newX The base x for the calculation.
     * @param steps The number of move steps that are executet in each depth.
     * @param depth The current recursive depth.
     * @return The AI result of the compution.
     */
    private MoveCheckResult checkMove(int newX, int steps, int depth) {
        
        
        MoveCheckResult result = this.getMoveResult(newX, steps, depth);
        
        //max recursive depth reached => no more recursive calls
        if(depth > CHECK_DEPTH) {
            result.nextResult = null;
            result.nextDirection = Direction.UNKNOWN;
        }
        else {
            //simulate not moving
            MoveCheckResult stop = this.checkMove(newX, steps, depth + 1);
            
            int playerWidth = this.controller.getWorld().getPlayers().get(playerIndex).getWidth();
            
            //simulate moving left (if the player isn't already on the left border of the world
            MoveCheckResult left = null;
            if(newX > 0) {
                left = this.checkMove(newX - MOVE_X_SPEED * steps, steps, depth + 1);
            }
            
            //simulate moving right (if the player isn't already on the right border of the world
            MoveCheckResult right = null;
            if(newX + playerWidth < this.controller.getWorld().getWidth()) {
                
                right = this.checkMove(newX + MOVE_X_SPEED * steps, steps, depth + 1);
            }
            
            //get the preferred direction and save the direction as the next result
            switch(this.getPreferredDirection(left, right, stop)) {
                case LEFT:
                    result.nextResult = left;
                    result.nextDirection = Direction.LEFT;
                    break;
                case RIGHT:
                    result.nextResult = right;
                    result.nextDirection = Direction.RIGHT;  
                    break;
                case STOP:
                    result.nextResult = stop;
                    result.nextDirection = Direction.STOP;
                    break;
                case UNKNOWN:
                    result.nextResult = stop;
                    result.nextDirection = Direction.UNKNOWN;
                    break;
            }     
        }

        return result;
    }
    
    /**
     * Calculates the distance of a projectil to the player (in the future after a given number of steps)
     * @param projectil The projectil to check.
     * @param steps The number of steps.
     * @return The future distance.
     */
    private int getProjectilDistance(Projectil projectil, int steps) {
        int value;
        
        //Calc the current distance (depending on wether the world is inverted)
        if(this.controller.getWorld().isInverted()) {
            
            value = projectil.getY() - 
                    this.controller.getWorld().getPlayers().get(playerIndex).getHeight();
        }
        else {
            value = (this.controller.getWorld().getPlayers().get(playerIndex).getY() - 
                    (projectil.getY() + projectil.getHeight()));           
        }
        
        //calculate the future distance (the projectil is moving and will be nearer in the future
        value -= steps * projectil.getSpeed();
        
        return value;
    }
}