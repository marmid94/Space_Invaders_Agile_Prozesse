package spaceinvader.model;

import java.util.HashMap;
import java.util.Map;
import spaceinvader.model.level.EnemyWave;
import spaceinvader.model.level.Level;
import java.util.Date;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Listeners for recognizing tilt moves.
 * @author Fabian Haag
 * @version 1.0
 */
public class TiltListener implements GameListener {
    /**
     * Counters that counts the movements of each player at the given times.
     */
    private Map<Player,Map<Date, Integer>> moveCounters;
    
    /**
     * The maximum x move in the given time.
     */
    private int maxTilt;
    
    /**
     * The time for the max tilt.
     */
    private long time;
    
    /**
     * The time that the player will be locked on tilt.
     */
    private int lockTime;
    
    /**
     * The game controller.
     */
    private GameController controller;

    /**
     * Constructor.
     * @param maxTilt The maximum x move in the given time.
     * @param time The time for the max tilt.
     * @param lockTime The time that the player will be locked on tilt.
     */
    public TiltListener(GameController controller, int maxTilt, int time, int lockTime) {
        this.moveCounters = new HashMap<>();
        
        if(controller == null) {
            throw new NullPointerException("The argument controller must not be null");
        }
        
        if(maxTilt < 0) {
            throw new IllegalArgumentException("The value of the argument maxTilt must be >= 0.");
        }
        if(time < 0) {
            throw new IllegalArgumentException("The value of the argument time must be >= 0.");
        }
        if(lockTime < 0) {
            throw new IllegalArgumentException("The value of the argument lockTime must be >= 0.");
        }
        
        this.controller = controller;
        this.maxTilt = maxTilt;
        this.time = time;
        this.lockTime = lockTime;
    }
    
    
    
    @Override
    public void onWin() {}

    @Override
    public void onGameOver() {}

    @Override
    public void onLevelStarted(Level level, int levelIndex) {}

    @Override
    public void onLevelFinished(Level level, int levelIndex) {}

    @Override
    public void onRespawn(Level level, int waveIndex) {}

    @Override
    public void onStart() { }

    @Override
    public void onStop() {}

    @Override
    public void onDeath(LifeLostReason reason) {}

    @Override
    public void onLifesAdded(int numberOfLifes, LifeAddReason reason) {}

    @Override
    public void onPointsAchieved(int points, PointReason reason) {}

    @Override
    public void onHit(AbstractTarget target, Projectil projectil, int damage, boolean wasKilled, int x, int y) {}

    @Override
    public void onHeal(AbstractTarget target, int heal, boolean wasFullHeal) {}

    @Override
    public void onMultiKill(int kills, int points) {}

    @Override
    public void onWaveStarted(EnemyWave wave, int waveIndex) {}

    @Override
    public void onPlayerWeaponChanged(Player player, Weapon wepaon, int wepaonIndex) {}

    @Override
    public void onPlayerMoved(Player player, int xMove) {
        //adds an observed player if it doesn't exist yet
        if(!this.moveCounters.containsKey(player)) {
            this.moveCounters.put(player, new ConcurrentHashMap<Date,Integer>());
        }
        
        //save the new move data
        this.moveCounters.get(player).put(new Date(), xMove);
        
        
        this.checkTilt(player);
    }
    
    
    /**
     * Checks wether the max tilt was reached, locks the player if reached, and removes old date entries.
     * @param player The player for that the check is done.
     */
    private void checkTilt(Player player) {
        //calc the minimal date for calulating the tilt
        Date minDate = new Date((new Date().getTime()) - this.time);
        
        int currentTilt = 0;
        
        for(Entry<Date, Integer> moveEntry : this.moveCounters.get(player).entrySet()) {
            
            if(moveEntry.getKey().before(minDate)) {
                //removes old entries
                this.moveCounters.get(player).remove(moveEntry.getKey());
                
            }
            else {
                //calc the current tilt
                currentTilt += moveEntry.getValue();
            }
        }
        
        //lock if max tilt is reached
        if(currentTilt >= this.maxTilt || currentTilt <= -this.maxTilt) {
            player.lockPlayerMove(this.lockTime);
            this.controller.getNotificationManager().notifyTilet(player);
            this.moveCounters.get(player).clear();
        }
    }

    @Override
    public void onTilt(Player player) {}
    
}