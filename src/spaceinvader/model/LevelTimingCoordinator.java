package spaceinvader.model;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;
import spaceinvader.model.level.EnemyWave;

/**
 * The class for all timed events in the game.
 * @author Martin Schmid
 * @author Fabian Haag
 * @version 1.0
 */
public class LevelTimingCoordinator implements ActionListener {
    /**
     * The timer for all the executed operations.
     */
    private Timer timer;
    
    /**
     * The counter that is used as timer divisor for shooting and moving.
     */
    private int shootingCount, movingCounter;
    
    /**
     * The rest time of the mirror action.
     */
    private int mirrorRestTime;
    
    /**
     * States wether the mirror action shall be started.
     */
    private boolean initMirror;
    
    /**
     * The rest time of the reflecting action.
     */
    private int reflectingRestTime;
    
    /**
     * States wether the reflecting action shall be started.
     */
    private boolean initReflecting;
    
    /**
     * The GameController that the TimingCoordinator belongs to.
     */
    private final GameController controller;
    
    
    /**
     * The timer divisor for enemy shooting.
     */
    static final int ENEMY_SHOOTING_FREQUENCY = 10;
    
    /**
     * The basic timer delay.
     */
    static final int TIMER_DELAY = 10;

    /**
     * Constructor.
     * @param controller The GameController that the TimingCoordinator belongs to.
     */
    public LevelTimingCoordinator(GameController controller) {
        this.timer = null;
        this.shootingCount = 0;
        this.movingCounter = 0;
        this.controller = controller;
        
        this.mirrorRestTime = 0;
        this.initMirror = false;
        
        this.reflectingRestTime = 0;
        this.initReflecting = false;
    }
    
    /**
     * Inits a mirror action int the next timer trigger.
     * @param time The mirror time.
     */
    void initMirror(int time) {
        this.initMirror = true;
        this.mirrorRestTime = time;
    }
    
    /**
     * Inits a reflecting action int the next timer trigger.
     * @param time The reflecting time.
     */
    void initReflecting(int time) {
        this.initReflecting = true;
        this.reflectingRestTime = time;
    }
    
    /**
     * Starts the timer.
     */
    void start() {
        //If the timer has not been initialized => init it
         
        if(this.timer == null) {
            this.timer = new Timer(TIMER_DELAY, this);
            this.timer.setRepeats(true);
        }
        
        this.timer.start();
    }
    
    /**
     * Pauses the timer.
     */
    void pause() {
        this.timer.stop();
    }
    
    /**
     * Returns wether the timer is currently paused.
     * @return True if it is paused, false if not.
     */
    boolean isPaused() {
        return this.timer == null || !this.timer.isRunning();
    }
    
    void reset() {
        this.initMirror = false;
        this.initReflecting = false;
        this.mirrorRestTime = 0;
        this.movingCounter = 0;
        this.reflectingRestTime = 0;
        this.shootingCount = 0;
        
        
    }
    
     /**
     * Move all the projectils and do collision handling.
     */
    private void moveProjectils() {
        for(Projectil projectil : this.controller.getWorld().getProjectils()) {
            int speed = projectil.getSpeed();
            
            //invert the
            if(projectil.isDirectionDown() == this.controller.getWorld().isInverted()) {
                speed *= -1;
            }
                
            //move the projectil
            try {
                projectil.moveY(speed);
            }
            //if the projectil would leave the world
            catch(MapBorderException e) {
                
                if(this.controller.getWorld().isReflecting()) {
                    //reflects the projectil if reflecting is enabled
                    int yPos = projectil.getY() + speed;
                    
                    
                    
                    int numberOfReflections1 = 0;
                    int numberOfReflections2 = 0;
                    int realYPos;
                    boolean isDirectionInverted = false;
                    
                    //if the projectil would leave the upper border => correct it
                    if(yPos < 0) {
                        numberOfReflections1++;
                        isDirectionInverted = true;
                        yPos *= -1;
                    }
                    
                    
                    //calc number of reflections and the distance to the world border
                    numberOfReflections2 = yPos / (this.controller.getWorld().getHeight() - projectil.getHeight());
                    numberOfReflections1 += numberOfReflections2;
                    realYPos = yPos - numberOfReflections2 * (this.controller.getWorld().getHeight() - projectil.getHeight());
                    
                    if(numberOfReflections2 % 2 == 1) {
                        isDirectionInverted = !isDirectionInverted;
                    }
                    
                    //invert the projectil direction if needed
                    if(isDirectionInverted) {
                        projectil.invertDirection();
                    }
                    
                   
                    
                    //check wether the projectil is moving upwards
                    // => then correction of yPos is needed
                    boolean isDirectionDown = projectil.isDirectionDown() != this.controller.getWorld().isInverted();
                    
                   
                    
                    if(!isDirectionDown) {
                        realYPos = this.controller.getWorld().getHeight() - realYPos - projectil.getHeight();
                    }
                    
                   
                    
                    //set the new position
                    projectil.setPosition(projectil.getX(), realYPos);
                
                }
                else {
                    //remove the projectil if reflecting is disabled
                    this.controller.getWorld().removeProjectil(projectil);
                }
            }
        }
   
        //detect and handle collissions
        this.controller.getCollisionHandler().detectAndHandleProjectilCollissions();
    }

    /**
     * Updates the cooldown timers of the enemy and player weapons.
     */
    private void updateCooldownTimers() {
        //update the cooldown timers of the enemy weapons
        for(Enemy enemy : this.controller.getWorld().getEnemies()) {
            enemy.getWeapon().updateCooldownTimer();
        }
        
        //update the cooldown timers of the player weapons
        for(Player player : this.controller.getWorld().getPlayers()) {
            for(Weapon weapon : player.getWeapons()) {
                weapon.updateCooldownTimer();
            }
        }
    }
    
    
    /**
     * Lets the enemies randomly shoot depending on their priority.
     */
    private void shootWithEnemies() {
        for(Enemy enemy : this.controller.getWorld().getEnemies()) {
            //randomly shoot
            //prio 1000 -> always shooting; prio 0 -> never shooting                
            if(enemy.priority > (Math.random() * 999)) {    
                enemy.shoot();
            }
        }
    }
    
    /**
     * Moves the target rows.
     */
    private void moveTargetRows() {
        this.movingCounter++;
        EnemyWave wave = this.controller.getCurrentWave();
        
        //move the rows of the current wave (if there is one)
        //the movingCounter is used so that the moving tempo can be changed
        if(wave != null && this.movingCounter >= wave.getSpeed()) {
            this.movingCounter = 0;
            for(TargetRow row : this.controller.getWorld().getTargetRows()) {
                try {
                    row.moveX();
                }
                catch(MapBorderException e) {
                    this.controller.getWorld().removeTargetRow(row);
                }
            }
        }
    }
    
    /**
     * Updates the timers of the deflectors.
     */
    void updateDeflectors() {
        for(Deflector deflector : this.controller.getWorld().getDeflectors()) {
            deflector.decrementLifeTime();
        }
    }
    
    /**
     * Updates the move lock timers of the players.
     */
    void updatePlayerMoveLocks() {
        for(Player player : this.controller.getWorld().getPlayers()) {
            player.updatePlayerMoveTimer();
        }
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == this.timer) {
            //timer action
            
            synchronized(this.controller.getWorld()) {
                
                //handle the mirror trigger
                if(this.initMirror) {
                    //init the mirror
                    
                    
                    this.initMirror = false;
                    
                    if(this.mirrorRestTime > 1) {
                        
                        this.controller.getWorld().setInverted(!this.controller.getWorld().isInverted());
                    }
                }
                
                if(this.mirrorRestTime > 1) {
                    //wait until the mirror will be reset
                    this.mirrorRestTime--;
                }
                else if(this.mirrorRestTime == 1) {
                    //reset the mirror
                    this.mirrorRestTime = 0;
                    this.controller.getWorld().setInverted(false);
                }

                
                
                //handle the reflecting trigger
                if(this.initReflecting) {
                    //init the reflecting
                    
                    
                    this.initReflecting = false;
                    
                    this.controller.getWorld().setReflecting(true);
                }
                
                if(this.reflectingRestTime > 1) {
                    //wait until the reflecting will be reset
                    this.reflectingRestTime--;
                }
                else if(this.reflectingRestTime == 1) {
                    //reset the reflecting
                    this.reflectingRestTime = 0;
                    this.controller.getWorld().setReflecting(false);
                }
                
                
                
                
                this.updateCooldownTimers();
                
                this.updatePlayerMoveLocks();
                
                this.updateDeflectors();
                
                this.moveTargetRows();
                
                
                //shoot with the enemies
                if(this.shootingCount > ENEMY_SHOOTING_FREQUENCY) {
                    this.shootingCount = 0;
                    this.shootWithEnemies();
                }
                this.shootingCount++;
                
                
                
                
                this.moveProjectils();
                
            }
            
            //notitfy the GUI for repainting
            this.controller.getNotificationManager().notifiyRepaint();
        }
    }
}