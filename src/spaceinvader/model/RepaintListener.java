package spaceinvader.model;

/**
 * The listener for all repaint events.
 * @author Stefan Fruhwirth
 * @version 1.0
 */
public interface RepaintListener {
    void onRepaint();
    
    void onLifesChanged(int lifes);
    
    void onPointsChanged(int points);
}
