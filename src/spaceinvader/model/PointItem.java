package spaceinvader.model;

import java.awt.Image;

/**
 * An item that add points to the world when destroyed.
 * @author Philip Müller
 * @version 1.1
 */
public class PointItem extends AbstractItem {
    /**
     * The number of points beeing achieved when the item is destroyed.
     */
    private int winPoints;
    
    /**
     * The GameController where the points will be added.
     */
    private GameController controller;
    
    /**
     * Constructor, setting the full live points, size and image of the target.
     * @param image The image to paint for the object.
     * @param height The height of the object.
     * @param width The width of the object.
     * @param fullLivePoints The maximum live points of the object.
     * @param winPoints The number of points beeing achieved when the item is destroyed.
     */
    public PointItem(Image image, 
            int width, 
            int height, 
            int fullLivePoints, 
            int winPoints) {
        super(image, width, height, fullLivePoints);
        
        if(winPoints < 0) {
            throw new IllegalArgumentException("The value of the argument winPoints must be >= 0.");
        }
        
        this.winPoints = winPoints;
        this.controller = null;
    }

    @Override
    public void setController(GameController controller) {
        if(controller == null) {
            throw new NullPointerException("The argument controller must not be null.");
        }
        
        this.controller = controller;
    }
    
    @Override
    protected void performItemAction() {
        if(this.controller == null) {
            throw new IllegalStateException("The controller has to be set by the method setController before calling the method performItemAction.");
        }
        
        //add the points
        this.controller.addPoints(this.winPoints, GameListener.PointReason.ITEM);
    }

    /**
     * Returns the number of points that will be achieved when detroying this item.
     * @return The number of points that will be achieved when detroying this item.
     */
    public int getWinPoints() {
        return winPoints;
    }  
}