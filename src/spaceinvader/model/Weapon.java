package spaceinvader.model;

/**
 * A weapon that places projectils in the world when shooting.
 * @author Philip Müller
 * @version 1.1
 */
public class Weapon implements Cloneable {
    /**
     * The world where the weapon places the projectils.
     */
    protected World world;
    
    /**
     * The reamining cooldown time.
     * If this value is 0 then the weapon is ready to shoot.
     */
    protected int remainingCooldownTime;
    
    /**
     * The complete cooldown time.
     * The remaining cooldown time will be set to this value after shooting.
     */
    protected int cooldownTime;
   
    /**
     * The prototype for projectils that will be created when shooting.
     */
    protected Projectil prototypeProjectil;
    
    /**
     * The name of the weapon for displaying it in the GUI.
     */
    protected String name;
    
    /**
     * Constructor.
     * @param prototypeProjectil The prototype for projectils that will be created when shooting.
     * @param cooldownTime The complete cooldown time.
     * @param name The name of the weapon.
     */
    public Weapon( 
            Projectil prototypeProjectil,
            int cooldownTime,
            String name) {
        this.world = null;
        this.remainingCooldownTime = 0;
        
        if(cooldownTime < 0) {
            throw new IllegalArgumentException("The value of the argument cooldownTime must be >= 0.");
        }
        
        if(prototypeProjectil == null) {
            throw new NullPointerException("The argument prototypeProjectil must not be null.");
        }
        
        if(name == null) {
            throw new NullPointerException("The argument name must not be null.");
        }
        
        this.cooldownTime = cooldownTime;
        this.prototypeProjectil = prototypeProjectil;
        this.name = name;
    }
    
    /**
     * Sets the world to place projectils when shooting.
     * @param world The world to place projectils.
     */
    void setWorld(World world) {
        if(world == null) {
            throw new NullPointerException("The argument world must not be null.");
        }
        
        this.world = world;
    }
    
    /**
     * Returns the projectil prototype.
     * @return The projectil prototype.
     */
    Projectil getPrototype() {
        return this.prototypeProjectil;
    }

    /**
     * Returns the name of the weapon.
     * @return The name of the weapon.
     */
    public String getName() {
        return name;
    }
    
    /**
     * Creates a projectil and places it in the world.
     * @param isEnemy Defines wether the projectil was shot from a player or an enemy.
     * @param x The x position of the created projectil.
     * @param y The y position of the created projectil.
     * @throws MapBorderException If the x or y position would be out of the world.
     */
    void createProjectil(boolean isEnemy, int x, int y) throws MapBorderException {
        if(this.world == null) {
            throw new IllegalStateException("The world has to be set by the method setWorld before calling createProjectil.");
        }
        
        if(this.remainingCooldownTime == 0) {
            //projectil will only be created, if the cooldown time is 0
            
            //create and place the projectil
            this.world.addProjectil(
                    Projectil.createFromPrototype(this.prototypeProjectil, isEnemy), x, y);
        
            //set the new cooldown time
            this.remainingCooldownTime = this.getCooldownTime();
        }
    }
    
    /**
     * Returns the complete cooldown time.
     * @return The complete cooldown time.
     */
    public int getCooldownTime() {
        return this.cooldownTime;
    }
    
    /**
     * Returns the remaining cooldown time.
     * @return The remaining cooldown time.
     */
    public int getRemainingCooldownTime() {
        return this.remainingCooldownTime;
    }
    
    /**
     * Returns wether the weapon is ready to shoot.
     * @return True if the weapon is ready to shoot, false if not.
     */
    public boolean isReady() {
        return this.getRemainingCooldownTime() == 0;
    }
    
    /**
     * Makes the weapon ready to shoot.
     */
    void setReady() {
        this.remainingCooldownTime = 0;
    }
    
    /**
     * Descreases the remaining cooldown time if it is greater than 0.
     * Will be called by the cooldown timer.
     */
    void updateCooldownTimer() {
        if(this.remainingCooldownTime > 0) {
            this.remainingCooldownTime--;
        }
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone(); 
    }
}