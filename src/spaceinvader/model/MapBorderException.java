package spaceinvader.model;

/**
 * An exception that will be thrown if a object is tried to be placed outside the bounds of a map.
 * @author Philip Müller
 * @version 1.0
 */
public class MapBorderException extends RuntimeException {
    private int currentX, currentY;
    private int desiredX, desiredY;
    private int mapWidth, mapHeight;

    /**
     * Constructor.
     * @param currentX
     * @param currentY
     * @param desiredX
     * @param desiredY
     * @param mapWidth
     * @param mapHeight 
     */
    public MapBorderException(int currentX, int currentY, int desiredX, int desiredY, int mapWidth, int mapHeight) {
        this.currentX = currentX;
        this.currentY = currentY;
        this.desiredX = desiredX;
        this.desiredY = desiredY;
        this.mapWidth = mapWidth;
        this.mapHeight = mapHeight;
    }

    /**
     * Constructor giving a message.
     * @param currentX
     * @param currentY
     * @param desiredX
     * @param desiredY
     * @param mapWidth
     * @param mapHeight
     * @param message 
     */
    public MapBorderException(int currentX, int currentY, int desiredX, int desiredY, int mapWidth, int mapHeight, String message) {
        super(message);
        this.currentX = currentX;
        this.currentY = currentY;
        this.desiredX = desiredX;
        this.desiredY = desiredY;
        this.mapWidth = mapWidth;
        this.mapHeight = mapHeight;
    }

    public int getCurrentX() {
        return currentX;
    }

    public int getCurrentY() {
        return currentY;
    }

    public int getDesiredX() {
        return desiredX;
    }

    public int getDesiredY() {
        return desiredY;
    }

    public int getMapHeight() {
        return mapHeight;
    }

    public int getMapWidth() {
        return mapWidth;
    }
}
