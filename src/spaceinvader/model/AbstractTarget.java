package spaceinvader.model;

import java.awt.Image;

/**
 * A world object that can be destroyed by shooting on it or on collision.
 * @author Philip Müller
 * @version 1.1
 */
public abstract class AbstractTarget extends AbstractWorldObject{
    /**
     * The current live points of the object.
     * If the live points are 0 => the target is dead.
     */
    private int currentLivePoints;
    
    /**
     * The maximum live points of the object.
     * If the target has full live points it is completely healed.
     */
    private int fullLivePoints;
    
    /**
    * Constructor, setting the full live points, size and image of the target.
    * @param image The image to paint for the object.
    * @param height The height of the object.
    * @param width The width of the object.
    * @param fullLivePoints The maximum live points of the object.
    */
    public AbstractTarget(Image image, int width, int height, int fullLivePoints) {
        super(image, width, height);
        
        if(fullLivePoints <= 0) {
            throw new IllegalArgumentException("The argument fullLivePoints needs to have a value > 0.");
        }
        this.currentLivePoints = fullLivePoints;
    }

    @Override
    void moveX(int x) throws MapBorderException {
        super.moveX(x); 
        
       //notify that the target was moved
        this.world.getNotificationManger().notifyTargetMove(this);
    }

    @Override
    void moveY(int y) throws MapBorderException {
        super.moveY(y); 

        //notify that the target was moved
        this.world.getNotificationManger().notifyTargetMove(this);
    }
    
    /**
     * Returns the maximum live points of the target.
     * @return The maximum live points of the target.
     */
    public int getFullLivePoints() {
        return this.fullLivePoints;
    }
    
    /**
     * Returns the current live points of the target.
     * @return The current live points of the target.
     */
    public int getCurrentLivePoints() {
        return this.currentLivePoints;
    }
          
    /**
     * Completely heals the target.
     * Sets the current live points to the full live points.
     */
    void heal() {
        this.heal(this.getFullLivePoints());
    }
    
    /**
     * Heals the target by the value of livePoints.
     * The maximum livePoints that the target can reach is the value of fullLivePoints().
     * @param livePoints The points to heal the target.
     */
    void heal(int livePoints) {
        synchronized(this) {
            if(livePoints <= 0) {
                throw new IllegalArgumentException("The argument livePoints needs to have a value > 0.");
            }

            //heal this target
            this.currentLivePoints += livePoints;
            
            if(this.currentLivePoints > this.getFullLivePoints()) {
                //if it would heal to more than full live points => heal to the maximum value (fullLivePoints())
                this.currentLivePoints = this.getFullLivePoints();
            }
        }
        
        //notify that the target was healed
        this.world.getNotificationManger().notifyHeal(
                this, 
                livePoints, 
                this.currentLivePoints == this.getFullLivePoints());
    }
    
    /**
     * Hits the target.
     * @param damage The damage of the hit.
     * @return True if the target was killed by the hit, false if not.
     */
    boolean hit(Projectil projectil) {
        if(projectil == null) {
            throw new NullPointerException("The argument projectil must not be null");
        }
        
        boolean isKilled;
        synchronized(this) {
        
            if(projectil.getDamage() >= this.currentLivePoints) {
                //if the target was killed
                this.currentLivePoints = 0;

                isKilled = true;
            }
            else {
                //if the target survives the hit
                this.currentLivePoints -= projectil.getDamage();

                isKilled = false;
            }
        }
        
        if(isKilled) {
            //call the abstract destruction method that is implemented by the derived types
            this.destroy();
        }
        
        //notifiy that the target was hit
        this.world.getNotificationManger().notifyHit(this, projectil, projectil.getDamage(), isKilled);
        return isKilled;
    }
    
    /**
     * Abstract method that will be internally called when the target is killed.
     * Will be implemented by the derived types for specific onKill actions.
     */
    protected abstract void destroy();
}