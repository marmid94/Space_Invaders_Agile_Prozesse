/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spaceinvader;

import spaceinvader.gui.MainFrame;
import spaceinvader.model.GameController;
import spaceinvader.model.level.GameLibrary;

/**
 * generate the main class of the game
 * @author Stefan Fruhwirth
 * @version 1.1
 */
public class SpaceInvader {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
     
        
        GameLibrary.initLibrary();
        
        GameController gameController = new GameController();
        gameController.init(800, 500);
        
        MainFrame frame = new MainFrame(gameController);
       
    }
    
}
